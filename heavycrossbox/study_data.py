#!/usr/bin/env python3

import pathlib
import torch

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import (
    hist_2d,
    load_input_csv,
    get_f_data,
    hist_f_cum,
    hist_f_eps,
    get_a_data,
    hist_a_cum,
    hist_a_eps,
)
from finn.heavycrossbox.phase_space import PhaseSpace
from finn.heavycrossbox.connection_matrix import ConnectionMatrix


if __name__ == "__main__":
    root = pathlib.Path("heavycrossbox")

    cfg = get_cfg(root)

    device = torch.device("cpu")
    dtype = getattr(torch, cfg["dtype"])

    scale, mt2, cut, cut_spur = ps_params(cfg)

    phase_space = PhaseSpace(scale, mt2, cut, cut_spur, dtype, device)

    n = 1000000
    x = phase_space.test(n, cut_spurious=True)
    data_train = x.cpu().numpy()

    print(" " * 4, "s", " " * 7, "t")
    print("min", data_train.min(axis=1))
    print("max", data_train.max(axis=1))

    boundary_points = load_input_csv(root / "boundary_input.csv")
    testing_points = load_input_csv(root / "testing_input.csv")

    hist_2d(
        root,
        data_train,
        boundary_points,
        testing_points,
        "$s$",
        "$t$",
        lloc="lower left",
    )

    f = abs(get_f_data(root, cfg))

    hist_f_eps(root, cfg, f)

    hist_f_cum(root, f)

    c = abs(get_a_data(cfg, ConnectionMatrix, x[:, :1000], mt2))

    hist_a_cum(root, c)

    hist_a_eps(root, c, cfg)
