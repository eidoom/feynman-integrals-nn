#!/usr/bin/env python3

import numpy

from finn.lib.generate_ps import generate_dataset_inputs


def boundaries(ps, n):
    assert not n % 3
    m = n // 3

    scale = float(ps.scale)
    mt2 = float(ps.const)
    cut = float(ps.cut_phys)

    sl = 2 * cut
    sh = scale

    tl = -(scale - cut)
    th = -cut

    rng = numpy.random.default_rng(1)

    # nb specific to scale/const/cut choice

    # t == th
    sa = rng.uniform(sl, sh, m)
    x0 = numpy.column_stack((sa, numpy.full(m, th)))

    # t == -s + cut
    sb = rng.uniform(sl, sh, m)
    x1 = numpy.column_stack((sb, -sb + cut))

    # s == sh
    tb = rng.uniform(tl, th, m)
    x2 = numpy.column_stack((numpy.full(m, sh), tb))

    # spurious surfaces are excluded by physical region

    return numpy.concatenate((x0, x1, x2))


if __name__ == "__main__":
    generate_dataset_inputs("heavycrossbox", boundaries, rationalised=True)
