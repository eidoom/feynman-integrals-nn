import math
import torch

from finn.lib.phase_space import PhaseSpaceBase


class PhaseSpace(PhaseSpaceBase):
    def points(self, n):
        """
        s, t(, mt2=const)
        """

        s = self.uniform(2 * self.cut_phys, self.scale, n)

        t = self.uniform(-s + self.cut_phys, -self.cut_phys, n)

        return torch.stack((s, t))

    def a_phys(self, x):
        """
        s channel: (mt2 > 0, s > 0, )-t > 0, s + t > 0
        """
        s = x[0]
        t = x[1]

        return torch.stack((-t, s + t))

    def a_spur(self, x):
        s = x[0]
        t = x[1]
        mt2 = self.const

        return torch.stack(
            (
                4 * mt2 + s,
                16 * mt2 + s,
                4 * mt2 + s + t,
                4 * mt2 - s,
                4 * mt2 - t,
                8 * mt2 - t,
                s**2 - 4 * mt2 * t + s * t,
                4 * mt2 * s + 4 * mt2 * t - s * t,
                4 * mt2 * s - s * t - t**2,
            )
        ).abs()
