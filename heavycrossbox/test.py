#!/usr/bin/env python3

import pathlib

from finn.lib.configuration import get_cfg
from finn.lib.testing import (
    get_abs_diff,
    get_args,
    get_rel_diff,
    get_testing_data,
    init_model,
    plot_abs_diff_cum,
    plot_abs_diff_eps,
    plot_abs_diff_ps,
    plot_training_statistics,
)


if __name__ == "__main__":
    args = get_args()
    root = pathlib.Path("heavycrossbox")
    part = root / args.part
    rep = part / args.replica

    plot_training_statistics(rep, p=3)

    cfg = get_cfg(root, part, args.override)

    model = init_model(rep, cfg)

    x, ff, nn = get_testing_data(root, part, cfg, model)

    ad = get_abs_diff(ff, nn)

    rd, _, nonzero = get_rel_diff(ff, nn)

    plot_abs_diff_cum(rep, ad)

    plot_abs_diff_ps(rep, ad, x, "$s$", "$t$")

    plot_abs_diff_eps(rep, cfg, ad, ff)
