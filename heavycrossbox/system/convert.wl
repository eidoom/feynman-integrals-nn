(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


{sijs,js,des}=Get["NPL_MIs_and_DEs_dimless.m"];


DeleteDuplicates@Series[js/.j[x__]:>Sum[j[x][i]*eps^i,{i,-4,0}],{eps,0,-2}]//Normal
(* MI series starts from eps^-2 but cancellations below eps^0 so MI really starts from eps^0 *)


sijs


sijs//Length


js//Length


des//Dimensions


desRed = des[[;;2]];


desRed//Dimensions


desCoeff = Table[Coefficient[desRed,eps,i],{i,0,2}];


desCoeff//Dimensions


desFlat=Flatten@desCoeff;


desFlat//Dimensions


desSimp=Simplify@desFlat;


Put[desSimp,"des.m"];
