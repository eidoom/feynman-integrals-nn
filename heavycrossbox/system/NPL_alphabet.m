{
  mt2,
  s,
  t,
  s + t,
  4*mt2 + s,
  16*mt2 + s,
  4*mt2 + s + t,
  4*mt2 - s,
  4*mt2 - t,
  8*mt2 - t,
  s^2 - 4*mt2*t + s*t,
  4*mt2*s + 4*mt2*t - s*t, 
  4*mt2*s - s*t - t^2
}
