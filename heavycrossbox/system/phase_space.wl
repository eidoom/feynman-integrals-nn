(* ::Package:: *)

Module[
{m,p,tt,ca},
m=DiagonalMatrix@{1,-1,-1,-1};
p[1]=Sqrt@s/2{1,0,0,1};
p[2]=Sqrt@s/2{1,0,0,-1};
p[3]=Sqrt@s/2{1,Sin@a,0,Cos@a};
p[4]=p[1]+p[2]-p[3];
Print["mom cons: ", p[1]+p[2]==p[3]+p[4]];
Print["on-shell: ", Simplify[p[#] . m . p[#]]&/@Range@4];
Print["s = ", # . m . #&[p[1]+p[2]]];
tt = FullSimplify[# . m . #]&[p[1]-p[3]];
Print["t = ", tt];
Print["p1.p2 = ", p[1] . m . p[2]];
ca=Solve[tt==t, Cos[a]][[1,1]];
Print["p1.p3 = ", Simplify[(p[1] . m . p[3])/.ca]];
Print["p2.p3 = ",Simplify[(p[2] . m . p[3])/.ca]];
]


Module[
{scale, cut, mt2},
scale = N@Sqrt[10];
mt2 = 1;
cut = 0.1*scale;
Print[scale," ",mt2," ",cut];
phys=And@@{
 s > cut,
 -t > cut,
 s + t > cut
};
spur=And@@{
  Abs[4*mt2 + s] > cut,
  Abs[16*mt2 + s] > cut,
  Abs[4*mt2 + s + t] > cut,
  Abs[s - 4*mt2] > cut,
  Abs[4*mt2 - t] > cut,
  Abs[8*mt2 - t] > cut,
  Abs[s^2 - 4*mt2*t + s*t] > cut,
  Abs[4*mt2*s + 4*mt2*t - s*t] > cut, 
  Abs[4*mt2*s - s*t - t^2] > cut
};
RegionPlot[
 {phys, And@@{phys, spur}},
 {s,0,scale},{t,-scale,0},
 FrameLabel -> {"s", "t"}
]
]


Module[
{scale, cut, mt2},
scale = Sqrt@10;
mt2 = 1;
cut = 0.1*scale;
all=-s<t<0&&cut<s<scale;
alla=Area@ImplicitRegion[all,{s,t}];
phys=And@@{
 s > cut,
 -t > cut,
 s + t > cut,
 s < scale,
 t > -scale
};
spur=And@@{
  Abs[4*mt2 + s] > cut,
  Abs[16*mt2 + s] > cut,
  Abs[4*mt2 + s + t] > cut,
  Abs[s - 4*mt2] > cut,
  Abs[4*mt2 - t] > cut,
  Abs[8*mt2 - t] > cut,
  Abs[s^2 - 4*mt2*t + s*t] > cut,
  Abs[4*mt2*s + 4*mt2*t - s*t] > cut, 
  Abs[4*mt2*s - s*t - t^2] > cut
};
physa=Area@ImplicitRegion[phys,{s,t}];
spura=Area@ImplicitRegion[phys&&spur,{s,t}];
Print["scales ", N@Min[cut,mt2], "--", N@scale];
Print["efficiency ", PercentForm@N[spura/alla]];
Print["training coverage ", PercentForm@N[spura/physa]];
]



