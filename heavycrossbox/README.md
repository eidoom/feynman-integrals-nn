# Heavy-cross-box

The system is defined by `system/NPL_MIs_and_DEs_dimless.m` as the nested list:

```
{
    {kinematic variables},
    {master integrals},
    {connection matrices}
}
```

## Dataset generation

Mathematica script depends on `AMFlow` for evaluation of integrals.

For example,

```shell
./heavycrossbox/generate_ps.py 10 100
cd heavycrossbox
./generate_fn.sh boundary 1 10
./generate_fn.sh testing 1 100
cd ..
```

## Training

For replica `<replica>` of part `<part>`,

```shell
./train.py heavycrossbox <part> <replica>
./test.py -p heavycrossbox <part> <replica>
```

## Timing

Epoch (512x256) time:

| Machine   | Device                                | RAM (GB) | Type | Partitions | Time (s) |
| --------- | ------------------------------------- | -------- | ---- | ---------- | -------- |
| Laptop 1  | Nvidia GeForce RTX 3050 Ti Laptop GPU | 4        | cuda | 4          | 29       |
| Desktop 1 | Nvidia GeForce GTX 1080 Ti GPU        | 11       | cuda | 2          | 53       |
| Laptop 2  | Nvidia GeForce GTX 1050 Mobile GPU    | 4        | cuda | 4          | 72       |
| Laptop 1  | Intel Core i7-12700H CPU `-j6`        | 32       | cpu  | 1          | 113      |
| Desktop 1 | Intel Core i5-6600K CPU               | 16       | cpu  | 2          | 165      |
| Laptop 2  | Intel Core i7-7700HQ CPU              | 16       | cpu  | 1          | 222      |
| Server 1  | Intel Xeon Gold 6130 CPU `-j16`       | 768      | cpu  | 1          | 241      |
