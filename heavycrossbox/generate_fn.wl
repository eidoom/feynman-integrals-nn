(* ::Package:: *)

(* vim: set ft=mma: *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


cacheDir = "cache"
If[
  DirectoryQ@cacheDir,
  DeleteDirectory[cacheDir, DeleteContents->True];
];


(* nb needs amflow commit b510cb20020fdd226c54f9c2cd0eefa7f5900b32 or later *)
Get[FileNameJoin @ {"~", "git", "amflow", "AMFlow.m"}];
SetReductionOptions["IBPReducer" -> "FiniteFlow+LiteRed"];


args = $CommandLine[[4;;]];
name = args[[1]];
n = ToExpression@args[[2]];


loops = 2;
(* need mis from eps^0 up to eps^4 *)
minepsmis = 0;
maxepsmis = 4;
(* and the js from eps^-4 up to eps^2 *)
maxepsorder = 2;
(* AMFlow starts js at eps^(-2L) = eps^(-4) *)
(* AMFlow takes the number of steps in eps orders as argument *)
(* ie. (number of eps orders) - 1 == highest weight *)
epsordersteps = 2 * loops + maxepsorder;
precision = 16;
threads = 64;


point = Import[name <> "_input.csv"][[n]];


cfg = Association @@ Import["config.json"];


{sijs, ints, des} = Get[FileNameJoin @ {"system", "NPL_MIs_and_DEs_dimless.m"}];


js = Union@Cases[ints, _j, Infinity];


mt2n = "const" /. cfg["phase_space"];
fullPoint = Append[point, mt2n];


Numeric = Thread[sijs -> (Rationalize@fullPoint)];


Internal = {k1, k2};
External = {p1, p2, p3, p4};
Invariants = sijs;
Conservation = {p4 -> p1 + p2 - p3};
Replacement = {
 p1^2 -> 0,
 p2^2 -> 0,
 p3^2 -> 0,
 p4^2 -> 0,
 p1*p2 -> s/2, 
 p1*p3 -> -t/2,
 p2*p3 -> (s+t)/2, 
 Nothing
};
InverseReplacement = {
  s -> 2*p1*p2,
  t -> -2*p1*p3,
  Nothing
};


Propagator = {
  k1^2,
  (k1 - p1)^2,
  (k1 + p2)^2,
  k2^2 - mt2,
  (k1 + k2 - p1)^2 - mt2,
  (k2 - p1 - p2 + p3)^2 - mt2,
  (k1 + k2 - p1 + p3)^2 - mt2,
  (k1 + p3)^2,
  (k1 + k2)^2,
  Nothing
};


AMFlowInfo["Family"] = NPL;
AMFlowInfo["Loop"] = Internal;
AMFlowInfo["Leg"] = External;
AMFlowInfo["Conservation"] = Conservation;
AMFlowInfo["Replacement"] = Replacement;
AMFlowInfo["Propagator"] = Propagator;
AMFlowInfo["Numeric"] = Numeric;
AMFlowInfo["NThread"] = threads;


safetyFilename = name <> "_js_" <> ToString[n] <> ".m";
If[
  FileExistsQ[safetyFilename]
  ,
  Print["Using j cache ", safetyFilename];
  jn = Get[safetyFilename];
  ,
  jn = SolveIntegrals[js, precision, epsordersteps];
  Put[jn, safetyFilename];
];


sol = Series[ints /. Numeric /. (jn /. Rule[a_, b_] :> (a -> b + FAIL * eps^(maxepsorder + 1))), {eps, 0, maxepsmis}]//Normal;


Print["check mis highest order: ", FreeQ[Variables@sol, FAIL]];
Print["check mis lowest order: ", DeleteCases[Coefficient[sol,eps,minepsmis-1],0]==={}];


solCoeffs = Flatten@Transpose@Table[Coefficient[sol,eps,i],{i,minepsmis,maxepsmis}];


(* further checks *)
re=Re@solCoeffs;
im=Im@solCoeffs;

epsorders = maxepsmis-minepsmis+1;
nMIs=36;

checkDim=Dimensions[re]===Dimensions[im]==={nMIs*epsorders};
If[
  Not[checkDim]
  ,
  Print["Dimension check failed! Quitting"];
  Quit[];
  ,
  Print["Dimension checks passed"];
];

re=ArrayReshape[re,{nMIs,epsorders}];
im=ArrayReshape[im,{nMIs,epsorders}];

expectedReW0 = Join[{1,-1},ConstantArray[0,nMIs-2]];

checkRe0=DeleteDuplicates[Chop[re[[All,1]]-expectedReW0]]==={0};
checkIm0=DeleteDuplicates[Chop[im[[All,1]]]]==={0};

If[
  Not[checkRe0&&checkIm0]
  ,
  Print["Weight-0 checks failed! Quitting"]; 
  Quit[];
  ,
  Print["Weight-0 checks passed"];
];


filename = name <> "_output_" <> StringPadLeft[ToString@n, 3, "0"] <> ".csv";
Export[FileNameJoin @ {"re", filename}, {Re@solCoeffs}];
Export[FileNameJoin @ {"im", filename}, {Im@solCoeffs}];


If[
  DirectoryQ@cacheDir,
  DeleteDirectory[cacheDir, DeleteContents->True];
];


Quit[];
