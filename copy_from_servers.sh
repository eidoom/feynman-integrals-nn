#!/usr/bin/env bash

trap "exit" SIGINT

f=heavycrossbox
# f=topbox

declare -A is=(
    [1]="$(seq 11 40)"
    [4]="$(seq 41 70)"
    [5]="$(seq 71 100)"
)

# d=boundary
d=testing

for i in ${!is[@]}; do
    for j in ${is[$i]}; do
        for p in re im; do
            rsync jd0${i}:git/feynman-integrals-nn/${f}/${p}/${d}_output_$(printf '%03d' $j).csv ${f}/${p}/ || exit 1
        done
    done
done

for p in re im; do
    cat ${f}/${p}/${d}_output_*.csv > ${f}/${p}/new_${d}_output.csv
done
