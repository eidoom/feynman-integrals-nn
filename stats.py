#!/usr/bin/env python3

import argparse, pathlib

from finn.lib.testing import (
    plot_training_statistics,
)


def get_args():
    parser = argparse.ArgumentParser("Plots the learning curve")
    parser.add_argument(
        "data",
        type=pathlib.Path,
        help="Path to data, eg box/re/0",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()

    plot_training_statistics(args.data, ss=("pdf",))
