(* ::Package:: *)

(* phase space construction the same as box1m 1L, just have additional spurious pole at 2L *)


Module[
{s12, cut},
s12 = 24;
cut = 0.1*s12;
all=And@@{
 -s23 > 0,
 s4 > 0,
 s12 + s23 - s4 > 0
};
phys=And@@{
 -s23 > cut,
 s4 > cut,
 s12 + s23 - s4 > cut
};
spur=And@@{
  Abs[s12-s4] > cut,
  Abs[s4-s23] > cut,
  Abs[s12+s23] > cut
};
RegionPlot[
 {all, And@@{phys, spur}, Not@spur},
 {s23,-s12,0},{s4,0,s12},
 FrameLabel -> {"s23", "s4"},
 PlotLegends->{
  "unregulated scattering region",
  "regulated scattering region", 
  "spurious regions"
 }
]
]


Module[
{s12, cut},
s12 = 24;
cut = 0.1*s12;
all=And@@{
 2*cut< s12-s4 <s12-cut,
 -(s12-s4) < s23 < -cut
};
alla=Area@ImplicitRegion[all,{s23,s4}];
phys=And@@{
 -s23 > cut,
 s4 > cut,
 s12 + s23 - s4 > cut
};
spur=And@@{
  Abs[s12-s4] > cut,
  Abs[s4-s23] > cut,
  Abs[s12+s23] > cut
};
physa=Area@ImplicitRegion[phys,{s23,s4}];
spura=Area@ImplicitRegion[phys&&spur,{s23,s4}];
Print["scales ", N@cut, "--", N@s12];
Print["efficiency ", PercentForm@N[spura/alla]];
Print["training coverage ", PercentForm@N[spura/physa]];
]


(* EOF *)
