(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* ::Subsubsection:: *)
(*Is*)


family = t331ZZZM;
(*{is,sijs,des}=Get[ToString[family]<>"_MIs_and_DEs_dimless.m"];*)
{sijs,is,des}=Get[ToString[family]<>"_DEs.m"];


j2mi = Get[ToString[family]<>"_reduction_to_utbasis.m"]; (* expression of the custom MIs j[fam,...]'s in terms of pure MIs mi[fam,i] *)


mi2spfuncs = Get[ToString[family]<>"_mi2spfuncs.m"]; (* expressions of the pure basis integrals mi[fam,i] in terms of special functions and constants *)


solution = Normal@Series[is /. j2mi /. mi[a__]:>(mi[a]+eps^5*ALARM) /. mi2spfuncs,{eps,0,4}];


logs2mpls={
	Log[(s12 + s23)/s12] -> G$[{-s12/s23}, 1],
	Log[1 - s12/s4] -> G$[{s4/s12}, 1],
	Log[1 - s23/s4] -> G$[{s4/s23}, 1],
	Log[(s12 + s23 - s4)/(s12 - s4)] -> G$[{(-s12 + s4)/s23}, 1]
};


toHandyG = G$[{x__},1]:>G[x,1];


solMPLd = solution /. logs2mpls /. toHandyG;


logCont = {
	Log[s23/s4] -> I*Pi + Log[-(s23/s4)],
	Log[-s4] -> (-I)*Pi + Log[s4]
};


mplCont = {
	G[i___,s4/s12,j___]:>G[i,SubPlus[s4/s12],j],
	G[a___,-s12/s23,b___]:>G[a,SubPlus[-s12/s23],b]
};


solCont = solMPLd /. logCont //. mplCont;


solCoeffs = Transpose@Table[Coefficient[solCont,eps,i],{i,0,4}];


(*solSimp=solCoeffs;*)
solSimp = Simplify@solCoeffs;


mpls=Union@Cases[solSimp,_G,Infinity];


Put[{mpls, solSimp}, "../solution.m"];


(* ::Subsubsection:: *)
(*DEs*)


desRed = des[[2;;]];


desCoeff = Table[Coefficient[desRed,eps,i],{i,0,1}];


desCoeff//Dimensions


desFlat = Flatten@desCoeff;


desSimp = FullSimplify@desFlat;


CollectFactorsX[EXPR_] := Module[{tmp,depth,coeff},
  depth = 1;
  If[MatchQ[EXPR[[0]],Plus], tmp = List@@EXPR; depth+=1;, tmp = EXPR;];
  coeff = Replace[EXPR,{Power[expr_,i_Integer]:>FFF[expr]^i /; i>0, Power[expr_,i_Integer]:>FFF[expr]^i /; i<0, Plus[expr_]:>FFF[Plus[expr]]},{depth}];
  Return[coeff];
];

FindIndependentFactorsX[Coefficients_List] := Module[{vars,rules,coeffs},
  coeffs = CollectFactorsX /@ Coefficients;
  vars = Join@@(Select[Variables[#],!MatchQ[#,FFF[_?NumericQ]]&]& /@ coeffs);
  vars = DeleteDuplicates[vars];
  rules =  MapIndexed[Rule[#1,y@@#2]&,vars];
  coeffs = coeffs //. Dispatch[rules] /. FFF[a_]:>a;
  Return[{coeffs,rules /. Rule[a_,b_]:>Rule[b,a /. FFF[x_]:>x]}];
];


{collected, factors} = FindIndependentFactorsX@desSimp;


Put[factors//Values,"factors.m"];


Put[collected/.{0->z,y[i_]:>y[i-1]},"collected.m"];
