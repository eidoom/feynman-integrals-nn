(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* factor to be pulled out - must have dim of mass^2 ~ s_{ij} *)
factor = s4;


(* load DEs for dimensionsfull basis *)
family = "t331ZZZM";
{sijs, mis, des} = Get[family<>"_DEs.m"];


scaling=sijs . des  // Simplify;
Print["check scaling: ", scaling//DiagonalMatrixQ];
Print["check integrability: ", 
  DeleteDuplicates[des[[1]] . des[[2]]-des[[2]] . des[[1]]+(D[des[[1]],sijs[[2]]]-D[des[[2]],sijs[[1]]])//Simplify//Flatten]==={0}];


(* function which returns the dimension in units of mass^2 for an expression in terms of sij's and j's *)
looporder = 2;
MassDim[expr_]:= Simplify[(expr/.Thread[sijs->\[CapitalLambda]*sijs]/.j[fam_,b__]:>j[fam,b]*\[CapitalLambda]^(-Total[{b}]+looporder*(2-eps)))/expr,\[CapitalLambda]>0];


dimensions=Exponent[MassDim/@mis,\[CapitalLambda]];
Print["check that scaling matches dimension: ", DeleteDuplicates[DiagonalMatrix[dimensions]-scaling//Simplify//Flatten]==={0}];


(*factorVec=factor^dimensions;*)


sijs


powers=Values@<|
 1->{2,-2,-2},
 2->{2,-3,-2},
 3->{3,-2,-3},
 4->{1,-1,-2},
 5->{4,-5,0},
 6->{3,-3,-2},
 7->{1,-2,-1},
 8->{3,-1,-3},
 9->{2,-3,-1},
10->{3,0,-4},
11->{2,-3,0},
12->{3,-3,-1},
13->{3,0,-4},
14->{2,-2,-1},
15->{4,-5,0},
16->{1,-2,0},
17->{3,-3,-1},
18->{5,-1,-3}
|>;
Print["check powers add up correctly: ", Plus@@@powers == Coefficient[dimensions,eps,0]];


factorVec=(Times@@(sijs^#)&/@powers)*(factor^(eps*Coefficient[dimensions,eps]));


T = DiagonalMatrix[factorVec];
newmis=Inverse[T] . mis;
Print["check that new basis is dimensionless: ", Union[MassDim/@newmis]==={1}];


newdes = Table[Factor[Inverse[T] . (des[[jj]] . T-D[T,sijs[[jj]]])],{jj,Length[sijs]}];
Print["check scaling: ", Union[Flatten[sijs . newdes//Simplify]]==={0}];
Print["check integrability: ",
 DeleteDuplicates[newdes[[1]] . newdes[[2]]-newdes[[2]] . newdes[[1]]+(D[newdes[[1]],sijs[[2]]]-D[newdes[[2]],sijs[[1]]])//Simplify//Flatten]==={0}];


simpdes=Simplify@newdes;


newfinal = {newmis, sijs, simpdes};


Put[newfinal,family<>"_MIs_and_DEs_dimless.m"];
