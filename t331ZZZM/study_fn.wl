(* ::Package:: *)

Install["handyG"];


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


{mpls, sol} = Get["solution.m"];
geval[ps_] := Module[{gg = Thread[mpls->(mpls/.#)]}, N[sol /. gg /. #]] & /@ ps;


m=1000;
cfg = Association @@ Import["config.json"];
s12n = "const" /. cfg["phase_space"];
input = Import["validation_input.csv"];
points = input[[;;Min[Length@input,m]]] /. {a_, b_} :> {s12->s12n, s23->a, s4->b};
fns = geval[points];
ffns = Flatten[ReIm@fns,{{2},{1,3,4}}];


means=Mean/@ffns
n=10;
{n, means[[n]]}
Module[{maxi=Position[Abs@means,Max@Abs@means][[1,1]]}, Flatten@{maxi, means[[maxi]]}]
(*stds=StandardDeviation/@ffns
{n,stds[[n]]}
Module[{max=Max@stds}, Flatten@{Position[stds,max], max}]*)
{Mean@means, StandardDeviation@Flatten@ffns}
(*Histogram[ffns,ChartLegends->Range@Length@sol]*)
Histogram[Flatten@ffns]


(* EOF *)
