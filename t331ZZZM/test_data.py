#!/usr/bin/env python3

import pathlib, json
import numpy, torch, matplotlib.pyplot
from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import get_a_data, hist_f_cum
from finn.t331ZZZM.connection_matrix import ConnectionMatrix


def hist_df(part, cfg, x, fn, connection, name=None, suffix=("png", "pdf")):
    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    batch_size = x.shape[1]
    num_in = cfg["dimensions"]["input"]["variables"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    des_eps = cfg["dimensions"]["connection"]["epsilon_orders"]

    fn = (
        fn.view(batch_size, num_fn, num_eps)
        .permute((2, 0, 1))
        .repeat_interleave(repeats=num_in, dim=1)
        .view(num_eps, batch_size * num_in, num_fn, 1)
    )

    df = torch.empty(
        (num_eps, batch_size * num_in, num_fn, 1), device=device, dtype=dtype
    )

    for k in range(num_eps):
        df[k] = sum(
            torch.matmul(connection[i], fn[k - i]) for i in range(min(k + 1, 2))
        )

    df = df.abs().flatten().cpu().numpy()

    stats(df, "df")

    df = df[df != 0.0]

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(df), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(6.4, 4.8),
        tight_layout=True,
    )

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\sum_k |\partial_k f_i^{(j)}|$")

    ax.hist(
        df,
        bins=bins,
        weights=numpy.full_like(df, 1 / len(df)),
        histtype="step",
    )

    for sx in suffix:
        fig.savefig(
            part / f"{'dist_df' if name is None else name}.{sx}",
            bbox_inches="tight",
        )


if __name__ == "__main__":
    root = pathlib.Path("t331ZZZM")

    cfg = get_cfg(root)

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])
    print("eps", torch.finfo(dtype).eps)

    _, s12, cut, __ = ps_params(cfg)

    part = root / "re"

    data_fn = numpy.loadtxt(part / "testing_output.csv", delimiter=",")
    fn = torch.tensor(data_fn, device=device, dtype=dtype)

    hist_f_cum(part, data_fn)

    data_x = numpy.loadtxt(root / "testing_input.csv", delimiter=",").T
    x = torch.tensor(data_x, device=device, dtype=dtype)

    num_in = cfg["dimensions"]["input"]["variables"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    des_eps = cfg["dimensions"]["connection"]["epsilon_orders"]

    # TODO needs updating to new API
    connection = ConnectionMatrix(des_eps, num_in, num_fn, s12)(x)

    hist_df(part, cfg, x, fn, connection)
