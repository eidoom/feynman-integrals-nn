#!/usr/bin/env python3

import pathlib, json
import torch, numpy, matplotlib.pyplot

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import (
    get_a_data,
    hist_2d,
    hist_a_cum,
    hist_f_cum,
    hist_f_f,
    load_input_csv,
)

from finn.t331ZZZM.connection_matrix import ConnectionMatrix
from finn.t331ZZZM.phase_space import PhaseSpace


if __name__ == "__main__":
    root = pathlib.Path("t331ZZZM")

    cfg = get_cfg(root)

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    _, s12, cut, scut = ps_params(cfg)

    phase_space = PhaseSpace(_, s12, cut, scut, dtype, device)

    n = 1000000
    x = phase_space.test(n, cut_spurious=False)
    data_input = x.cpu().numpy()

    print("s12:", s12)
    print(" " * 4, "s23", " " * 7, "s4")
    print("min", data_input.min(axis=1))
    print("max", data_input.max(axis=1))

    boundary_points = load_input_csv(root / "boundary_input.csv")
    testing_points = []

    hist_2d(
        root,
        data_input,
        boundary_points,
        testing_points,
        "$s_{23}$",
        "$s_{4}$",
        lloc="upper left",
    )

    for p in ("re", "im"):
        part = root / p
        data_fn = numpy.loadtxt(part / "testing_output.csv", delimiter=",")
        hist_f_cum(part, data_fn, log=True)
        # hist_f_f(part, cfg, data_fn, log=False)
        if p == "im":
            chopped = data_fn[abs(data_fn) > numpy.finfo(numpy.float32).eps]
            hist_f_cum(part, chopped, log=False, name="dist_f_cum_chop")

    xx = x[:, :10000]
    data_de = get_a_data(cfg, ConnectionMatrix, xx, s12, verbose=True)

    hist_a_cum(root, data_de, log=False)
