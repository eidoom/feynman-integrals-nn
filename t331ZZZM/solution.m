{{G[s4/s23, 1], G[(-s12 + s4)/s23, 1], G[SubPlus[-(s12/s23)], 1], 
  G[SubPlus[s4/s12], 1], G[0, s4/s23, 1], G[0, (-s12 + s4)/s23, 1], 
  G[0, SubPlus[-(s12/s23)], 1], G[0, SubPlus[s4/s12], 1], 
  G[s4/s23, (-s12 + s4)/s23, 1], G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1], 
  G[0, 0, s4/s23, 1], G[0, 0, (-s12 + s4)/s23, 1], 
  G[0, 0, SubPlus[s4/s12], 1], G[0, s4/s23, s4/s23, 1], 
  G[0, s4/s23, (-s12 + s4)/s23, 1], G[0, s4/s23, SubPlus[-(s12/s23)], 1], 
  G[0, (-s12 + s4)/s23, s4/s23, 1], G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 
   1], G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1], 
  G[0, SubPlus[-(s12/s23)], s4/s23, 1], G[0, SubPlus[s4/s12], 
   SubPlus[s4/s12], 1], G[s4/s23, s4/s23, (-s12 + s4)/s23, 1], 
  G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1], 
  G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1], G[0, 0, 0, s4/s23, 1], 
  G[0, 0, 0, SubPlus[s4/s12], 1], G[0, 0, s4/s23, s4/s23, 1], 
  G[0, 0, s4/s23, (-s12 + s4)/s23, 1], G[0, 0, SubPlus[s4/s12], 
   SubPlus[s4/s12], 1], G[0, s4/s23, 0, (-s12 + s4)/s23, 1], 
  G[0, s4/s23, 0, SubPlus[-(s12/s23)], 1], G[0, s4/s23, s4/s23, s4/s23, 1], 
  G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1], G[0, s4/s23, (-s12 + s4)/s23, 
   s4/s23, 1], G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1], 
  G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1], 
  G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1]}, 
 {{0, (-3*Log[s12/s4])/(2*s12^2 - 2*s12*s4), 
   (5*Pi^2 + 30*G[0, SubPlus[s4/s12], 1] - (12*I)*Pi*Log[s12/s4] - 
     30*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 27*Log[s12/s4]^2 + 
     24*Log[s12/s4]*Log[-(s23/s4)] + 36*Log[s12/s4]*Log[s4])/
    (12*s12^2 - 12*s12*s4), -1/12*((-2*I)*Pi^3 - (48*I)*Pi*G[0, s4/s23, 1] - 
      (12*I)*Pi*G[0, SubPlus[s4/s12], 1] - 
      (48*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 96*G[0, 0, s4/s23, 1] + 
      42*G[0, 0, SubPlus[s4/s12], 1] + 48*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
      42*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
      21*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 48*G[0, (-s12 + s4)/s23, 1]*
       Log[s12/s4] + 24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
      (18*I)*Pi*Log[s12/s4]^2 + 21*Log[s12/s4]^3 + 8*Pi^2*Log[-(s23/s4)] - 
      48*G[0, s4/s23, 1]*Log[-(s23/s4)] + 48*G[0, SubPlus[s4/s12], 1]*
       Log[-(s23/s4)] - 48*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
      36*Log[s12/s4]^2*Log[-(s23/s4)] + 24*Log[s12/s4]*Log[-(s23/s4)]^2 - 
      8*G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
        6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
        6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
         ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
      10*Pi^2*Log[s4] + 60*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
      (24*I)*Pi*Log[s12/s4]*Log[s4] + 54*Log[s12/s4]^2*Log[s4] + 
      48*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 36*Log[s12/s4]*Log[s4]^2 - 
      G[SubPlus[s4/s12], 1]*(7*Pi^2 + 42*G[0, SubPlus[s4/s12], 1] + 
        45*Log[s12/s4]^2 + 12*Log[s12/s4]*((-I)*Pi + 4*Log[-(s23/s4)] + 
          5*Log[s4])))/(s12*(s12 - s4)), 
   -(((-59*Pi^4)/720 - (I/6)*Pi^3*G[SubPlus[s4/s12], 1] + 
      (17*Pi^2*G[SubPlus[s4/s12], 1]^2)/24 - (11*Pi^2*G[0, s4/s23, 1])/3 + 
      G[0, s4/s23, 1]^2 - (Pi^2*G[0, (-s12 + s4)/s23, 1])/3 - 
      (17*Pi^2*G[0, SubPlus[s4/s12], 1])/12 - I*Pi*G[SubPlus[s4/s12], 1]*
       G[0, SubPlus[s4/s12], 1] + (17*G[SubPlus[s4/s12], 1]^2*
        G[0, SubPlus[s4/s12], 1])/4 - 2*G[0, (-s12 + s4)/s23, 1]*
       G[0, SubPlus[s4/s12], 1] + G[0, SubPlus[s4/s12], 1]^2 - 
      (11*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1])/3 + 2*G[0, s4/s23, 1]*
       G[s4/s23, (-s12 + s4)/s23, 1] - (10*I)*Pi*G[0, 0, s4/s23, 1] + 
      (5*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] - 
      (29*G[SubPlus[s4/s12], 1]*G[0, 0, SubPlus[s4/s12], 1])/2 - 
      (2*I)*Pi*G[0, s4/s23, s4/s23, 1] - (10*I)*Pi*
       G[0, s4/s23, (-s12 + s4)/s23, 1] + I*Pi*G[0, SubPlus[s4/s12], 
        SubPlus[s4/s12], 1] - (17*G[SubPlus[s4/s12], 1]*
        G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1])/2 + 
      (2*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
      (10*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
      6*G[0, 0, 0, s4/s23, 1] + (23*G[0, 0, 0, SubPlus[s4/s12], 1])/2 + 
      12*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] + 
      (29*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1])/2 - 
      2*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
      10*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
      (17*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1])/2 - 
      (Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4])/3 + 
      (I/2)*Pi*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 
      (17*G[SubPlus[s4/s12], 1]^3*Log[s12/s4])/12 - 
      (10*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      2*G[SubPlus[s4/s12], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
      (4*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
      2*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
      12*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
      2*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
      10*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
      4*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] - 
      (5*Pi^2*Log[s12/s4]^2)/12 + ((3*I)/2)*Pi*G[SubPlus[s4/s12], 1]*
       Log[s12/s4]^2 + (21*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2)/8 + 
      6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + 2*G[0, SubPlus[s4/s12], 1]*
       Log[s12/s4]^2 - ((7*I)/6)*Pi*Log[s12/s4]^3 - 
      (35*G[SubPlus[s4/s12], 1]*Log[s12/s4]^3)/12 + (15*Log[s12/s4]^4)/16 - 
      (4*Pi^2*G[SubPlus[s4/s12], 1]*Log[-(s23/s4)])/3 - 
      8*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
      6*G[0, 0, s4/s23, 1]*Log[-(s23/s4)] + 12*G[0, 0, SubPlus[s4/s12], 1]*
       Log[-(s23/s4)] - 2*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] - 
      2*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
      8*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
      2*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
      10*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
      (4*Pi^2*Log[s12/s4]*Log[-(s23/s4)])/3 + 4*G[SubPlus[s4/s12], 1]^2*
       Log[s12/s4]*Log[-(s23/s4)] - 2*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*
       Log[-(s23/s4)] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
       Log[-(s23/s4)] + (7*Log[s12/s4]^3*Log[-(s23/s4)])/3 + 
      (2*Pi^2*Log[-(s23/s4)]^2)/3 - 4*G[0, s4/s23, 1]*Log[-(s23/s4)]^2 + 
      4*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]^2 - 
      4*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]^2 - 
      4*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]^2 + 
      3*Log[s12/s4]^2*Log[-(s23/s4)]^2 + (4*Log[s12/s4]*Log[-(s23/s4)]^3)/3 + 
      G[s4/s23, 1]^2*G[(-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)]) + 
      (5*G[(-s12 + s4)/s23, 1]^2*(Pi^2 + 6*G[0, s4/s23, 1] + 
         6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
         6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*Log[-(s23/s4)]))/
       6 - (I/3)*Pi^3*Log[s4] - (7*Pi^2*G[SubPlus[s4/s12], 1]*Log[s4])/6 - 
      (8*I)*Pi*G[0, s4/s23, 1]*Log[s4] - (2*I)*Pi*G[0, SubPlus[s4/s12], 1]*
       Log[s4] - 7*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
      (8*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
      16*G[0, 0, s4/s23, 1]*Log[s4] + 7*G[0, 0, SubPlus[s4/s12], 1]*Log[s4] + 
      8*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
      7*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] + 
      (2*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
      (7*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*Log[s4])/2 + 
      8*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
      4*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
      (3*I)*Pi*Log[s12/s4]^2*Log[s4] - (15*G[SubPlus[s4/s12], 1]*
        Log[s12/s4]^2*Log[s4])/2 + (7*Log[s12/s4]^3*Log[s4])/2 + 
      (4*Pi^2*Log[-(s23/s4)]*Log[s4])/3 - 8*G[0, s4/s23, 1]*Log[-(s23/s4)]*
       Log[s4] + 8*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] - 
      8*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
      8*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 
      6*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 4*Log[s12/s4]*Log[-(s23/s4)]^2*
       Log[s4] + (5*Pi^2*Log[s4]^2)/6 + 5*G[0, SubPlus[s4/s12], 1]*
       Log[s4]^2 - (2*I)*Pi*Log[s12/s4]*Log[s4]^2 - 5*G[SubPlus[s4/s12], 1]*
       Log[s12/s4]*Log[s4]^2 + (9*Log[s12/s4]^2*Log[s4]^2)/2 + 
      4*Log[s12/s4]*Log[-(s23/s4)]*Log[s4]^2 + 2*Log[s12/s4]*Log[s4]^3 - 
      (G[s4/s23, 1]*(15*G[(-s12 + s4)/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 
         6*G[s4/s23, (-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)]) - 
         G[(-s12 + s4)/s23, 1]*(11*Pi^2 - 6*G[0, s4/s23, 1] + 
           12*Log[-(s23/s4)]^2 + (24*I)*Pi*Log[s4] + 24*Log[-(s23/s4)]*
            Log[s4])))/3 + (4*I)*Pi*Zeta[3] - 6*G[SubPlus[s4/s12], 1]*
       Zeta[3] + 7*Log[s12/s4]*Zeta[3] + 4*Log[-(s23/s4)]*Zeta[3] - 
      (G[(-s12 + s4)/s23, 1]*((-30*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 
         36*G[0, 0, s4/s23, 1] + 36*G[0, 0, SubPlus[s4/s12], 1] - 
         6*G[0, s4/s23, s4/s23, 1] + 30*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
         24*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 17*Pi^2*Log[s12/s4] + 
         12*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 30*G[0, (-s12 + s4)/s23, 1]*
          Log[s12/s4] + (18*I)*Pi*Log[s12/s4]^2 + 4*Pi^2*Log[-(s23/s4)] + 
         24*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
         30*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
         18*Log[s12/s4]^2*Log[-(s23/s4)] + 12*Log[s12/s4]*Log[-(s23/s4)]^2 + 
         4*Pi^2*Log[s4] + 24*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
         (24*I)*Pi*Log[s12/s4]*Log[s4] + 24*Log[s12/s4]*Log[-(s23/s4)]*
          Log[s4] + 6*G[0, s4/s23, 1]*((-5*I)*Pi - Log[-(s23/s4)] + 
           4*Log[s4]) - 2*G[SubPlus[s4/s12], 1]*(2*Pi^2 + 
           12*G[0, SubPlus[s4/s12], 1] + 9*Log[s12/s4]^2 + 
           12*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4])) + 12*Zeta[3]))/3)/
     (s12*(-s12 + s4)))}, {1/(s12^2*s23), 
   (-2*(Log[s12/s4] + Log[-(s23/s4)] + Log[s4]))/(s12^2*s23), 
   (5*Pi^2 + 12*G[0, s4/s23, 1] + 36*G[0, SubPlus[s4/s12], 1] - 
     36*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 24*Log[s12/s4]^2 - 
     (12*I)*G[s4/s23, 1]*(Pi - I*Log[-(s23/s4)]) + 
     48*Log[s12/s4]*Log[-(s23/s4)] + 24*Log[-(s23/s4)]^2 + 
     48*Log[s12/s4]*Log[s4] + 48*Log[-(s23/s4)]*Log[s4] + 24*Log[s4]^2)/
    (12*s12^2*s23), -1/6*(-6*Pi^2*G[SubPlus[s4/s12], 1] - 
      (30*I)*Pi*G[0, s4/s23, 1] - 36*G[SubPlus[s4/s12], 1]*
       G[0, SubPlus[s4/s12], 1] - (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 
      48*G[0, 0, s4/s23, 1] + 18*G[0, 0, SubPlus[s4/s12], 1] - 
      12*G[0, s4/s23, s4/s23, 1] + 12*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
      36*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 5*Pi^2*Log[s12/s4] + 
      18*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 12*G[0, (-s12 + s4)/s23, 1]*
       Log[s12/s4] + 18*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
      27*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + 8*Log[s12/s4]^3 + 
      5*Pi^2*Log[-(s23/s4)] - 18*G[0, s4/s23, 1]*Log[-(s23/s4)] + 
      36*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
      12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
      36*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
      24*Log[s12/s4]^2*Log[-(s23/s4)] + 24*Log[s12/s4]*Log[-(s23/s4)]^2 + 
      8*Log[-(s23/s4)]^3 - 6*G[s4/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) - 
      2*G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
        6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
        6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
      5*Pi^2*Log[s4] + 12*G[0, s4/s23, 1]*Log[s4] + 
      36*G[0, SubPlus[s4/s12], 1]*Log[s4] - 36*G[SubPlus[s4/s12], 1]*
       Log[s12/s4]*Log[s4] + 24*Log[s12/s4]^2*Log[s4] + 
      48*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 24*Log[-(s23/s4)]^2*Log[s4] + 
      24*Log[s12/s4]*Log[s4]^2 + 24*Log[-(s23/s4)]*Log[s4]^2 + 8*Log[s4]^3 - 
      2*G[s4/s23, 1]*(2*Pi^2 - 6*G[0, s4/s23, 1] + G[(-s12 + s4)/s23, 1]*
         ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 3*Log[-(s23/s4)]^2 + 
        (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) + 25*Zeta[3])/
     (s12^2*s23), (17*Pi^4 + 180*Pi^2*G[SubPlus[s4/s12], 1]^2 - 
     900*Pi^2*G[0, s4/s23, 1] + 270*G[0, s4/s23, 1]^2 - 
     180*Pi^2*G[0, (-s12 + s4)/s23, 1] - 1440*G[0, s4/s23, 1]*
      G[0, (-s12 + s4)/s23, 1] + 1080*G[SubPlus[s4/s12], 1]^2*
      G[0, SubPlus[s4/s12], 1] - 360*G[0, s4/s23, 1]*
      G[0, SubPlus[s4/s12], 1] - 1080*G[0, (-s12 + s4)/s23, 1]*
      G[0, SubPlus[s4/s12], 1] + 270*G[0, SubPlus[s4/s12], 1]^2 - 
     300*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] + 360*G[0, s4/s23, 1]*
      G[s4/s23, (-s12 + s4)/s23, 1] - 360*G[0, SubPlus[s4/s12], 1]*
      G[s4/s23, (-s12 + s4)/s23, 1] - (2880*I)*Pi*G[0, 0, s4/s23, 1] - 
     1620*G[SubPlus[s4/s12], 1]*G[0, 0, SubPlus[s4/s12], 1] - 
     (900*I)*Pi*G[0, s4/s23, s4/s23, 1] - 
     (360*I)*Pi*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
     (1440*I)*Pi*G[0, (-s12 + s4)/s23, s4/s23, 1] - 
     2160*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     (360*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
     3960*G[0, 0, 0, s4/s23, 1] + 540*G[0, 0, 0, SubPlus[s4/s12], 1] + 
     1440*G[0, 0, s4/s23, s4/s23, 1] + 2880*G[0, 0, s4/s23, (-s12 + s4)/s23, 
       1] + 1620*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
     1440*G[0, s4/s23, 0, (-s12 + s4)/s23, 1] + 
     720*G[0, s4/s23, s4/s23, s4/s23, 1] - 
     720*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
     360*G[0, s4/s23, (-s12 + s4)/s23, s4/s23, 1] + 
     360*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
     2160*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     360*Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 360*G[SubPlus[s4/s12], 1]^3*
      Log[s12/s4] + 360*G[SubPlus[s4/s12], 1]*G[0, s4/s23, 1]*Log[s12/s4] - 
     (1800*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     1080*G[SubPlus[s4/s12], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     1080*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
     (360*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     360*G[SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     2880*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     540*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
     360*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[s12/s4] + 
     360*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     540*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] + 
     150*Pi^2*Log[s12/s4]^2 + 945*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2 - 
     180*G[0, s4/s23, 1]*Log[s12/s4]^2 + 540*G[0, (-s12 + s4)/s23, 1]*
      Log[s12/s4]^2 + 270*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 
     630*G[SubPlus[s4/s12], 1]*Log[s12/s4]^3 + 120*Log[s12/s4]^4 - 
     (120*I)*G[s4/s23, 1]^3*(Pi - I*Log[-(s23/s4)]) - 
     360*Pi^2*G[SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
     2160*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
     1080*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
     1620*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] + 
     360*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
     1440*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[-(s23/s4)] + 
     2160*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
     360*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
     300*Pi^2*Log[s12/s4]*Log[-(s23/s4)] + 1080*G[SubPlus[s4/s12], 1]^2*
      Log[s12/s4]*Log[-(s23/s4)] - 1080*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*
      Log[-(s23/s4)] + 1080*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*
      Log[-(s23/s4)] - 360*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4]*
      Log[-(s23/s4)] - 1620*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
      Log[-(s23/s4)] + 480*Log[s12/s4]^3*Log[-(s23/s4)] + 
     150*Pi^2*Log[-(s23/s4)]^2 - 540*G[0, s4/s23, 1]*Log[-(s23/s4)]^2 + 
     1080*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]^2 - 
     360*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]^2 - 
     1080*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]^2 + 
     720*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 480*Log[s12/s4]*Log[-(s23/s4)]^3 + 
     120*Log[-(s23/s4)]^4 + 30*G[(-s12 + s4)/s23, 1]^2*
      (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
       (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
       6*Log[s12/s4]*Log[-(s23/s4)]) - 360*Pi^2*G[SubPlus[s4/s12], 1]*
      Log[s4] - (1800*I)*Pi*G[0, s4/s23, 1]*Log[s4] - 
     2160*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
     (720*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
     2880*G[0, 0, s4/s23, 1]*Log[s4] + 1080*G[0, 0, SubPlus[s4/s12], 1]*
      Log[s4] - 720*G[0, s4/s23, s4/s23, 1]*Log[s4] + 
     720*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
     2160*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] + 
     300*Pi^2*Log[s12/s4]*Log[s4] + 1080*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*
      Log[s4] + 720*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
     1080*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
     1620*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*Log[s4] + 
     480*Log[s12/s4]^3*Log[s4] + 300*Pi^2*Log[-(s23/s4)]*Log[s4] - 
     1080*G[0, s4/s23, 1]*Log[-(s23/s4)]*Log[s4] + 
     2160*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] - 
     720*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
     2160*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 
     1440*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 1440*Log[s12/s4]*
      Log[-(s23/s4)]^2*Log[s4] + 480*Log[-(s23/s4)]^3*Log[s4] + 
     150*Pi^2*Log[s4]^2 + 360*G[0, s4/s23, 1]*Log[s4]^2 + 
     1080*G[0, SubPlus[s4/s12], 1]*Log[s4]^2 - 1080*G[SubPlus[s4/s12], 1]*
      Log[s12/s4]*Log[s4]^2 + 720*Log[s12/s4]^2*Log[s4]^2 + 
     1440*Log[s12/s4]*Log[-(s23/s4)]*Log[s4]^2 + 720*Log[-(s23/s4)]^2*
      Log[s4]^2 + 480*Log[s12/s4]*Log[s4]^3 + 480*Log[-(s23/s4)]*Log[s4]^3 + 
     120*Log[s4]^4 - 60*G[s4/s23, 1]^2*(2*Pi^2 - 6*G[0, s4/s23, 1] + 
       G[(-s12 + s4)/s23, 1]*((-6*I)*Pi - 6*Log[-(s23/s4)]) + 
       3*Log[-(s23/s4)]^2 + (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) + 
     30*G[s4/s23, 1]*(I*Pi^3 + (12*I)*Pi*G[0, s4/s23, 1] + 
       (12*I)*Pi*G[0, SubPlus[s4/s12], 1] - (12*I)*Pi*
        G[s4/s23, (-s12 + s4)/s23, 1] - 48*G[0, 0, s4/s23, 1] + 
       24*G[0, 0, SubPlus[s4/s12], 1] - 24*G[0, s4/s23, s4/s23, 1] + 
       12*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
       12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 2*Pi^2*Log[s12/s4] + 
       6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 12*G[0, (-s12 + s4)/s23, 1]*
        Log[s12/s4] - 12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
       (6*I)*Pi*Log[s12/s4]^2 + G[(-s12 + s4)/s23, 1]^2*
        ((-6*I)*Pi - 6*Log[-(s23/s4)]) + Pi^2*Log[-(s23/s4)] + 
       36*G[0, s4/s23, 1]*Log[-(s23/s4)] + 12*G[0, SubPlus[s4/s12], 1]*
        Log[-(s23/s4)] - 12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
       6*Log[s12/s4]^2*Log[-(s23/s4)] - 4*Log[-(s23/s4)]^3 - 
       2*G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
         6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])) - 8*Pi^2*Log[s4] + 
       24*G[0, s4/s23, 1]*Log[s4] - 12*Log[-(s23/s4)]^2*Log[s4] - 
       (12*I)*Pi*Log[s4]^2 - 12*Log[-(s23/s4)]*Log[s4]^2 + 
       4*G[(-s12 + s4)/s23, 1]*(-6*G[0, s4/s23, 1] + 3*Log[-(s23/s4)]^2 + 
         2*Pi*(Pi + (3*I)*Log[s4]) + 6*Log[-(s23/s4)]*Log[s4]) - 
       36*Zeta[3]) + 60*G[(-s12 + s4)/s23, 1]*
      ((6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 
       18*G[0, 0, SubPlus[s4/s12], 1] + 12*G[0, s4/s23, s4/s23, 1] - 
       6*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
       12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 7*Pi^2*Log[s12/s4] - 
       6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 6*G[0, (-s12 + s4)/s23, 1]*
        Log[s12/s4] - (9*I)*Pi*Log[s12/s4]^2 - 2*Pi^2*Log[-(s23/s4)] - 
       12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
       6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       9*Log[s12/s4]^2*Log[-(s23/s4)] - 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
       (6*I)*G[0, s4/s23, 1]*(Pi + I*Log[-(s23/s4)] + (2*I)*Log[s4]) - 
       2*Pi^2*Log[s4] - 12*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
       (12*I)*Pi*Log[s12/s4]*Log[s4] - 12*Log[s12/s4]*Log[-(s23/s4)]*
        Log[s4] + G[SubPlus[s4/s12], 1]*(2*Pi^2 + 
         12*G[0, SubPlus[s4/s12], 1] + 9*Log[s12/s4]^2 + 
         12*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4])) - 6*Zeta[3]) + 
     540*G[SubPlus[s4/s12], 1]*Zeta[3] + 1500*Log[s12/s4]*Zeta[3] + 
     1500*Log[-(s23/s4)]*Zeta[3] + 1500*Log[s4]*Zeta[3])/(180*s12^2*s23)}, 
  {0, Log[s12/s4]/(s12*(-s12 + s4)), 
   (Log[s12/s4]*((-4*I)*Pi + 3*Log[s12/s4] + 4*Log[s4]))/(2*s12*(s12 - s4)), 
   -1/6*(Log[s12/s4]*(-Pi^2 + 7*Log[s12/s4]^2 - 12*(Pi + I*Log[s4])^2 + 
       18*Log[s12/s4]*((-I)*Pi + Log[s4])))/(s12*(s12 - s4)), 
   -1/24*(Log[s12/s4]*(15*Log[s12/s4]^3 - 6*Log[s12/s4]*
        (Pi^2 + 12*(Pi + I*Log[s4])^2) + (8*I)*Pi^2*(Pi + I*Log[s4]) + 
       56*Log[s12/s4]^2*((-I)*Pi + Log[s4]) + 32*((-I)*Pi + Log[s4])^3 + 
       112*Zeta[3]))/(s12*(-s12 + s4))}, {1/(4*s12*s23), 
   -1/2*(Log[s12/s4] + Log[-(s23/s4)] + Log[s4])/(s12*s23), 
   (7*Pi^2 + 36*G[0, SubPlus[s4/s12], 1] - 36*G[SubPlus[s4/s12], 1]*
      Log[s12/s4] + 12*Log[s12/s4]^2 + 24*Log[s12/s4]*Log[-(s23/s4)] + 
     12*Log[-(s23/s4)]^2 + 24*Log[s12/s4]*Log[s4] + 
     24*Log[-(s23/s4)]*Log[s4] + 12*Log[s4]^2)/(24*s12*s23), 
   -1/12*(36*G[0, 0, SubPlus[s4/s12], 1] + 
      54*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 7*Pi^2*Log[s12/s4] + 
      27*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 4*Log[s12/s4]^3 + 
      7*Pi^2*Log[-(s23/s4)] + 36*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
      12*Log[s12/s4]^2*Log[-(s23/s4)] + 12*Log[s12/s4]*Log[-(s23/s4)]^2 + 
      4*Log[-(s23/s4)]^3 + 7*Pi^2*Log[s4] + 36*G[0, SubPlus[s4/s12], 1]*
       Log[s4] + 12*Log[s12/s4]^2*Log[s4] + 24*Log[s12/s4]*Log[-(s23/s4)]*
       Log[s4] + 12*Log[-(s23/s4)]^2*Log[s4] + 12*Log[s12/s4]*Log[s4]^2 + 
      12*Log[-(s23/s4)]*Log[s4]^2 + 4*Log[s4]^3 - 9*G[SubPlus[s4/s12], 1]*
       (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 2*Log[s12/s4]^2 + 
        4*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4])) - 28*Zeta[3])/(s12*s23), 
   (83*Pi^4 + 144*Pi^2*G[0, (-s12 + s4)/s23, 1] - 864*G[0, s4/s23, 1]*
      G[0, (-s12 + s4)/s23, 1] - 72*Pi^2*G[0, SubPlus[s4/s12], 1] + 
     864*G[0, (-s12 + s4)/s23, 1]*G[0, SubPlus[s4/s12], 1] - 
     432*G[0, SubPlus[s4/s12], 1]^2 + (864*I)*Pi*
      G[0, s4/s23, (-s12 + s4)/s23, 1] + (864*I)*Pi*G[0, (-s12 + s4)/s23, 
       s4/s23, 1] + 1728*G[0, 0, 0, SubPlus[s4/s12], 1] + 
     864*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
     864*G[0, s4/s23, 0, (-s12 + s4)/s23, 1] + 
     3024*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     504*G[SubPlus[s4/s12], 1]^3*Log[s12/s4] + 
     1728*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] + 
     168*Pi^2*Log[s12/s4]^2 + 48*Log[s12/s4]^4 + 
     1728*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
     864*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
     864*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[-(s23/s4)] + 
     2592*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
     336*Pi^2*Log[s12/s4]*Log[-(s23/s4)] + 192*Log[s12/s4]^3*Log[-(s23/s4)] + 
     168*Pi^2*Log[-(s23/s4)]^2 + 864*G[0, SubPlus[s4/s12], 1]*
      Log[-(s23/s4)]^2 + 288*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 
     192*Log[s12/s4]*Log[-(s23/s4)]^3 + 48*Log[-(s23/s4)]^4 + 
     1728*G[0, 0, SubPlus[s4/s12], 1]*Log[s4] + 
     2592*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] + 
     336*Pi^2*Log[s12/s4]*Log[s4] + 192*Log[s12/s4]^3*Log[s4] + 
     336*Pi^2*Log[-(s23/s4)]*Log[s4] + 1728*G[0, SubPlus[s4/s12], 1]*
      Log[-(s23/s4)]*Log[s4] + 576*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 
     576*Log[s12/s4]*Log[-(s23/s4)]^2*Log[s4] + 192*Log[-(s23/s4)]^3*
      Log[s4] + 168*Pi^2*Log[s4]^2 + 864*G[0, SubPlus[s4/s12], 1]*Log[s4]^2 + 
     288*Log[s12/s4]^2*Log[s4]^2 + 576*Log[s12/s4]*Log[-(s23/s4)]*Log[s4]^2 + 
     288*Log[-(s23/s4)]^2*Log[s4]^2 + 192*Log[s12/s4]*Log[s4]^3 + 
     192*Log[-(s23/s4)]*Log[s4]^3 + 48*Log[s4]^4 + 36*G[SubPlus[s4/s12], 1]^2*
      (7*Pi^2 + 42*G[0, SubPlus[s4/s12], 1] + 18*Log[s12/s4]^2 + 
       36*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4])) - 
     72*G[SubPlus[s4/s12], 1]*(12*G[0, 0, SubPlus[s4/s12], 1] + 
       42*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 5*Pi^2*Log[s12/s4] + 
       12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 12*G[0, SubPlus[s4/s12], 1]*
        Log[s12/s4] + 4*Log[s12/s4]^3 + 6*Pi^2*Log[-(s23/s4)] + 
       36*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 12*Log[s12/s4]^2*
        Log[-(s23/s4)] + 12*Log[s12/s4]*Log[-(s23/s4)]^2 + 6*Pi^2*Log[s4] + 
       36*G[0, SubPlus[s4/s12], 1]*Log[s4] + 12*Log[s12/s4]^2*Log[s4] + 
       24*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 12*Log[s12/s4]*Log[s4]^2 - 
       30*Zeta[3]) - 1344*Log[s12/s4]*Zeta[3] - 1344*Log[-(s23/s4)]*Zeta[3] - 
     1344*Log[s4]*Zeta[3] + 144*G[(-s12 + s4)/s23, 1]*
      ((-I)*Pi^3 - (6*I)*Pi*G[0, SubPlus[s4/s12], 1] + 
       12*G[0, 0, s4/s23, 1] - 12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
       6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + G[0, s4/s23, 1]*
        ((-6*I)*Pi - 6*Log[-(s23/s4)]) - Pi^2*Log[-(s23/s4)] - 
       6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 2*G[SubPlus[s4/s12], 1]*
        (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 3*Log[s12/s4]*
          (I*Pi + Log[-(s23/s4)])) + 12*Zeta[3]))/(288*s12*s23)}, 
  {0, -1/2*Log[s12/s4]/(s12 - s4), (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] - 
     6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 2*Log[s12/s4]^2 + 
     4*Log[s12/s4]*Log[-(s23/s4)] + 4*Log[s12/s4]*Log[s4])/(4*s12 - 4*s4), 
   (4*Pi^2*G[(-s12 + s4)/s23, 1] + 5*Pi^2*G[SubPlus[s4/s12], 1] + 
     24*G[(-s12 + s4)/s23, 1]*G[0, s4/s23, 1] + 24*G[(-s12 + s4)/s23, 1]*
      G[0, SubPlus[s4/s12], 1] + 30*G[SubPlus[s4/s12], 1]*
      G[0, SubPlus[s4/s12], 1] + 12*G[0, 0, SubPlus[s4/s12], 1] - 
     24*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
     30*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 5*Pi^2*Log[s12/s4] - 
     24*G[(-s12 + s4)/s23, 1]*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 
     15*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 24*G[0, (-s12 + s4)/s23, 1]*
      Log[s12/s4] - 24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
     18*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 4*Log[s12/s4]^3 + 
     12*Log[s12/s4]*(Pi - I*Log[-(s23/s4)])^2 - 
     4*Pi^2*(I*Pi + Log[-(s23/s4)]) - 24*G[s4/s23, 1]*G[(-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) - 36*G[0, SubPlus[s4/s12], 1]*
      (I*Pi + Log[-(s23/s4)]) + 24*G[s4/s23, (-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + 24*G[(-s12 + s4)/s23, 1]*Log[s12/s4]*
      (I*Pi + Log[-(s23/s4)]) + 36*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
      (I*Pi + Log[-(s23/s4)]) - 12*Log[s12/s4]^2*(I*Pi + Log[-(s23/s4)]) - 
     (2*(s23 + s4)*(-3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 
        6*(I*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 2*G[0, 0, s4/s23, 1] + 
          2*G[0, 0, SubPlus[s4/s12], 1] - G[0, s4/s23, (-s12 + s4)/s23, 1] - 
          G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
          G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + G[0, s4/s23, 1]*
           ((-I)*Pi - Log[-(s23/s4)]) + G[0, SubPlus[s4/s12], 1]*
           ((-I)*Pi - Log[s12/s4] - Log[-(s23/s4)]) + 
          G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]) + 
        G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
          6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
           ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
        G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
          6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))))/(s12 + s23) + 
     (6*I)*Pi^2*(Pi + I*Log[s4]) + (36*I)*G[0, SubPlus[s4/s12], 1]*
      (Pi + I*Log[s4]) + (12*I)*Log[s12/s4]^2*(Pi + I*Log[s4]) - 
     24*Log[s12/s4]*(Pi - I*Log[-(s23/s4)])*(Pi + I*Log[s4]) + 
     12*Log[s12/s4]*(Pi + I*Log[s4])^2 + 36*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
      ((-I)*Pi + Log[s4]) + 6*Zeta[3])/(12*(s12 - s4)), 
   ((11*Pi^4)/180 - (Pi^2*G[s4/s23, 1]*G[(-s12 + s4)/s23, 1])/3 + 
     (Pi^2*G[(-s12 + s4)/s23, 1]^2)/6 + (2*Pi^2*G[(-s12 + s4)/s23, 1]*
       G[SubPlus[-(s12/s23)], 1])/3 + (Pi^2*G[(-s12 + s4)/s23, 1]*
       G[SubPlus[s4/s12], 1])/3 + (2*Pi^2*G[SubPlus[-(s12/s23)], 1]*
       G[SubPlus[s4/s12], 1])/3 + (3*Pi^2*G[SubPlus[s4/s12], 1]^2)/8 - 
     2*G[s4/s23, 1]*G[(-s12 + s4)/s23, 1]*G[0, s4/s23, 1] + 
     G[(-s12 + s4)/s23, 1]^2*G[0, s4/s23, 1] + 4*G[(-s12 + s4)/s23, 1]*
      G[SubPlus[-(s12/s23)], 1]*G[0, s4/s23, 1] - 
     (Pi^2*G[0, (-s12 + s4)/s23, 1])/6 - G[0, s4/s23, 1]*
      G[0, (-s12 + s4)/s23, 1] + (3*G[0, SubPlus[s4/s12], 1]^2)/2 + 
     (Pi^2*G[s4/s23, (-s12 + s4)/s23, 1])/3 + 2*G[0, s4/s23, 1]*
      G[s4/s23, (-s12 + s4)/s23, 1] - 
     (2*Pi^2*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1])/3 - 
     4*G[0, s4/s23, 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
     2*G[(-s12 + s4)/s23, 1]*G[0, 0, s4/s23, 1] + 8*G[SubPlus[-(s12/s23)], 1]*
      G[0, 0, s4/s23, 1] - 4*G[(-s12 + s4)/s23, 1]*G[0, 0, SubPlus[s4/s12], 
       1] + 8*G[SubPlus[-(s12/s23)], 1]*G[0, 0, SubPlus[s4/s12], 1] - 
     7*G[SubPlus[s4/s12], 1]*G[0, 0, SubPlus[s4/s12], 1] + 
     2*G[(-s12 + s4)/s23, 1]*G[0, s4/s23, s4/s23, 1] - 
     2*G[(-s12 + s4)/s23, 1]*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
     4*G[SubPlus[-(s12/s23)], 1]*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
     2*G[(-s12 + s4)/s23, 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     4*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     (9*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1])/2 + 
     18*G[0, 0, 0, SubPlus[s4/s12], 1] + 4*G[0, 0, s4/s23, (-s12 + s4)/s23, 
       1] + 7*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
     G[0, s4/s23, 0, (-s12 + s4)/s23, 1] + 
     4*G[0, s4/s23, 0, SubPlus[-(s12/s23)], 1] - 
     2*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
     2*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
     4*G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
     (9*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1])/2 - 
     (Pi^2*G[(-s12 + s4)/s23, 1]*Log[s12/s4])/3 - 
     (7*Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4])/12 - 
     G[(-s12 + s4)/s23, 1]^2*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 
     4*G[(-s12 + s4)/s23, 1]*G[SubPlus[-(s12/s23)], 1]*G[SubPlus[s4/s12], 1]*
      Log[s12/s4] - G[(-s12 + s4)/s23, 1]*G[SubPlus[s4/s12], 1]^2*
      Log[s12/s4] - 2*G[SubPlus[-(s12/s23)], 1]*G[SubPlus[s4/s12], 1]^2*
      Log[s12/s4] - (3*G[SubPlus[s4/s12], 1]^3*Log[s12/s4])/4 - 
     2*G[(-s12 + s4)/s23, 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     4*G[SubPlus[-(s12/s23)], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     G[SubPlus[s4/s12], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     4*G[SubPlus[s4/s12], 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
      Log[s12/s4] + 4*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     8*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
     2*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     4*G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
     4*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] + 
     (5*Pi^2*Log[s12/s4]^2)/12 + 2*G[(-s12 + s4)/s23, 1]*
      G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + 
     (5*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2)/4 + 2*G[0, (-s12 + s4)/s23, 1]*
      Log[s12/s4]^2 - G[SubPlus[s4/s12], 1]*Log[s12/s4]^3 + Log[s12/s4]^4/6 - 
     4*G[0, SubPlus[-(s12/s23)], 1]*(G[0, s4/s23, 1] + 
       G[SubPlus[s4/s12], 1]*Log[s12/s4]) + 2*G[s4/s23, (-s12 + s4)/s23, 1]*
      (Pi - I*Log[-(s23/s4)])^2 + 2*G[(-s12 + s4)/s23, 1]*Log[s12/s4]*
      (Pi - I*Log[-(s23/s4)])^2 + 3*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
      (Pi - I*Log[-(s23/s4)])^2 - (Pi^2*G[(-s12 + s4)/s23, 1]*
       (I*Pi + Log[-(s23/s4)]))/2 + G[s4/s23, 1]^2*G[(-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) - G[s4/s23, 1]*G[(-s12 + s4)/s23, 1]^2*
      (I*Pi + Log[-(s23/s4)]) - 4*G[s4/s23, 1]*G[(-s12 + s4)/s23, 1]*
      G[SubPlus[-(s12/s23)], 1]*(I*Pi + Log[-(s23/s4)]) - 
     (5*Pi^2*G[SubPlus[s4/s12], 1]*(I*Pi + Log[-(s23/s4)]))/6 - 
     G[(-s12 + s4)/s23, 1]*G[0, s4/s23, 1]*(I*Pi + Log[-(s23/s4)]) - 
     4*G[SubPlus[-(s12/s23)], 1]*G[0, s4/s23, 1]*(I*Pi + Log[-(s23/s4)]) - 
     2*G[s4/s23, 1]*G[s4/s23, (-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)]) + 
     2*G[(-s12 + s4)/s23, 1]*G[s4/s23, (-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + 4*G[SubPlus[-(s12/s23)], 1]*
      G[s4/s23, (-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)]) + 
     4*G[s4/s23, 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
      (I*Pi + Log[-(s23/s4)]) - 2*G[0, 0, SubPlus[s4/s12], 1]*
      (I*Pi + Log[-(s23/s4)]) + G[0, s4/s23, (-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + 4*G[0, s4/s23, SubPlus[-(s12/s23)], 1]*
      (I*Pi + Log[-(s23/s4)]) + G[0, (-s12 + s4)/s23, s4/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + 4*G[0, SubPlus[-(s12/s23)], s4/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + 5*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*
      (I*Pi + Log[-(s23/s4)]) + 2*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) - 2*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 
       1]*(I*Pi + Log[-(s23/s4)]) - 4*G[s4/s23, (-s12 + s4)/s23, 
       SubPlus[-(s12/s23)], 1]*(I*Pi + Log[-(s23/s4)]) + 
     (Pi^2*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))/2 + 
     G[(-s12 + s4)/s23, 1]^2*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]) + 
     4*G[(-s12 + s4)/s23, 1]*G[SubPlus[-(s12/s23)], 1]*Log[s12/s4]*
      (I*Pi + Log[-(s23/s4)]) + 3*G[(-s12 + s4)/s23, 1]*G[SubPlus[s4/s12], 1]*
      Log[s12/s4]*(I*Pi + Log[-(s23/s4)]) + 4*G[SubPlus[-(s12/s23)], 1]*
      G[SubPlus[s4/s12], 1]*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]) + 
     (5*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))/2 - 
     4*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4]*
      (I*Pi + Log[-(s23/s4)]) - 2*G[(-s12 + s4)/s23, 1]*Log[s12/s4]^2*
      (I*Pi + Log[-(s23/s4)]) - 3*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
      (I*Pi + Log[-(s23/s4)]) + (2*Log[s12/s4]^3*(I*Pi + Log[-(s23/s4)]))/3 + 
     (Pi^2*(I*Pi + Log[-(s23/s4)])^2)/3 + 2*G[s4/s23, 1]*
      G[(-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)])^2 + 
     Log[s12/s4]^2*(I*Pi + Log[-(s23/s4)])^2 + 
     (2*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])^3)/3 + 
     ((2*I)/3)*Pi^2*G[(-s12 + s4)/s23, 1]*(Pi + I*Log[s4]) + 
     ((5*I)/6)*Pi^2*G[SubPlus[s4/s12], 1]*(Pi + I*Log[s4]) + 
     (4*I)*G[(-s12 + s4)/s23, 1]*G[0, s4/s23, 1]*(Pi + I*Log[s4]) + 
     (2*I)*G[0, 0, SubPlus[s4/s12], 1]*(Pi + I*Log[s4]) + 
     (3*I)*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*(Pi + I*Log[s4]) + 
     (2*Pi^2*(Pi - I*Log[-(s23/s4)])*(Pi + I*Log[s4]))/3 + 
     4*G[s4/s23, 1]*G[(-s12 + s4)/s23, 1]*(Pi - I*Log[-(s23/s4)])*
      (Pi + I*Log[s4]) - 4*G[s4/s23, (-s12 + s4)/s23, 1]*
      (Pi - I*Log[-(s23/s4)])*(Pi + I*Log[s4]) - 4*G[(-s12 + s4)/s23, 1]*
      Log[s12/s4]*(Pi - I*Log[-(s23/s4)])*(Pi + I*Log[s4]) - 
     6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*(Pi - I*Log[-(s23/s4)])*
      (Pi + I*Log[s4]) + 2*Log[s12/s4]^2*(Pi - I*Log[-(s23/s4)])*
      (Pi + I*Log[s4]) - (Pi^2*(Pi + I*Log[s4])^2)/2 + 
     3*G[SubPlus[s4/s12], 1]*Log[s12/s4]*(Pi + I*Log[s4])^2 + 
     4*G[0, s4/s23, (-s12 + s4)/s23, 1]*((-I)*Pi + Log[s4]) + 
     5*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*((-I)*Pi + Log[s4]) + 
     (5*Pi^2*Log[s12/s4]*((-I)*Pi + Log[s4]))/6 + 4*G[(-s12 + s4)/s23, 1]*
      G[SubPlus[s4/s12], 1]*Log[s12/s4]*((-I)*Pi + Log[s4]) + 
     (5*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*((-I)*Pi + Log[s4]))/2 + 
     4*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*((-I)*Pi + Log[s4]) + 
     (2*Log[s12/s4]^3*((-I)*Pi + Log[s4]))/3 + 
     2*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])^2*((-I)*Pi + Log[s4]) + 
     Log[s12/s4]^2*((-I)*Pi + Log[s4])^2 + 2*Log[s12/s4]*
      (I*Pi + Log[-(s23/s4)])*((-I)*Pi + Log[s4])^2 + 
     (2*Log[s12/s4]*((-I)*Pi + Log[s4])^3)/3 + 
     (G[0, SubPlus[s4/s12], 1]*(Pi^2 + 12*G[(-s12 + s4)/s23, 1]^2 + 
        27*G[SubPlus[s4/s12], 1]^2 - 12*G[0, (-s12 + s4)/s23, 1] + 
        48*G[0, SubPlus[-(s12/s23)], 1] - 48*G[(-s12 + s4)/s23, 
          SubPlus[-(s12/s23)], 1] + 12*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        24*Log[s12/s4]^2 + 48*G[SubPlus[-(s12/s23)], 1]*
         ((-I)*Pi + G[SubPlus[s4/s12], 1] - Log[s12/s4] - Log[-(s23/s4)]) - 
        60*G[SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 48*Log[s12/s4]*
         Log[-(s23/s4)] + 36*Log[-(s23/s4)]^2 + 12*G[(-s12 + s4)/s23, 1]*
         (I*Pi + 4*G[SubPlus[-(s12/s23)], 1] + 2*G[SubPlus[s4/s12], 1] - 
          3*Log[-(s23/s4)] - 4*Log[s4]) - 60*G[SubPlus[s4/s12], 1]*Log[s4] + 
        48*Log[s12/s4]*Log[s4] + 72*Log[-(s23/s4)]*Log[s4] + 36*Log[s4]^2))/
      12 - 2*G[(-s12 + s4)/s23, 1]*Zeta[3] - 
     (5*G[SubPlus[s4/s12], 1]*Zeta[3])/2 + (13*Log[s12/s4]*Zeta[3])/3 + 
     I*(Pi + I*Log[s4])*Zeta[3] + 
     ((s23 + s4)*(-(G[(-s12 + s4)/s23, 1]^2*(Pi^2 + 6*G[0, s4/s23, 1] + 
           6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
           6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
            ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)])) + 
        2*G[(-s12 + s4)/s23, 1]*((-I)*Pi^3 - (12*I)*Pi*G[0, s4/s23, 1] - 
          (6*I)*Pi*G[0, SubPlus[s4/s12], 1] - (6*I)*Pi*
           G[s4/s23, (-s12 + s4)/s23, 1] + 12*G[0, 0, s4/s23, 1] + 
          12*G[0, 0, SubPlus[s4/s12], 1] - 6*G[0, s4/s23, s4/s23, 1] + 
          6*G[0, s4/s23, (-s12 + s4)/s23, 1] + 7*Pi^2*Log[s12/s4] + 
          (6*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
          6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + (6*I)*Pi*Log[s12/s4]^2 - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + G[s4/s23, 1]^2*
           ((-3*I)*Pi - 3*Log[-(s23/s4)]) + Pi^2*Log[-(s23/s4)] + 
          6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
          6*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 - 
          4*G[SubPlus[-(s12/s23)], 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
            6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
            6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*
             Log[-(s23/s4)]) + 2*Pi^2*Log[s4] + 12*G[0, s4/s23, 1]*Log[s4] + 
          12*G[0, SubPlus[s4/s12], 1]*Log[s4] + (12*I)*Pi*Log[s12/s4]*
           Log[s4] - 12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
          12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + G[s4/s23, 1]*
           (-5*Pi^2 + 6*G[0, s4/s23, 1] - 6*Log[-(s23/s4)]^2 + 
            24*G[SubPlus[-(s12/s23)], 1]*(I*Pi + Log[-(s23/s4)]) - 
            (12*I)*Pi*Log[s4] - 12*Log[-(s23/s4)]*Log[s4]) + 12*Zeta[3]) + 
        2*(3*G[0, s4/s23, 1]^2 + Pi^2*G[0, (-s12 + s4)/s23, 1] - 
          8*Pi^2*G[0, SubPlus[s4/s12], 1] + (24*I)*Pi*G[SubPlus[-(s12/s23)], 
            1]*G[0, SubPlus[s4/s12], 1] + 6*G[0, (-s12 + s4)/s23, 1]*
           G[0, SubPlus[s4/s12], 1] - 24*G[0, SubPlus[-(s12/s23)], 1]*
           G[0, SubPlus[s4/s12], 1] - 9*G[0, SubPlus[s4/s12], 1]^2 + 
          5*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] + (6*I)*Pi*G[s4/s23, 1]*
           G[s4/s23, (-s12 + s4)/s23, 1] - (24*I)*Pi*G[SubPlus[-(s12/s23)], 
            1]*G[s4/s23, (-s12 + s4)/s23, 1] + 4*Pi^2*G[(-s12 + s4)/s23, 
            SubPlus[-(s12/s23)], 1] - (24*I)*Pi*G[s4/s23, 1]*
           G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
          24*G[0, SubPlus[s4/s12], 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 
            1] - (12*I)*Pi*G[0, 0, s4/s23, 1] - 48*G[SubPlus[-(s12/s23)], 1]*
           G[0, 0, s4/s23, 1] + (12*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] - 
          48*G[SubPlus[-(s12/s23)], 1]*G[0, 0, SubPlus[s4/s12], 1] - 
          (6*I)*Pi*G[0, s4/s23, s4/s23, 1] + (12*I)*Pi*G[0, s4/s23, 
            (-s12 + s4)/s23, 1] + 24*G[SubPlus[-(s12/s23)], 1]*
           G[0, s4/s23, (-s12 + s4)/s23, 1] - (24*I)*Pi*G[0, s4/s23, 
            SubPlus[-(s12/s23)], 1] - (24*I)*Pi*G[0, SubPlus[-(s12/s23)], 
            s4/s23, 1] + (6*I)*Pi*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 
            1] - (6*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
          (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
          (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
          72*G[0, 0, 0, SubPlus[s4/s12], 1] - 12*G[0, 0, s4/s23, 
            (-s12 + s4)/s23, 1] - 12*G[0, 0, SubPlus[s4/s12], 
            SubPlus[s4/s12], 1] - 24*G[0, s4/s23, 0, SubPlus[-(s12/s23)], 
            1] + 6*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
          6*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
          24*G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
          (12*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
          (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
          (24*I)*Pi*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
          12*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
          36*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
          6*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
          24*G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
          18*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] - 
          6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 - 
          6*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*
           Log[-(s23/s4)] + 6*G[s4/s23, 1]*G[s4/s23, (-s12 + s4)/s23, 1]*
           Log[-(s23/s4)] - 24*G[SubPlus[-(s12/s23)], 1]*
           G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
          24*G[s4/s23, 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[-(s23/s4)] + 12*G[0, 0, s4/s23, 1]*Log[-(s23/s4)] + 
          36*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] - 
          24*G[0, s4/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] - 
          24*G[0, SubPlus[-(s12/s23)], s4/s23, 1]*Log[-(s23/s4)] - 
          6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
          6*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
          24*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[-(s23/s4)] - 24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*
           Log[-(s23/s4)] + 24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[s12/s4]*Log[-(s23/s4)] - 6*G[0, SubPlus[s4/s12], 1]*
           Log[-(s23/s4)]^2 + 6*G[s4/s23, (-s12 + s4)/s23, 1]*
           Log[-(s23/s4)]^2 - (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
          (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
          24*G[0, 0, s4/s23, 1]*Log[s4] + 24*G[0, 0, SubPlus[s4/s12], 1]*
           Log[s4] - 12*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] - 
          12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] + 
          12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
          3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*((-I)*Pi - 
            4*G[SubPlus[-(s12/s23)], 1] + Log[s12/s4] + Log[-(s23/s4)] + 
            2*Log[s4]) + G[0, s4/s23, 1]*(-5*Pi^2 + 
            24*G[0, SubPlus[-(s12/s23)], 1] - 6*G[s4/s23, (-s12 + s4)/s23, 
              1] + 24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
            6*Log[-(s23/s4)]^2 + 24*G[SubPlus[-(s12/s23)], 1]*
             (I*Pi + Log[-(s23/s4)]) - (12*I)*Pi*Log[s4] - 12*Log[-(s23/s4)]*
             Log[s4]) + G[SubPlus[s4/s12], 1]*((-I)*Pi^3 + 
            12*G[0, 0, SubPlus[s4/s12], 1] + 7*Pi^2*Log[s12/s4] - 
            6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
            24*G[0, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
            24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
            (6*I)*Pi*Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] + 
            6*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 - 
            4*G[SubPlus[-(s12/s23)], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
              6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])) + 2*Pi^2*Log[s4] + 
            (12*I)*Pi*Log[s12/s4]*Log[s4] + 12*Log[s12/s4]*Log[-(s23/s4)]*
             Log[s4] + 6*G[0, SubPlus[s4/s12], 1]*((-I)*Pi + Log[-(s23/s4)] + 
              2*Log[s4]) + 12*Zeta[3]))))/(12*(s12 + s23)))/(s12 - s4)}, 
  {0, 0, (-3*(G[0, s4/s23, 1] - G[s4/s23, 1]*(I*Pi + Log[-(s23/s4)])))/
    (2*s12*s23), -1/4*(-3*G[s4/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 
      2*((6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 6*G[0, 0, s4/s23, 1] - 
        3*G[0, s4/s23, s4/s23, 1] - 6*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
        6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
        6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
        G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
          6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*
           Log[-(s23/s4)]) + (6*I)*G[0, s4/s23, 1]*(Pi + I*Log[s4])) + 
      G[s4/s23, 1]*(7*Pi^2 + 6*G[0, s4/s23, 1] - (12*I)*G[(-s12 + s4)/s23, 1]*
         (Pi - I*Log[-(s23/s4)]) + 6*Log[-(s23/s4)]^2 + (12*I)*Pi*Log[s4] + 
        12*Log[-(s23/s4)]*Log[s4]))/(s12*s23), 
   (G[s4/s23, 1]^3*((-2*I)*Pi - 2*Log[-(s23/s4)]) + 
     G[s4/s23, 1]^2*(-5*Pi^2 + 6*G[0, s4/s23, 1] - 6*Log[-(s23/s4)]^2 + 
       12*G[(-s12 + s4)/s23, 1]*(I*Pi + Log[-(s23/s4)]) - (12*I)*Pi*Log[s4] - 
       12*Log[-(s23/s4)]*Log[s4]) + 2*(6*G[0, s4/s23, 1]^2 - 
       3*G[(-s12 + s4)/s23, 1]^2*(Pi^2 + 6*G[0, s4/s23, 1] + 
         6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
         6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*
          Log[-(s23/s4)]) + 6*((6*I)*Pi*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
         (2*I)*Pi*G[0, (-s12 + s4)/s23, s4/s23, 1] + 
         (2*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
         (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
         4*G[0, 0, 0, s4/s23, 1] + 2*G[0, 0, s4/s23, s4/s23, 1] - 
         4*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] + 
         2*G[0, s4/s23, 0, (-s12 + s4)/s23, 1] + G[0, s4/s23, s4/s23, s4/s23, 
          1] - 2*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
         6*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
         (4*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
         4*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
         6*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
         2*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + 
         2*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
         2*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[-(s23/s4)] + 
         2*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
         6*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
         G[0, s4/s23, s4/s23, 1]*((-2*I)*Pi - 4*Log[-(s23/s4)] - 2*Log[s4]) + 
         (4*I)*G[0, 0, s4/s23, 1]*(Pi + I*Log[s4]) - 
         4*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
         4*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4]) + 
       2*G[s4/s23, (-s12 + s4)/s23, 1]*(6*Log[-(s23/s4)]^2 + 
         Pi*(7*Pi + (12*I)*Log[s4]) + 12*Log[-(s23/s4)]*Log[s4]) + 
       3*G[0, s4/s23, 1]*(5*Pi^2 - 4*G[0, (-s12 + s4)/s23, 1] + 
         4*G[s4/s23, (-s12 + s4)/s23, 1] + (8*I)*Pi*Log[s4] - 4*Log[s4]^2) + 
       2*G[(-s12 + s4)/s23, 1]*((-18*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 
         24*G[0, 0, s4/s23, 1] + 12*G[0, 0, SubPlus[s4/s12], 1] + 
         6*G[0, s4/s23, s4/s23, 1] + 18*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
         12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 9*Pi^2*Log[s12/s4] + 
         6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 18*G[0, (-s12 + s4)/s23, 1]*
          Log[s12/s4] + (6*I)*Pi*Log[s12/s4]^2 + 2*Pi^2*Log[-(s23/s4)] + 
         12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
         18*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
         6*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
         2*Pi^2*Log[s4] + 12*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
         (12*I)*Pi*Log[s12/s4]*Log[s4] + 12*Log[s12/s4]*Log[-(s23/s4)]*
          Log[s4] + 6*G[0, s4/s23, 1]*((-3*I)*Pi - Log[-(s23/s4)] + 
           2*Log[s4]) - 2*G[SubPlus[s4/s12], 1]*
          (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 3*Log[s12/s4]^2 + 
           6*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4])))) + 
     2*G[s4/s23, 1]*((-7*I)*Pi^3 - 12*G[0, 0, s4/s23, 1] - 
       6*G[0, s4/s23, s4/s23, 1] - (12*I)*G[s4/s23, (-s12 + s4)/s23, 1]*
        (Pi - I*Log[-(s23/s4)]) - Pi^2*Log[-(s23/s4)] + 
       12*G[0, s4/s23, 1]*Log[-(s23/s4)] + 4*Log[-(s23/s4)]^3 + 
       18*G[(-s12 + s4)/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 14*Pi^2*Log[s4] + 
       12*G[0, s4/s23, 1]*Log[s4] + 12*Log[-(s23/s4)]^2*Log[s4] + 
       (12*I)*Pi*Log[s4]^2 + 12*Log[-(s23/s4)]*Log[s4]^2 - 
       2*G[(-s12 + s4)/s23, 1]*(7*Pi^2 + 6*G[0, s4/s23, 1] + 
         6*Log[-(s23/s4)]^2 + (12*I)*Pi*Log[s4] + 12*Log[-(s23/s4)]*
          Log[s4]) - 6*Zeta[3]))/(8*s12*s23)}, 
  {1/(4*s12^2), (I*Pi - Log[s12/s4] - Log[s4])/(2*s12^2), 
   ((5*Pi^2)/24 + ((-I)*Pi + Log[s12/s4] + Log[s4])^2/2)/s12^2, 
   (-5*Pi^2*((-I)*Pi + Log[s12/s4] + Log[s4]) - 
     4*((-I)*Pi + Log[s12/s4] + Log[s4])^3 + 58*Zeta[3])/(12*s12^2), 
   (9*Pi^4 + 40*Pi^2*((-I)*Pi + Log[s12/s4] + Log[s4])^2 + 
     16*((-I)*Pi + Log[s12/s4] + Log[s4])^4 - 
     928*((-I)*Pi + Log[s12/s4] + Log[s4])*Zeta[3])/(96*s12^2)}, 
  {0, (s23*(I*Pi + Log[-(s23/s4)]))/(2*(-(s12*s23) + s12*s4)), 
   (5*Pi^2*s23 - Pi^2*s4 - 6*(s23 + s4)*G[0, s4/s23, 1] + 
     6*s23*Log[-(s23/s4)]^2 + 6*(s23 + s4)*G[s4/s23, 1]*
      (I*Pi + Log[-(s23/s4)]) + (12*I)*Pi*s23*Log[s4] + 
     12*s23*Log[-(s23/s4)]*Log[s4])/(12*s12*(s23 - s4)), 
   (3*G[s4/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) - 
     (2*s23*(-3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 
        6*(I*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 2*G[0, 0, s4/s23, 1] + 
          2*G[0, 0, SubPlus[s4/s12], 1] - G[0, s4/s23, (-s12 + s4)/s23, 1] - 
          G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
          G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + G[0, s4/s23, 1]*
           ((-I)*Pi - Log[-(s23/s4)]) + G[0, SubPlus[s4/s12], 1]*
           ((-I)*Pi - Log[s12/s4] - Log[-(s23/s4)]) + 
          G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]) + 
        G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
          6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
           ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
        G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
          6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))))/(s12 + s23) + 
     G[s4/s23, 1]*(-6*G[0, s4/s23, 1] + 6*Log[-(s23/s4)]^2 + 
       Pi*(5*Pi + (12*I)*Log[s4]) + 12*Log[-(s23/s4)]*Log[s4]) + 
     (s23*(I*Pi^3 - 2*Pi^2*G[SubPlus[s4/s12], 1] + 
        (12*I)*Pi*G[0, s4/s23, 1] + (12*I)*Pi*G[0, SubPlus[s4/s12], 1] - 
        12*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1] - 
        (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 48*G[0, 0, s4/s23, 1] + 
        24*G[0, 0, SubPlus[s4/s12], 1] - 24*G[0, s4/s23, s4/s23, 1] + 
        12*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
        12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 2*Pi^2*Log[s12/s4] - 
        (12*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 12*G[0, (-s12 + s4)/s23, 1]*
         Log[s12/s4] - 12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
        (6*I)*Pi*Log[s12/s4]^2 - (12*I)*G[s4/s23, 1]^2*
         (Pi - I*Log[-(s23/s4)]) + Pi^2*Log[-(s23/s4)] + 
        36*G[0, s4/s23, 1]*Log[-(s23/s4)] + 12*G[0, SubPlus[s4/s12], 1]*
         Log[-(s23/s4)] - 12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
        12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
        6*Log[s12/s4]^2*Log[-(s23/s4)] - 4*Log[-(s23/s4)]^3 - 
        2*G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
          6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*
           Log[-(s23/s4)]) - 8*Pi^2*Log[s4] + 24*G[0, s4/s23, 1]*Log[s4] - 
        12*Log[-(s23/s4)]^2*Log[s4] - (12*I)*Pi*Log[s4]^2 - 
        12*Log[-(s23/s4)]*Log[s4]^2 - 4*G[s4/s23, 1]*
         (2*Pi^2 - 6*G[0, s4/s23, 1] + G[(-s12 + s4)/s23, 1]*
           ((-3*I)*Pi - 3*Log[-(s23/s4)]) + 3*Log[-(s23/s4)]^2 + 
          (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) - 36*Zeta[3]))/
      (s23 - s4) + 2*(I*Pi^3 + 6*G[0, 0, s4/s23, 1] + 
       3*G[0, s4/s23, s4/s23, 1] - 6*G[0, s4/s23, 1]*Log[-(s23/s4)] - 
       Pi^2*Log[s4] - 6*G[0, s4/s23, 1]*Log[s4] + 3*Zeta[3]))/(12*s12), 
   (-30*G[s4/s23, 1]^3*(I*Pi + Log[-(s23/s4)]) - 3*G[s4/s23, 1]^2*
      (-30*G[0, s4/s23, 1] + 6*Log[-(s23/s4)]^2 + Pi*(Pi + (12*I)*Log[s4]) + 
       12*Log[-(s23/s4)]*Log[s4]) + 6*G[s4/s23, 1]*
      ((3*I)*Pi^3 + (24*I)*Pi*G[0, s4/s23, 1] + 
       (24*I)*Pi*G[0, SubPlus[s4/s12], 1] + (24*I)*Pi*
        G[s4/s23, (-s12 + s4)/s23, 1] - (24*I)*Pi*G[(-s12 + s4)/s23, 
         SubPlus[-(s12/s23)], 1] - 60*G[0, 0, s4/s23, 1] + 
       48*G[0, 0, SubPlus[s4/s12], 1] - 30*G[0, s4/s23, s4/s23, 1] + 
       24*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
       24*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 4*Pi^2*Log[s12/s4] + 
       12*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 24*G[0, (-s12 + s4)/s23, 1]*
        Log[s12/s4] - 24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
       (12*I)*Pi*Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] + 
       36*G[0, s4/s23, 1]*Log[-(s23/s4)] + 24*G[0, SubPlus[s4/s12], 1]*
        Log[-(s23/s4)] + 24*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] + 
       12*Log[s12/s4]^2*Log[-(s23/s4)] - 4*Log[-(s23/s4)]^3 + 
       24*G[(-s12 + s4)/s23, 1]*G[SubPlus[-(s12/s23)], 1]*
        (I*Pi + Log[-(s23/s4)]) - 4*G[SubPlus[s4/s12], 1]*
        (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 6*Log[s12/s4]*
          (I*Pi + Log[-(s23/s4)])) - 10*Pi^2*Log[s4] + 
       12*G[0, s4/s23, 1]*Log[s4] - 12*Log[-(s23/s4)]^2*Log[s4] - 
       (12*I)*Pi*Log[s4]^2 - 12*Log[-(s23/s4)]*Log[s4]^2 - 54*Zeta[3]) - 
     2*(8*Pi^4 + 3*Pi^2*G[0, s4/s23, 1] - 36*G[0, s4/s23, 1]^2 - 
       72*G[0, s4/s23, 1]*G[0, SubPlus[-(s12/s23)], 1] + 
       72*G[0, s4/s23, 1]*G[0, SubPlus[s4/s12], 1] + 
       72*G[0, SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1] + 
       12*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] + 72*G[0, s4/s23, 1]*
        G[s4/s23, (-s12 + s4)/s23, 1] + 72*G[0, SubPlus[s4/s12], 1]*
        G[s4/s23, (-s12 + s4)/s23, 1] - 12*Pi^2*G[(-s12 + s4)/s23, 
         SubPlus[-(s12/s23)], 1] - 72*G[0, s4/s23, 1]*G[(-s12 + s4)/s23, 
         SubPlus[-(s12/s23)], 1] - 72*G[0, SubPlus[s4/s12], 1]*
        G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
       (144*I)*Pi*G[0, s4/s23, s4/s23, 1] + (72*I)*Pi*
        G[0, s4/s23, SubPlus[-(s12/s23)], 1] + 
       (72*I)*Pi*G[0, SubPlus[-(s12/s23)], s4/s23, 1] + 
       (144*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
       (72*I)*Pi*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
       72*G[0, 0, 0, s4/s23, 1] + 216*G[0, 0, 0, SubPlus[s4/s12], 1] - 
       180*G[0, 0, s4/s23, s4/s23, 1] + 
       72*G[0, s4/s23, 0, SubPlus[-(s12/s23)], 1] - 
       90*G[0, s4/s23, s4/s23, s4/s23, 1] + 72*G[0, s4/s23, (-s12 + s4)/s23, 
         s4/s23, 1] + 72*G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 
         1] - 72*G[SubPlus[s4/s12], 1]*G[0, s4/s23, 1]*Log[s12/s4] - 
       72*G[SubPlus[s4/s12], 1]*G[0, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
       (72*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
       72*G[SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
       (72*I)*Pi*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
       72*G[SubPlus[s4/s12], 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
        Log[s12/s4] - 108*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
       72*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[s12/s4] + 
       72*G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
       3*Pi^2*Log[s12/s4]^2 + 36*G[0, s4/s23, 1]*Log[s12/s4]^2 + 
       18*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 + 72*G[0, 0, s4/s23, 1]*
        Log[-(s23/s4)] + 180*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] + 
       72*G[0, s4/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] + 
       72*G[0, SubPlus[-(s12/s23)], s4/s23, 1]*Log[-(s23/s4)] + 
       144*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       72*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] + 
       72*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[-(s23/s4)] - 
       72*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4]*
        Log[-(s23/s4)] - 36*G[0, s4/s23, 1]*Log[-(s23/s4)]^2 + 
       12*G[(-s12 + s4)/s23, 1]*G[SubPlus[-(s12/s23)], 1]*
        (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
         (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
         6*Log[s12/s4]*Log[-(s23/s4)]) + 12*G[SubPlus[-(s12/s23)], 1]*
        (-3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 
         6*(I*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 2*G[0, 0, s4/s23, 1] + 
           2*G[0, 0, SubPlus[s4/s12], 1] - G[0, s4/s23, (-s12 + s4)/s23, 1] - 
           G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
           G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + G[0, s4/s23, 1]*
            ((-I)*Pi - Log[-(s23/s4)]) + G[0, SubPlus[s4/s12], 1]*
            ((-I)*Pi - Log[s12/s4] - Log[-(s23/s4)]) + 
           G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]) + 
         G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
           6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))) + (12*I)*Pi^3*Log[s4] + 
       72*G[0, 0, s4/s23, 1]*Log[s4] + 36*G[0, s4/s23, s4/s23, 1]*Log[s4] - 
       72*G[0, s4/s23, 1]*Log[-(s23/s4)]*Log[s4] - 6*Pi^2*Log[s4]^2 - 
       36*G[0, s4/s23, 1]*Log[s4]^2 - (36*I)*Pi*Zeta[3] - 
       108*Log[s12/s4]*Zeta[3] + 36*Log[s4]*Zeta[3]) + 
     (3*s23*(3*Pi^4 - (4*I)*Pi^3*G[SubPlus[s4/s12], 1] - 
        24*Pi^2*G[0, s4/s23, 1] - 36*G[0, s4/s23, 1]^2 + 
        4*Pi^2*G[0, (-s12 + s4)/s23, 1] - 28*Pi^2*G[0, SubPlus[s4/s12], 1] - 
        (24*I)*Pi*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1] + 
        48*G[0, s4/s23, 1]*G[0, SubPlus[s4/s12], 1] + 
        24*G[0, (-s12 + s4)/s23, 1]*G[0, SubPlus[s4/s12], 1] - 
        36*G[0, SubPlus[s4/s12], 1]^2 + 24*Pi^2*G[s4/s23, (-s12 + s4)/s23, 
          1] + 48*G[0, SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1] - 
        (48*I)*Pi*G[0, 0, s4/s23, 1] + (120*I)*Pi*G[0, 0, SubPlus[s4/s12], 
          1] + 72*G[SubPlus[s4/s12], 1]*G[0, 0, SubPlus[s4/s12], 1] + 
        (120*I)*Pi*G[0, s4/s23, s4/s23, 1] + (48*I)*Pi*
         G[0, s4/s23, (-s12 + s4)/s23, 1] + (24*I)*Pi*G[0, SubPlus[s4/s12], 
          SubPlus[s4/s12], 1] + (48*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 
          1] + (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
        96*G[0, 0, 0, s4/s23, 1] - 192*G[0, 0, s4/s23, s4/s23, 1] - 
        48*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] - 
        72*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
        96*G[0, s4/s23, s4/s23, s4/s23, 1] + 48*G[0, s4/s23, s4/s23, 
          (-s12 + s4)/s23, 1] + 48*G[0, s4/s23, (-s12 + s4)/s23, s4/s23, 1] - 
        24*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
        (4*I)*Pi^3*Log[s12/s4] + 28*Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        (12*I)*Pi*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 
        48*G[SubPlus[s4/s12], 1]*G[0, s4/s23, 1]*Log[s12/s4] + 
        (48*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
        24*G[SubPlus[s4/s12], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
        (96*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
        (48*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
        48*G[SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
        48*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
        72*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
        48*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[s12/s4] - 
        24*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
        72*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] - 
        14*Pi^2*Log[s12/s4]^2 + (36*I)*Pi*G[SubPlus[s4/s12], 1]*
         Log[s12/s4]^2 - 18*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2 + 
        24*G[0, s4/s23, 1]*Log[s12/s4]^2 - 36*G[0, (-s12 + s4)/s23, 1]*
         Log[s12/s4]^2 + 36*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 
        (12*I)*Pi*Log[s12/s4]^3 + 4*Pi^2*G[SubPlus[s4/s12], 1]*
         Log[-(s23/s4)] + 24*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*
         Log[-(s23/s4)] + 144*G[0, 0, s4/s23, 1]*Log[-(s23/s4)] + 
        24*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
        216*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] - 
        24*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
        48*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
        24*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
        4*Pi^2*Log[s12/s4]*Log[-(s23/s4)] - 12*G[SubPlus[s4/s12], 1]^2*
         Log[s12/s4]*Log[-(s23/s4)] - 48*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*
         Log[-(s23/s4)] + 48*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4]*
         Log[-(s23/s4)] + 36*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
         Log[-(s23/s4)] - 12*Log[s12/s4]^3*Log[-(s23/s4)] - 
        2*Pi^2*Log[-(s23/s4)]^2 - 72*G[0, s4/s23, 1]*Log[-(s23/s4)]^2 - 
        24*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]^2 + 
        24*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]^2 + 
        24*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]^2 - 
        12*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 4*Log[-(s23/s4)]^4 + 
        16*G[s4/s23, 1]^3*(I*Pi + Log[-(s23/s4)]) - 2*G[(-s12 + s4)/s23, 1]^2*
         (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
          (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
          6*Log[s12/s4]*Log[-(s23/s4)]) - (4*I)*Pi^3*Log[s4] + 
        8*Pi^2*G[SubPlus[s4/s12], 1]*Log[s4] - (48*I)*Pi*G[0, s4/s23, 1]*
         Log[s4] - (48*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
        48*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
        (48*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
        192*G[0, 0, s4/s23, 1]*Log[s4] - 96*G[0, 0, SubPlus[s4/s12], 1]*
         Log[s4] + 96*G[0, s4/s23, s4/s23, 1]*Log[s4] - 
        48*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
        48*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] - 
        8*Pi^2*Log[s12/s4]*Log[s4] + (48*I)*Pi*G[SubPlus[s4/s12], 1]*
         Log[s12/s4]*Log[s4] - 24*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*
         Log[s4] - 48*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
        48*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
        (24*I)*Pi*Log[s12/s4]^2*Log[s4] - 4*Pi^2*Log[-(s23/s4)]*Log[s4] - 
        144*G[0, s4/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
        48*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] + 
        48*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] + 
        48*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] - 
        24*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 16*Log[-(s23/s4)]^3*
         Log[s4] + 16*Pi^2*Log[s4]^2 - 48*G[0, s4/s23, 1]*Log[s4]^2 + 
        24*Log[-(s23/s4)]^2*Log[s4]^2 + (16*I)*Pi*Log[s4]^3 + 
        16*Log[-(s23/s4)]*Log[s4]^3 + 8*G[s4/s23, 1]^2*
         (2*Pi^2 - 6*G[0, s4/s23, 1] + G[(-s12 + s4)/s23, 1]*
           ((-3*I)*Pi - 3*Log[-(s23/s4)]) + 3*Log[-(s23/s4)]^2 + 
          (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) - (136*I)*Pi*Zeta[3] + 
        72*G[SubPlus[s4/s12], 1]*Zeta[3] - 72*Log[s12/s4]*Zeta[3] + 
        8*Log[-(s23/s4)]*Zeta[3] + 144*Log[s4]*Zeta[3] + 
        4*G[(-s12 + s4)/s23, 1]*((-I)*Pi^3 - (6*I)*Pi*
           G[s4/s23, (-s12 + s4)/s23, 1] + 12*G[0, 0, s4/s23, 1] + 
          18*G[0, 0, SubPlus[s4/s12], 1] - 12*G[0, s4/s23, s4/s23, 1] + 
          6*G[0, s4/s23, (-s12 + s4)/s23, 1] + 7*Pi^2*Log[s12/s4] + 
          (6*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
          6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + (9*I)*Pi*Log[s12/s4]^2 - 
          9*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] - 
          6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
          9*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
          2*Pi^2*Log[s4] + (12*I)*Pi*Log[s12/s4]*Log[s4] - 
          12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
          12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 12*G[0, s4/s23, 1]*
           ((-I)*Pi + Log[s4]) + 6*G[0, SubPlus[s4/s12], 1]*
           ((-I)*Pi + Log[-(s23/s4)] + 2*Log[s4]) + 18*Zeta[3]) + 
        4*G[s4/s23, 1]*((-I)*Pi^3 - (12*I)*Pi*G[0, s4/s23, 1] - 
          (12*I)*Pi*G[0, SubPlus[s4/s12], 1] + 48*G[0, 0, s4/s23, 1] - 
          24*G[0, 0, SubPlus[s4/s12], 1] + 24*G[0, s4/s23, s4/s23, 1] - 
          12*G[0, s4/s23, (-s12 + s4)/s23, 1] - 12*G[0, SubPlus[s4/s12], 
            SubPlus[s4/s12], 1] - 2*Pi^2*Log[s12/s4] - 
          6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 12*G[0, (-s12 + s4)/s23, 1]*
           Log[s12/s4] + 12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
          (6*I)*Pi*Log[s12/s4]^2 - Pi^2*Log[-(s23/s4)] - 36*G[0, s4/s23, 1]*
           Log[-(s23/s4)] - 12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*Log[s12/s4]^2*Log[-(s23/s4)] + 4*Log[-(s23/s4)]^3 + 
          3*G[(-s12 + s4)/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 
          2*G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
            6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])) + 8*Pi^2*Log[s4] - 
          24*G[0, s4/s23, 1]*Log[s4] + 12*Log[-(s23/s4)]^2*Log[s4] + 
          (12*I)*Pi*Log[s4]^2 + 12*Log[-(s23/s4)]*Log[s4]^2 - 
          2*G[(-s12 + s4)/s23, 1]*(-6*G[0, s4/s23, 1] + 3*Log[-(s23/s4)]^2 + 
            2*Pi*(Pi + (3*I)*Log[s4]) + 6*Log[-(s23/s4)]*Log[s4]) + 
          36*Zeta[3])))/(s23 - s4) + 
     (6*s23*(-(G[(-s12 + s4)/s23, 1]^2*(Pi^2 + 6*G[0, s4/s23, 1] + 
           6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
           6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
            ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)])) + 
        2*G[(-s12 + s4)/s23, 1]*((-I)*Pi^3 - (12*I)*Pi*G[0, s4/s23, 1] - 
          (6*I)*Pi*G[0, SubPlus[s4/s12], 1] - (6*I)*Pi*
           G[s4/s23, (-s12 + s4)/s23, 1] + 12*G[0, 0, s4/s23, 1] + 
          12*G[0, 0, SubPlus[s4/s12], 1] - 6*G[0, s4/s23, s4/s23, 1] + 
          6*G[0, s4/s23, (-s12 + s4)/s23, 1] + 7*Pi^2*Log[s12/s4] + 
          (6*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
          6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + (6*I)*Pi*Log[s12/s4]^2 - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + G[s4/s23, 1]^2*
           ((-3*I)*Pi - 3*Log[-(s23/s4)]) + Pi^2*Log[-(s23/s4)] + 
          6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
          6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
          6*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 - 
          4*G[SubPlus[-(s12/s23)], 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
            6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
            6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*
             Log[-(s23/s4)]) + 2*Pi^2*Log[s4] + 12*G[0, s4/s23, 1]*Log[s4] + 
          12*G[0, SubPlus[s4/s12], 1]*Log[s4] + (12*I)*Pi*Log[s12/s4]*
           Log[s4] - 12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
          12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + G[s4/s23, 1]*
           (-5*Pi^2 + 6*G[0, s4/s23, 1] - 6*Log[-(s23/s4)]^2 + 
            24*G[SubPlus[-(s12/s23)], 1]*(I*Pi + Log[-(s23/s4)]) - 
            (12*I)*Pi*Log[s4] - 12*Log[-(s23/s4)]*Log[s4]) + 12*Zeta[3]) + 
        2*(3*G[0, s4/s23, 1]^2 + Pi^2*G[0, (-s12 + s4)/s23, 1] - 
          8*Pi^2*G[0, SubPlus[s4/s12], 1] + (24*I)*Pi*G[SubPlus[-(s12/s23)], 
            1]*G[0, SubPlus[s4/s12], 1] + 6*G[0, (-s12 + s4)/s23, 1]*
           G[0, SubPlus[s4/s12], 1] - 24*G[0, SubPlus[-(s12/s23)], 1]*
           G[0, SubPlus[s4/s12], 1] - 9*G[0, SubPlus[s4/s12], 1]^2 + 
          5*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] + (6*I)*Pi*G[s4/s23, 1]*
           G[s4/s23, (-s12 + s4)/s23, 1] - (24*I)*Pi*G[SubPlus[-(s12/s23)], 
            1]*G[s4/s23, (-s12 + s4)/s23, 1] + 4*Pi^2*G[(-s12 + s4)/s23, 
            SubPlus[-(s12/s23)], 1] - (24*I)*Pi*G[s4/s23, 1]*
           G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
          24*G[0, SubPlus[s4/s12], 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 
            1] - (12*I)*Pi*G[0, 0, s4/s23, 1] - 48*G[SubPlus[-(s12/s23)], 1]*
           G[0, 0, s4/s23, 1] + (12*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] - 
          48*G[SubPlus[-(s12/s23)], 1]*G[0, 0, SubPlus[s4/s12], 1] - 
          (6*I)*Pi*G[0, s4/s23, s4/s23, 1] + (12*I)*Pi*G[0, s4/s23, 
            (-s12 + s4)/s23, 1] + 24*G[SubPlus[-(s12/s23)], 1]*
           G[0, s4/s23, (-s12 + s4)/s23, 1] - (24*I)*Pi*G[0, s4/s23, 
            SubPlus[-(s12/s23)], 1] - (24*I)*Pi*G[0, SubPlus[-(s12/s23)], 
            s4/s23, 1] + (6*I)*Pi*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 
            1] - (6*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
          (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
          (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
          72*G[0, 0, 0, SubPlus[s4/s12], 1] - 12*G[0, 0, s4/s23, 
            (-s12 + s4)/s23, 1] - 12*G[0, 0, SubPlus[s4/s12], 
            SubPlus[s4/s12], 1] - 24*G[0, s4/s23, 0, SubPlus[-(s12/s23)], 
            1] + 6*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
          6*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
          24*G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
          (12*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
          (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
          (24*I)*Pi*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
          12*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
          36*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
          6*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
          24*G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
          18*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] - 
          6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 - 
          6*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 + 
          24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*
           Log[-(s23/s4)] + 6*G[s4/s23, 1]*G[s4/s23, (-s12 + s4)/s23, 1]*
           Log[-(s23/s4)] - 24*G[SubPlus[-(s12/s23)], 1]*
           G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
          24*G[s4/s23, 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[-(s23/s4)] + 12*G[0, 0, s4/s23, 1]*Log[-(s23/s4)] + 
          36*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] - 
          24*G[0, s4/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] - 
          24*G[0, SubPlus[-(s12/s23)], s4/s23, 1]*Log[-(s23/s4)] - 
          6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
          6*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
          6*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
          24*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[-(s23/s4)] - 24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*
           Log[-(s23/s4)] + 24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
           Log[s12/s4]*Log[-(s23/s4)] - 6*G[0, SubPlus[s4/s12], 1]*
           Log[-(s23/s4)]^2 + 6*G[s4/s23, (-s12 + s4)/s23, 1]*
           Log[-(s23/s4)]^2 - (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
          (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
          24*G[0, 0, s4/s23, 1]*Log[s4] + 24*G[0, 0, SubPlus[s4/s12], 1]*
           Log[s4] - 12*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] - 
          12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
          12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] + 
          12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
          3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*((-I)*Pi - 
            4*G[SubPlus[-(s12/s23)], 1] + Log[s12/s4] + Log[-(s23/s4)] + 
            2*Log[s4]) + G[0, s4/s23, 1]*(-5*Pi^2 + 
            24*G[0, SubPlus[-(s12/s23)], 1] - 6*G[s4/s23, (-s12 + s4)/s23, 
              1] + 24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
            6*Log[-(s23/s4)]^2 + 24*G[SubPlus[-(s12/s23)], 1]*
             (I*Pi + Log[-(s23/s4)]) - (12*I)*Pi*Log[s4] - 12*Log[-(s23/s4)]*
             Log[s4]) + G[SubPlus[s4/s12], 1]*((-I)*Pi^3 + 
            12*G[0, 0, SubPlus[s4/s12], 1] + 7*Pi^2*Log[s12/s4] - 
            6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
            24*G[0, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
            24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
            (6*I)*Pi*Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] + 
            6*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 - 
            4*G[SubPlus[-(s12/s23)], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
              6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])) + 2*Pi^2*Log[s4] + 
            (12*I)*Pi*Log[s12/s4]*Log[s4] + 12*Log[s12/s4]*Log[-(s23/s4)]*
             Log[s4] + 6*G[0, SubPlus[s4/s12], 1]*((-I)*Pi + Log[-(s23/s4)] + 
              2*Log[s4]) + 12*Zeta[3]))))/(s12 + s23))/(72*s12)}, 
  {0, -((I*Pi + Log[-(s23/s4)])/(2*s12*s23 - 2*s12*s4)), 
   (2*Pi^2 - 6*G[0, s4/s23, 1] + 3*Log[-(s23/s4)]^2 + 
     6*G[s4/s23, 1]*(I*Pi + Log[-(s23/s4)]) + (6*I)*Pi*Log[s4] + 
     6*Log[-(s23/s4)]*Log[s4])/(6*s12*(s23 - s4)), 
   -1/12*((-I)*Pi^3 + 2*Pi^2*G[SubPlus[s4/s12], 1] - 
      (12*I)*Pi*G[0, s4/s23, 1] - (12*I)*Pi*G[0, SubPlus[s4/s12], 1] + 
      12*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1] + 
      (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 48*G[0, 0, s4/s23, 1] - 
      24*G[0, 0, SubPlus[s4/s12], 1] + 24*G[0, s4/s23, s4/s23, 1] - 
      12*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
      12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 2*Pi^2*Log[s12/s4] + 
      (12*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]^2*
       Log[s12/s4] - 12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - (6*I)*Pi*Log[s12/s4]^2 - 
      Pi^2*Log[-(s23/s4)] - 36*G[0, s4/s23, 1]*Log[-(s23/s4)] - 
      12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
      12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
      12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] - 
      6*Log[s12/s4]^2*Log[-(s23/s4)] + 4*Log[-(s23/s4)]^3 + 
      12*G[s4/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 2*G[(-s12 + s4)/s23, 1]*
       (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
        (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        6*Log[s12/s4]*Log[-(s23/s4)]) + 8*Pi^2*Log[s4] - 
      24*G[0, s4/s23, 1]*Log[s4] + 12*Log[-(s23/s4)]^2*Log[s4] + 
      (12*I)*Pi*Log[s4]^2 + 12*Log[-(s23/s4)]*Log[s4]^2 + 
      4*G[s4/s23, 1]*(2*Pi^2 - 6*G[0, s4/s23, 1] + G[(-s12 + s4)/s23, 1]*
         ((-3*I)*Pi - 3*Log[-(s23/s4)]) + 3*Log[-(s23/s4)]^2 + 
        (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) + 36*Zeta[3])/
     (s12*(s23 - s4)), (3*Pi^4 - (4*I)*Pi^3*G[SubPlus[s4/s12], 1] - 
     24*Pi^2*G[0, s4/s23, 1] - 36*G[0, s4/s23, 1]^2 + 
     4*Pi^2*G[0, (-s12 + s4)/s23, 1] - 28*Pi^2*G[0, SubPlus[s4/s12], 1] - 
     (24*I)*Pi*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1] + 
     48*G[0, s4/s23, 1]*G[0, SubPlus[s4/s12], 1] + 
     24*G[0, (-s12 + s4)/s23, 1]*G[0, SubPlus[s4/s12], 1] - 
     36*G[0, SubPlus[s4/s12], 1]^2 + 24*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] + 
     48*G[0, SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1] - 
     (48*I)*Pi*G[0, 0, s4/s23, 1] + (120*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] + 
     72*G[SubPlus[s4/s12], 1]*G[0, 0, SubPlus[s4/s12], 1] + 
     (120*I)*Pi*G[0, s4/s23, s4/s23, 1] + 
     (48*I)*Pi*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
     (24*I)*Pi*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
     (48*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
     (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
     96*G[0, 0, 0, s4/s23, 1] - 192*G[0, 0, s4/s23, s4/s23, 1] - 
     48*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] - 
     72*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     96*G[0, s4/s23, s4/s23, s4/s23, 1] + 
     48*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
     48*G[0, s4/s23, (-s12 + s4)/s23, s4/s23, 1] - 
     24*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
     (4*I)*Pi^3*Log[s12/s4] + 28*Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
     (12*I)*Pi*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 
     48*G[SubPlus[s4/s12], 1]*G[0, s4/s23, 1]*Log[s12/s4] + 
     (48*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     24*G[SubPlus[s4/s12], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     (96*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
     (48*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     48*G[SubPlus[s4/s12], 1]*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     48*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
     72*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
     48*G[0, (-s12 + s4)/s23, s4/s23, 1]*Log[s12/s4] - 
     24*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
     72*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] - 
     14*Pi^2*Log[s12/s4]^2 + (36*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 
     18*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2 + 24*G[0, s4/s23, 1]*
      Log[s12/s4]^2 - 36*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + 
     36*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 - (12*I)*Pi*Log[s12/s4]^3 + 
     4*Pi^2*G[SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 24*G[SubPlus[s4/s12], 1]*
      G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 144*G[0, 0, s4/s23, 1]*
      Log[-(s23/s4)] + 24*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
     216*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] - 
     24*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
     48*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
     24*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
     4*Pi^2*Log[s12/s4]*Log[-(s23/s4)] - 12*G[SubPlus[s4/s12], 1]^2*
      Log[s12/s4]*Log[-(s23/s4)] - 48*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*
      Log[-(s23/s4)] + 48*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s12/s4]*
      Log[-(s23/s4)] + 36*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
      Log[-(s23/s4)] - 12*Log[s12/s4]^3*Log[-(s23/s4)] - 
     2*Pi^2*Log[-(s23/s4)]^2 - 72*G[0, s4/s23, 1]*Log[-(s23/s4)]^2 - 
     24*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]^2 + 
     24*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]^2 + 
     24*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]^2 - 
     12*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 4*Log[-(s23/s4)]^4 + 
     16*G[s4/s23, 1]^3*(I*Pi + Log[-(s23/s4)]) - 2*G[(-s12 + s4)/s23, 1]^2*
      (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
       (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
       6*Log[s12/s4]*Log[-(s23/s4)]) - (4*I)*Pi^3*Log[s4] + 
     8*Pi^2*G[SubPlus[s4/s12], 1]*Log[s4] - (48*I)*Pi*G[0, s4/s23, 1]*
      Log[s4] - (48*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
     48*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s4] + 
     (48*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
     192*G[0, 0, s4/s23, 1]*Log[s4] - 96*G[0, 0, SubPlus[s4/s12], 1]*
      Log[s4] + 96*G[0, s4/s23, s4/s23, 1]*Log[s4] - 
     48*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
     48*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] - 
     8*Pi^2*Log[s12/s4]*Log[s4] + (48*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
      Log[s4] - 24*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*Log[s4] - 
     48*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
     48*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] - 
     (24*I)*Pi*Log[s12/s4]^2*Log[s4] - 4*Pi^2*Log[-(s23/s4)]*Log[s4] - 
     144*G[0, s4/s23, 1]*Log[-(s23/s4)]*Log[s4] - 48*G[0, SubPlus[s4/s12], 1]*
      Log[-(s23/s4)]*Log[s4] + 48*G[s4/s23, (-s12 + s4)/s23, 1]*
      Log[-(s23/s4)]*Log[s4] + 48*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
      Log[-(s23/s4)]*Log[s4] - 24*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 
     16*Log[-(s23/s4)]^3*Log[s4] + 16*Pi^2*Log[s4]^2 - 
     48*G[0, s4/s23, 1]*Log[s4]^2 + 24*Log[-(s23/s4)]^2*Log[s4]^2 + 
     (16*I)*Pi*Log[s4]^3 + 16*Log[-(s23/s4)]*Log[s4]^3 + 
     8*G[s4/s23, 1]^2*(2*Pi^2 - 6*G[0, s4/s23, 1] + G[(-s12 + s4)/s23, 1]*
        ((-3*I)*Pi - 3*Log[-(s23/s4)]) + 3*Log[-(s23/s4)]^2 + 
       (6*I)*Pi*Log[s4] + 6*Log[-(s23/s4)]*Log[s4]) - (136*I)*Pi*Zeta[3] + 
     72*G[SubPlus[s4/s12], 1]*Zeta[3] - 72*Log[s12/s4]*Zeta[3] + 
     8*Log[-(s23/s4)]*Zeta[3] + 144*Log[s4]*Zeta[3] + 
     4*G[(-s12 + s4)/s23, 1]*((-I)*Pi^3 - (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 
         1] + 12*G[0, 0, s4/s23, 1] + 18*G[0, 0, SubPlus[s4/s12], 1] - 
       12*G[0, s4/s23, s4/s23, 1] + 6*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
       7*Pi^2*Log[s12/s4] + (6*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
       6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + (9*I)*Pi*Log[s12/s4]^2 - 
       9*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] - 
       6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
       9*Log[s12/s4]^2*Log[-(s23/s4)] + 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
       2*Pi^2*Log[s4] + (12*I)*Pi*Log[s12/s4]*Log[s4] - 
       12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
       12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 12*G[0, s4/s23, 1]*
        ((-I)*Pi + Log[s4]) + 6*G[0, SubPlus[s4/s12], 1]*
        ((-I)*Pi + Log[-(s23/s4)] + 2*Log[s4]) + 18*Zeta[3]) + 
     4*G[s4/s23, 1]*((-I)*Pi^3 - (12*I)*Pi*G[0, s4/s23, 1] - 
       (12*I)*Pi*G[0, SubPlus[s4/s12], 1] + 48*G[0, 0, s4/s23, 1] - 
       24*G[0, 0, SubPlus[s4/s12], 1] + 24*G[0, s4/s23, s4/s23, 1] - 
       12*G[0, s4/s23, (-s12 + s4)/s23, 1] - 
       12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 2*Pi^2*Log[s12/s4] - 
       6*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 12*G[0, (-s12 + s4)/s23, 1]*
        Log[s12/s4] + 12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
       (6*I)*Pi*Log[s12/s4]^2 - Pi^2*Log[-(s23/s4)] - 
       36*G[0, s4/s23, 1]*Log[-(s23/s4)] - 12*G[0, SubPlus[s4/s12], 1]*
        Log[-(s23/s4)] - 6*Log[s12/s4]^2*Log[-(s23/s4)] + 
       4*Log[-(s23/s4)]^3 + 3*G[(-s12 + s4)/s23, 1]^2*
        (I*Pi + Log[-(s23/s4)]) + 2*G[SubPlus[s4/s12], 1]*
        (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 6*Log[s12/s4]*
          (I*Pi + Log[-(s23/s4)])) + 8*Pi^2*Log[s4] - 
       24*G[0, s4/s23, 1]*Log[s4] + 12*Log[-(s23/s4)]^2*Log[s4] + 
       (12*I)*Pi*Log[s4]^2 + 12*Log[-(s23/s4)]*Log[s4]^2 - 
       2*G[(-s12 + s4)/s23, 1]*(-6*G[0, s4/s23, 1] + 3*Log[-(s23/s4)]^2 + 
         2*Pi*(Pi + (3*I)*Log[s4]) + 6*Log[-(s23/s4)]*Log[s4]) + 36*Zeta[3]))/
    (24*s12*(s23 - s4))}, 
  {0, 0, -1/12*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] - 6*G[SubPlus[s4/s12], 1]*
       Log[s12/s4] + 3*Log[s12/s4]^2)/(s12 - s4), 
   ((-2*I)*Pi^3 + 6*G[0, 0, SubPlus[s4/s12], 1] - 
     6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + Pi^2*Log[s12/s4] - 
     3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - (6*I)*Pi*Log[s12/s4]^2 + 
     3*Log[s12/s4]^3 + G[SubPlus[s4/s12], 1]*
      (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] - 3*Log[s12/s4]^2 + 
       (12*I)*Log[s12/s4]*(Pi + I*Log[s4])) + 2*Pi^2*Log[s4] + 
     6*Log[s12/s4]^2*Log[s4] + 12*G[0, SubPlus[s4/s12], 1]*
      ((-I)*Pi + Log[s4]) + 12*Zeta[3])/(12*(s12 - s4)), 
   (121*Pi^4 + (720*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] - 
     (720*I)*Pi*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     360*G[0, 0, 0, SubPlus[s4/s12], 1] + 
     360*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
     360*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
     (120*I)*Pi^3*Log[s12/s4] + 60*G[SubPlus[s4/s12], 1]^3*Log[s12/s4] + 
     360*Pi^2*Log[s12/s4]^2 + (360*I)*Pi*Log[s12/s4]^3 - 105*Log[s12/s4]^4 - 
     30*G[SubPlus[s4/s12], 1]^2*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] - 
       3*Log[s12/s4]^2 + (12*I)*Log[s12/s4]*(Pi + I*Log[s4])) + 
     (240*I)*Pi^3*Log[s4] - 720*G[0, 0, SubPlus[s4/s12], 1]*Log[s4] + 
     720*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] - 
     120*Pi^2*Log[s12/s4]*Log[s4] + (720*I)*Pi*Log[s12/s4]^2*Log[s4] - 
     360*Log[s12/s4]^3*Log[s4] - 120*Pi^2*Log[s4]^2 - 
     360*Log[s12/s4]^2*Log[s4]^2 + 60*G[0, SubPlus[s4/s12], 1]*
      (13*Pi^2 + (24*I)*Pi*Log[s4] - 12*Log[s4]^2) + 
     60*G[SubPlus[s4/s12], 1]*((2*I)*Pi^3 - 6*G[0, 0, SubPlus[s4/s12], 1] + 
       6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 14*Pi^2*Log[s12/s4] - 
       (6*I)*Pi*Log[s12/s4]^2 + Log[s12/s4]^3 + 
       (12*I)*G[0, SubPlus[s4/s12], 1]*(Pi + I*Log[s4]) - 2*Pi^2*Log[s4] - 
       (24*I)*Pi*Log[s12/s4]*Log[s4] + 6*Log[s12/s4]^2*Log[s4] + 
       12*Log[s12/s4]*Log[s4]^2 - 12*Zeta[3]) + (1440*I)*Pi*Zeta[3] - 
     720*Log[s12/s4]*Zeta[3] - 1440*Log[s4]*Zeta[3])/(720*(s12 - s4))}, 
  {1/(4*s23), -1/2*(Log[-(s23/s4)] + Log[s4])/s23, 
   (-Pi^2 + 12*Log[-(s23/s4)]^2 + 24*Log[-(s23/s4)]*Log[s4] + 12*Log[s4]^2)/
    (24*s23), (-4*Log[-(s23/s4)]^3 + Pi^2*Log[s4] - 
     12*Log[-(s23/s4)]^2*Log[s4] - 4*Log[s4]^3 + 
     Log[-(s23/s4)]*(Pi^2 - 12*Log[s4]^2) - 32*Zeta[3])/(12*s23), 
   (-19*Pi^4 + 80*Log[-(s23/s4)]^4 + 320*Log[-(s23/s4)]^3*Log[s4] - 
     40*Pi^2*Log[s4]^2 + 80*Log[s4]^4 - 40*Log[-(s23/s4)]^2*
      (Pi^2 - 12*Log[s4]^2) - 80*Log[-(s23/s4)]*(Pi^2*Log[s4] - 4*Log[s4]^3 - 
       32*Zeta[3]) + 2560*Log[s4]*Zeta[3])/(480*s23)}, 
  {0, 0, -((Pi^2/6 + G[0, s4/s23, 1] + G[0, SubPlus[s4/s12], 1] - 
      G[SubPlus[s4/s12], 1]*Log[s12/s4] - G[s4/s23, 1]*
       (I*Pi + Log[-(s23/s4)]) + Log[s12/s4]*(I*Pi + Log[-(s23/s4)]))/
     (s12 + s23 - s4)), (-(Pi^2*G[SubPlus[s4/s12], 1]) - 
     (6*I)*Pi*G[0, s4/s23, 1] - 6*G[SubPlus[s4/s12], 1]*
      G[0, SubPlus[s4/s12], 1] - (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 
     6*G[0, 0, s4/s23, 1] + 6*G[0, 0, SubPlus[s4/s12], 1] + 
     6*G[0, s4/s23, (-s12 + s4)/s23, 1] + 
     6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 4*Pi^2*Log[s12/s4] + 
     3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 6*G[0, (-s12 + s4)/s23, 1]*
      Log[s12/s4] + (3*I)*Pi*Log[s12/s4]^2 - 3*G[SubPlus[s4/s12], 1]*
      Log[s12/s4]^2 + Pi^2*Log[-(s23/s4)] + 6*G[0, SubPlus[s4/s12], 1]*
      Log[-(s23/s4)] - 6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
     6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] + 
     3*Log[s12/s4]^2*Log[-(s23/s4)] + 3*Log[s12/s4]*Log[-(s23/s4)]^2 - 
     G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
       6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 6*Log[s12/s4]*Log[-(s23/s4)]) - 
     3*G[s4/s23, 1]*(Pi - I*Log[-(s23/s4)])*
      (Pi - (2*I)*G[(-s12 + s4)/s23, 1] + I*Log[-(s23/s4)] + (2*I)*Log[s4]) + 
     Pi^2*Log[s4] + 6*G[0, s4/s23, 1]*Log[s4] + 6*G[0, SubPlus[s4/s12], 1]*
      Log[s4] + (6*I)*Pi*Log[s12/s4]*Log[s4] - 6*G[SubPlus[s4/s12], 1]*
      Log[s12/s4]*Log[s4] + 6*Log[s12/s4]*Log[-(s23/s4)]*Log[s4])/
    (3*(s12 + s23 - s4)), -1/60*(3*Pi^4 + 20*Pi^2*G[SubPlus[s4/s12], 1]^2 - 
      130*Pi^2*G[0, s4/s23, 1] - 10*Pi^2*G[0, SubPlus[s4/s12], 1] + 
      120*G[SubPlus[s4/s12], 1]^2*G[0, SubPlus[s4/s12], 1] - 
      120*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] - 
      (240*I)*Pi*G[0, 0, s4/s23, 1] - 240*G[SubPlus[s4/s12], 1]*
       G[0, 0, SubPlus[s4/s12], 1] - (240*I)*Pi*G[0, s4/s23, (-s12 + s4)/s23, 
        1] - 240*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 
        1] - (240*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
      240*G[0, 0, 0, s4/s23, 1] + 240*G[0, 0, 0, SubPlus[s4/s12], 1] + 
      240*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] + 
      240*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
      240*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
      240*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
      (50*I)*Pi^3*Log[s12/s4] - 30*Pi^2*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 
      40*G[SubPlus[s4/s12], 1]^3*Log[s12/s4] - 
      (240*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      240*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      240*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      80*Pi^2*Log[s12/s4]^2 + 60*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]^2 + 
      120*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + (40*I)*Pi*Log[s12/s4]^3 - 
      40*G[SubPlus[s4/s12], 1]*Log[s12/s4]^3 - 40*Pi^2*G[SubPlus[s4/s12], 1]*
       Log[-(s23/s4)] - 240*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*
       Log[-(s23/s4)] + 240*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
      240*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] - 
      240*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
      30*Pi^2*Log[s12/s4]*Log[-(s23/s4)] + 120*G[SubPlus[s4/s12], 1]^2*
       Log[s12/s4]*Log[-(s23/s4)] - 120*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2*
       Log[-(s23/s4)] + 40*Log[s12/s4]^3*Log[-(s23/s4)] + 
      20*Pi^2*Log[-(s23/s4)]^2 + 120*G[0, SubPlus[s4/s12], 1]*
       Log[-(s23/s4)]^2 - 120*G[s4/s23, (-s12 + s4)/s23, 1]*
       Log[-(s23/s4)]^2 - 120*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
       Log[-(s23/s4)]^2 + 60*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 
      40*Log[s12/s4]*Log[-(s23/s4)]^3 + 20*G[(-s12 + s4)/s23, 1]^2*
       (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
        (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        6*Log[s12/s4]*Log[-(s23/s4)]) - 40*Pi^2*G[SubPlus[s4/s12], 1]*
       Log[s4] - (240*I)*Pi*G[0, s4/s23, 1]*Log[s4] - 
      240*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
      (240*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
      240*G[0, 0, s4/s23, 1]*Log[s4] + 240*G[0, 0, SubPlus[s4/s12], 1]*
       Log[s4] + 240*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
      240*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] + 
      160*Pi^2*Log[s12/s4]*Log[s4] + 120*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*
       Log[s4] + 240*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
      (120*I)*Pi*Log[s12/s4]^2*Log[s4] - 120*G[SubPlus[s4/s12], 1]*
       Log[s12/s4]^2*Log[s4] + 40*Pi^2*Log[-(s23/s4)]*Log[s4] + 
      240*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] - 
      240*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] - 
      240*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 
      120*Log[s12/s4]^2*Log[-(s23/s4)]*Log[s4] + 120*Log[s12/s4]*
       Log[-(s23/s4)]^2*Log[s4] + 20*Pi^2*Log[s4]^2 + 
      120*G[0, s4/s23, 1]*Log[s4]^2 + 120*G[0, SubPlus[s4/s12], 1]*
       Log[s4]^2 + (120*I)*Pi*Log[s12/s4]*Log[s4]^2 - 
      120*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4]^2 + 
      120*Log[s12/s4]*Log[-(s23/s4)]*Log[s4]^2 + (10*I)*G[s4/s23, 1]*
       (Pi - I*Log[-(s23/s4)])*(5*Pi^2 - 12*G[(-s12 + s4)/s23, 1]^2 - 
        4*Log[-(s23/s4)]^2 + (4*I)*Log[-(s23/s4)]*(Pi + (3*I)*Log[s4]) + 
        (12*I)*Pi*Log[s4] - 12*Log[s4]^2 + 12*G[(-s12 + s4)/s23, 1]*
         ((-I)*Pi + Log[-(s23/s4)] + 2*Log[s4])) + 40*G[(-s12 + s4)/s23, 1]*
       ((6*I)*Pi*G[0, s4/s23, 1] + (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 
        6*G[0, 0, s4/s23, 1] - 6*G[0, 0, SubPlus[s4/s12], 1] - 
        6*G[0, s4/s23, (-s12 + s4)/s23, 1] - 6*G[0, SubPlus[s4/s12], 
          SubPlus[s4/s12], 1] - 4*Pi^2*Log[s12/s4] - 
        3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 6*G[0, (-s12 + s4)/s23, 1]*
         Log[s12/s4] - (3*I)*Pi*Log[s12/s4]^2 - Pi^2*Log[-(s23/s4)] - 
        6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
        6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
        3*Log[s12/s4]^2*Log[-(s23/s4)] - 3*Log[s12/s4]*Log[-(s23/s4)]^2 - 
        Pi^2*Log[s4] - 6*G[0, s4/s23, 1]*Log[s4] - 6*G[0, SubPlus[s4/s12], 1]*
         Log[s4] - (6*I)*Pi*Log[s12/s4]*Log[s4] - 6*Log[s12/s4]*
         Log[-(s23/s4)]*Log[s4] + G[SubPlus[s4/s12], 1]*
         (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 3*Log[s12/s4]^2 + 
          6*Log[s12/s4]*(Log[-(s23/s4)] + Log[s4]))))/(s12 + s23 - s4)}, 
  {0, 0, Pi^2/(12*s12), -1/6*((-I)*Pi^3 + Pi^2*Log[s12/s4] + Pi^2*Log[s4] - 
      3*Zeta[3])/s12, (Pi^4/90 + (Pi^2*((-I)*Pi + Log[s12/s4] + Log[s4])^2)/
      6 - ((-I)*Pi + Log[s12/s4] + Log[s4])*Zeta[3])/s12}, 
  {0, 0, 0, 0, -1/60*(4*Pi^4 + 360*G[0, 0, 0, SubPlus[s4/s12], 1] - 
      180*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 5*Pi^2*Log[s12/s4]^2 + 
      30*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 180*Log[s12/s4]*Zeta[3])/
     (s12 - s4)}, {0, 0, 0, (-3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] + 
     6*(I*Pi*G[s4/s23, (-s12 + s4)/s23, 1] + 2*G[0, 0, s4/s23, 1] + 
       2*G[0, 0, SubPlus[s4/s12], 1] - G[0, s4/s23, (-s12 + s4)/s23, 1] - 
       G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - G[0, (-s12 + s4)/s23, 1]*
        Log[s12/s4] + G[0, s4/s23, 1]*((-I)*Pi - Log[-(s23/s4)]) + 
       G[0, SubPlus[s4/s12], 1]*((-I)*Pi - Log[s12/s4] - Log[-(s23/s4)]) + 
       G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]) + 
     G[(-s12 + s4)/s23, 1]*(Pi^2 + 6*G[0, s4/s23, 1] + 
       6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
        ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
     G[SubPlus[s4/s12], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
       6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])))/(6*(s12 + s23)), 
   (G[(-s12 + s4)/s23, 1]^2*(Pi^2 + 6*G[0, s4/s23, 1] + 
       6*G[0, SubPlus[s4/s12], 1] + (6*I)*Pi*Log[s12/s4] - 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + G[s4/s23, 1]*
        ((-6*I)*Pi - 6*Log[-(s23/s4)]) + 6*Log[s12/s4]*Log[-(s23/s4)]) + 
     2*(-3*G[0, s4/s23, 1]^2 - Pi^2*G[0, (-s12 + s4)/s23, 1] + 
       8*Pi^2*G[0, SubPlus[s4/s12], 1] - (24*I)*Pi*G[SubPlus[-(s12/s23)], 1]*
        G[0, SubPlus[s4/s12], 1] - 6*G[0, (-s12 + s4)/s23, 1]*
        G[0, SubPlus[s4/s12], 1] + 24*G[0, SubPlus[-(s12/s23)], 1]*
        G[0, SubPlus[s4/s12], 1] + 9*G[0, SubPlus[s4/s12], 1]^2 - 
       5*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] - (6*I)*Pi*G[s4/s23, 1]*
        G[s4/s23, (-s12 + s4)/s23, 1] + (24*I)*Pi*G[SubPlus[-(s12/s23)], 1]*
        G[s4/s23, (-s12 + s4)/s23, 1] - 4*Pi^2*G[(-s12 + s4)/s23, 
         SubPlus[-(s12/s23)], 1] + (24*I)*Pi*G[s4/s23, 1]*
        G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
       24*G[0, SubPlus[s4/s12], 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 
         1] + (12*I)*Pi*G[0, 0, s4/s23, 1] + 48*G[SubPlus[-(s12/s23)], 1]*
        G[0, 0, s4/s23, 1] - (12*I)*Pi*G[0, 0, SubPlus[s4/s12], 1] + 
       48*G[SubPlus[-(s12/s23)], 1]*G[0, 0, SubPlus[s4/s12], 1] + 
       (6*I)*Pi*G[0, s4/s23, s4/s23, 1] - (12*I)*Pi*
        G[0, s4/s23, (-s12 + s4)/s23, 1] - 24*G[SubPlus[-(s12/s23)], 1]*
        G[0, s4/s23, (-s12 + s4)/s23, 1] + (24*I)*Pi*
        G[0, s4/s23, SubPlus[-(s12/s23)], 1] + 
       (24*I)*Pi*G[0, SubPlus[-(s12/s23)], s4/s23, 1] - 
       (6*I)*Pi*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] - 
       24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 
         1] + (6*I)*Pi*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1] - 
       (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] - 
       (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] + 
       72*G[0, 0, 0, SubPlus[s4/s12], 1] + 
       12*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] + 
       12*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
       24*G[0, s4/s23, 0, SubPlus[-(s12/s23)], 1] - 
       6*G[0, s4/s23, s4/s23, (-s12 + s4)/s23, 1] + 
       6*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
       24*G[0, s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
       (12*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
       24*G[SubPlus[-(s12/s23)], 1]*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
       (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
       24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*Log[s12/s4] - 
       (24*I)*Pi*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
       12*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
       36*G[0, 0, SubPlus[s4/s12], 1]*Log[s12/s4] + 
       6*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
       24*G[0, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
       18*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s12/s4] + 
       6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + 6*G[0, SubPlus[s4/s12], 1]*
        Log[s12/s4]^2 - 24*G[SubPlus[-(s12/s23)], 1]*G[0, SubPlus[s4/s12], 1]*
        Log[-(s23/s4)] - 6*G[s4/s23, 1]*G[s4/s23, (-s12 + s4)/s23, 1]*
        Log[-(s23/s4)] + 24*G[SubPlus[-(s12/s23)], 1]*
        G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
       24*G[s4/s23, 1]*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*
        Log[-(s23/s4)] - 12*G[0, 0, s4/s23, 1]*Log[-(s23/s4)] - 
       36*G[0, 0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
       6*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] + 
       24*G[0, s4/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] + 
       24*G[0, SubPlus[-(s12/s23)], s4/s23, 1]*Log[-(s23/s4)] + 
       6*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
       6*G[s4/s23, s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       6*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] - 
       24*G[s4/s23, (-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[-(s23/s4)] + 
       24*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] - 
       24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4]*
        Log[-(s23/s4)] + 6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]^2 - 
       6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]^2 + 
       (12*I)*Pi*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
       (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1]*Log[s4] - 
       24*G[0, 0, s4/s23, 1]*Log[s4] - 24*G[0, 0, SubPlus[s4/s12], 1]*
        Log[s4] + 12*G[0, s4/s23, (-s12 + s4)/s23, 1]*Log[s4] + 
       12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1]*Log[s4] + 
       12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]*Log[s4] + 
       12*G[0, SubPlus[s4/s12], 1]*Log[s12/s4]*Log[s4] + 
       12*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)]*Log[s4] - 
       12*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)]*Log[s4] + 
       3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4]*((-I)*Pi - 
         4*G[SubPlus[-(s12/s23)], 1] + Log[s12/s4] + Log[-(s23/s4)] + 
         2*Log[s4]) + G[0, s4/s23, 1]*(5*Pi^2 - 24*G[0, SubPlus[-(s12/s23)], 
           1] + 6*G[s4/s23, (-s12 + s4)/s23, 1] - 
         24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1] - 
         (24*I)*G[SubPlus[-(s12/s23)], 1]*(Pi - I*Log[-(s23/s4)]) + 
         6*Log[-(s23/s4)]^2 + (12*I)*Pi*Log[s4] + 12*Log[-(s23/s4)]*
          Log[s4]) + G[SubPlus[s4/s12], 1]*(I*Pi^3 - 
         12*G[0, 0, SubPlus[s4/s12], 1] - 7*Pi^2*Log[s12/s4] + 
         6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - 
         24*G[0, SubPlus[-(s12/s23)], 1]*Log[s12/s4] + 
         24*G[(-s12 + s4)/s23, SubPlus[-(s12/s23)], 1]*Log[s12/s4] - 
         (6*I)*Pi*Log[s12/s4]^2 - Pi^2*Log[-(s23/s4)] - 
         6*Log[s12/s4]^2*Log[-(s23/s4)] - 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
         4*G[SubPlus[-(s12/s23)], 1]*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
           6*Log[s12/s4]*(I*Pi + Log[-(s23/s4)])) + 
         (6*I)*G[0, SubPlus[s4/s12], 1]*(Pi + I*Log[-(s23/s4)] + 
           (2*I)*Log[s4]) - 2*Pi^2*Log[s4] - (12*I)*Pi*Log[s12/s4]*Log[s4] - 
         12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] - 12*Zeta[3])) + 
     2*G[(-s12 + s4)/s23, 1]*(I*Pi^3 + (12*I)*Pi*G[0, s4/s23, 1] + 
       (6*I)*Pi*G[0, SubPlus[s4/s12], 1] + 
       (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 12*G[0, 0, s4/s23, 1] - 
       12*G[0, 0, SubPlus[s4/s12], 1] + 6*G[0, s4/s23, s4/s23, 1] - 
       6*G[0, s4/s23, (-s12 + s4)/s23, 1] - 7*Pi^2*Log[s12/s4] - 
       (6*I)*Pi*G[SubPlus[s4/s12], 1]*Log[s12/s4] - 
       6*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] - (6*I)*Pi*Log[s12/s4]^2 + 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 - Pi^2*Log[-(s23/s4)] - 
       6*G[0, SubPlus[s4/s12], 1]*Log[-(s23/s4)] + 
       6*G[s4/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
       6*G[SubPlus[s4/s12], 1]*Log[s12/s4]*Log[-(s23/s4)] - 
       6*Log[s12/s4]^2*Log[-(s23/s4)] - 6*Log[s12/s4]*Log[-(s23/s4)]^2 + 
       3*G[s4/s23, 1]^2*(I*Pi + Log[-(s23/s4)]) + 4*G[SubPlus[-(s12/s23)], 1]*
        (Pi^2 + 6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
         (6*I)*Pi*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
         6*Log[s12/s4]*Log[-(s23/s4)]) - 2*Pi^2*Log[s4] - 
       12*G[0, s4/s23, 1]*Log[s4] - 12*G[0, SubPlus[s4/s12], 1]*Log[s4] - 
       (12*I)*Pi*Log[s12/s4]*Log[s4] + 12*G[SubPlus[s4/s12], 1]*Log[s12/s4]*
        Log[s4] - 12*Log[s12/s4]*Log[-(s23/s4)]*Log[s4] + 
       G[s4/s23, 1]*(5*Pi^2 - 6*G[0, s4/s23, 1] - 
         (24*I)*G[SubPlus[-(s12/s23)], 1]*(Pi - I*Log[-(s23/s4)]) + 
         6*Log[-(s23/s4)]^2 + (12*I)*Pi*Log[s4] + 12*Log[-(s23/s4)]*
          Log[s4]) - 12*Zeta[3]))/(12*(s12 + s23))}, 
  {0, 0, -1/12*(Pi^2 + 6*G[0, s4/s23, 1] - 6*G[s4/s23, 1]*
       (I*Pi + Log[-(s23/s4)]))/(s23 - s4), 
   (G[s4/s23, 1]^2*((-3*I)*Pi - 3*Log[-(s23/s4)]) - 
     G[s4/s23, 1]*(5*Pi^2 - 6*G[0, s4/s23, 1] + 6*Log[-(s23/s4)]^2 + 
       (12*I)*Pi*Log[s4] + 12*Log[-(s23/s4)]*Log[s4]) + 
     2*((-I)*Pi^3 - 6*G[0, 0, s4/s23, 1] - 3*G[0, s4/s23, s4/s23, 1] + 
       6*G[0, s4/s23, 1]*Log[-(s23/s4)] + Pi^2*Log[s4] + 
       6*G[0, s4/s23, 1]*Log[s4] - 3*Zeta[3]))/(12*(s23 - s4)), 
   (30*G[s4/s23, 1]^3*(I*Pi + Log[-(s23/s4)]) + 15*G[s4/s23, 1]^2*
      (5*Pi^2 - 6*G[0, s4/s23, 1] + 6*Log[-(s23/s4)]^2 + (12*I)*Pi*Log[s4] + 
       12*Log[-(s23/s4)]*Log[s4]) + 30*G[0, s4/s23, 1]*
      (Pi^2 - 12*Log[-(s23/s4)]^2 - 24*Log[-(s23/s4)]*Log[s4] - 
       12*Log[s4]^2) + 30*G[s4/s23, 1]*((-3*I)*Pi^3 + 12*G[0, 0, s4/s23, 1] + 
       6*G[0, s4/s23, s4/s23, 1] - Pi^2*Log[-(s23/s4)] - 
       12*G[0, s4/s23, 1]*Log[-(s23/s4)] + 4*Log[-(s23/s4)]^3 + 
       10*Pi^2*Log[s4] - 12*G[0, s4/s23, 1]*Log[s4] + 
       12*Log[-(s23/s4)]^2*Log[s4] + (12*I)*Pi*Log[s4]^2 + 
       12*Log[-(s23/s4)]*Log[s4]^2 + 6*Zeta[3]) + 
     4*(14*Pi^4 - 180*G[0, 0, 0, s4/s23, 1] - 90*G[0, 0, s4/s23, s4/s23, 1] - 
       45*G[0, s4/s23, s4/s23, s4/s23, 1] + 180*G[0, 0, s4/s23, 1]*
        Log[-(s23/s4)] + 90*G[0, s4/s23, s4/s23, 1]*Log[-(s23/s4)] + 
       (30*I)*Pi^3*Log[s4] + 180*G[0, 0, s4/s23, 1]*Log[s4] + 
       90*G[0, s4/s23, s4/s23, 1]*Log[s4] - 15*Pi^2*Log[s4]^2 - 
       (90*I)*Pi*Zeta[3] + 90*Log[s4]*Zeta[3]))/(360*(s23 - s4))}, 
  {0, 0, 0, (6*Zeta[3])/s12, (Pi^4/10 - 12*((-I)*Pi + Log[s12/s4] + Log[s4])*
      Zeta[3])/s12}, {-1/2*s4, s4*((-I)*Pi + Log[s4]), 
   (s4*(Pi^2 + 12*(Pi + I*Log[s4])^2))/12, 
   -1/6*(s4*(Pi^2*((-I)*Pi + Log[s4]) - 4*((-I)*Pi + Log[s4])^3 - 
      32*Zeta[3])), (s4*(19*Pi^4 - 40*Pi^2*(Pi + I*Log[s4])^2 - 
      80*(Pi + I*Log[s4])^4 + (2560*I)*(Pi + I*Log[s4])*Zeta[3]))/240}}}
