# One-external-mass double-box

The system is defined by `system/t331ZZZM_DEs.m` as the nested list:

```
{
    {kinematic variables},
    {master integrals},
    {connection matrices}
}
```

## Dataset generation

### Production of source files

The master integrals `j` and differential equations for the integral family `t331ZZZM` are defined in `t331ZZZM/system/t331ZZZM_DEs.m`.

#### Dimensionful integrals

We use the integrals as they are (ie. we do not normalise dimensions).

From `t331ZZZM/system/t331ZZZM_DEs.m`, we extract an analytic form of the integrals as an $\epsilon$ series in terms of MPLs and logs with the appropriate analytic continuations.
This uses: `t331ZZZM/system/t331ZZZM_reduction_to_utbasis.m`, expressions of the master integrals `j[fam,...]` in terms of pure basis master integrals `mi[fam,i]`; and `t331ZZZM/system/t331ZZZM_mi2spfuncs.m`, expressions of the pure basis master integrals `mi[fam,i]` in terms of special functions (MPLs and logs) and constants.
The mapping is performed by the script,
```shell
math -script t331ZZZM/system/convert.wl
```
It outputs the file `t331ZZZM/solution.m`, which is included in the repo.

The `t331ZZZM/system/convert.wl` script also formats the differential equations as connection matrices for each variable and $\epsilon$ order.
It outputs these matrices flattened as a 1d list in the order `(epsilon order, input variable, matrix_row, matrix_column)`.
Common factors are collected and output as `t331ZZZM/system/factors.m`, while the differential equation coefficients list is output in terms of the common factors as `t331ZZZM/system/collected.m`.
A Python file `t331ZZZM/connection_matrix.py` to define the connection matrices is manually prepared from a template produced by
```shell
./mma2py.py --full < t331ZZZM/system/factors.m > t331ZZZM/connection_matrix.py
./mma2py.py < t331ZZZM/system/collected.m >> t331ZZZM/connection_matrix.py
```
Note that `y(#)` must be replaced with `y[#]`, for example with this Vim regex,
```vim
:%s/y(\(\d\+\))/y[\1]/g
```
The file is autoformatted after editing with
```shell
black t331ZZZM/connection_matrix.py
```
The result is included in the repo.

Note that because the integrals are dimensionful and we scale the variables, the integrals pick up a scaling factor (which is always one in the dimensionless case).

#### Dimensionless integrals

It is also possible to use a basis of dimensionless integrals.
However, we find that training performance is worse than with the dimensionful integrals for this family.
The dimension normalisation procedure is detailed here in case it is used for further work.

To normalise the dimensions of the integrals,
```shell
math -script t331ZZZM/system/dimless_DE.wl
```
This outputs the file `t331ZZZM/system/t331ZZZM_MIs_and_DEs_dimless.m`.
It can be substituted for the file `t331ZZZM/system/t331ZZZM_DEs.m` in the steps of the "Dimensionful integrals" section to produce the processed files.

### Production of data files

Input datasets (phase space points) are generated with `t331ZZZM/generate_ps.py`.
For output datasets, we have `t331ZZZM/generate_fn.wl`, which evaluates `t331ZZZM/solution.m` at the phase space points, depending on `handyG` for the evaluation of MPLs.
For example,

```shell
./t331ZZZM/generate_ps.py -p 6 6 10000
math -script t331ZZZM/generate_fn.wl
```

Pre-generated datasets are available on [Hugging Face](https://huggingface.co/datasets/feynman-integrals-nn/t331ZZZM).

## Training

For replica `<replica>` of part `<part>`,

```shell
./train.py t331ZZZM <part> <replica>
./test.py -p t331ZZZM <part> <replica>
```

## Timing

Epoch (512x256) time:

| Machine   | Device                                | RAM (GB) | Type | Partitions | Time (s) |
| --------- | ------------------------------------- | -------- | ---- | ---------- | -------- |
| Laptop 1  | Nvidia GeForce RTX 3050 Ti Laptop GPU | 4        | cuda | 1          | 15       |
| Desktop 1 | Nvidia GeForce GTX 1080 Ti GPU        | 11       | cuda | 1          | 27       |
| Laptop 2  | Nvidia GeForce GTX 1050 Mobile GPU    | 4        | cuda | 1          | 36       |
| Laptop 1  | Intel Core i7-12700H CPU `-j6`        | 32       | cpu  | 1          | 53       |
| Desktop 1 | Intel Core i5-6600K CPU               | 16       | cpu  | 2          | 76       |
| Desktop 2 | AMD Ryzen 7 5700G CPU                 | 32       | cpu  | 1          | 103      |
| Laptop 2  | Intel Core i7-7700HQ CPU              | 16       | cpu  | 1          | 106      |
| Server 1  | Intel Xeon Gold 6130 CPU `-j16`       | 768      | cpu  | 1          | 112      |
| Laptop 3  | Apple M1 Pro (6+2) CPU `-j6`          | 16       | cpu  | 1          | 135      |
