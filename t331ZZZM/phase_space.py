import math
import torch

from finn.box1m.phase_space import PhaseSpace as PhaseSpace1L


class PhaseSpace(PhaseSpace1L):
    def a_spur(self, s):
        """
        spurious regulated: s12 - s4 > cut, s4 - s23 > cut, s12 + s23 > cut
        last one is only addition compared to one loop
        these are actually always satisfied by phys cut for current PS choice
        """
        s12 = self.const
        s23 = s[0]
        s4 = s[1]

        return torch.stack(
            (
                s12 - s4,
                s4 - s23,
                s12 + s23,
            )
        )
