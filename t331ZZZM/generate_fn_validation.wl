(* ::Package:: *)

Install["handyG"];


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


cfg = Association @@ Import["config.json"];


s12n = "const" /. cfg["phase_space"]
trans = {t_, v_} -> {s12 -> s12n, s23 -> t, s4 -> v};


{mpls, sol} = Get["solution.m"];
geval[ps_] := Module[{gg = Thread[mpls->(mpls/.#)]}, N[sol /. gg /. #]] & /@ ps;


bvPS = Import["validation_input.csv"] /. trans;


bv = geval[bvPS];


flat[part_, x_] := Flatten /@ part @ x;


Export["re/validation_output.csv", flat[Re, bv]];
Export["im/validation_output.csv", flat[Im, bv]];
