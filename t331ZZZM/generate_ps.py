#!/usr/bin/env python3

import numpy

from finn.box1m.generate_ps import boundaries
from finn.lib.generate_ps import generate_dataset_inputs


if __name__ == "__main__":
    generate_dataset_inputs("t331ZZZM", boundaries)
