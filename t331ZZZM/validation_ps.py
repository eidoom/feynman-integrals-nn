#!/usr/bin/env python3

import torch

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.generate_ps import generate
from finn.t331ZZZM.phase_space import PhaseSpace


if __name__ == "__main__":
    root = "t331ZZZM"
    cfg = get_cfg(root)

    scale, const, cut, _ = ps_params(cfg)

    print(f"Using scale = {scale}, const = {const}, cut = {cut}")

    dtype = torch.float64
    device = torch.device("cpu")

    phase_space = PhaseSpace(scale, const, cut, _, dtype, device)

    generate(
        root,
        "validation",
        phase_space,
        100000,
        3,
    )
