#!/usr/bin/env python3

import pathlib, json, math

import numpy, matplotlib.pyplot

from finn.lib.configuration import get_cfg
from finn.lib.testing import (
    get_abs_diff,
    get_args,
    get_labels,
    get_rel_diff,
    get_testing_data,
    init_model,
    plot_abs_diff_cum,
    plot_rel_diff_cum,
    plot_rel_diff_ps,
    plot_training_statistics,
)


def fn_err(part, means, labels, err_type):
    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

    ax.set_xlabel(f"Mean {err_type} difference")
    ax.set_ylabel("function index, $\epsilon$ power")

    ax.set_xscale("log")

    ax.barh(
        numpy.arange(len(means)),
        means,
        tick_label=labels,
    )

    ax.tick_params(axis="y", which="major", labelsize=4)

    for s in ("pdf", "png"):
        fig.savefig(part / f"fn_{err_type[:3]}_err.{s}", bbox_inches="tight")

    matplotlib.pyplot.close()


if __name__ == "__main__":
    args = get_args()
    root = pathlib.Path("t331ZZZM")
    part = root / args.part
    rep = part / args.replica

    plot_training_statistics(rep)

    cfg = get_cfg(root, part, args.override)

    model = init_model(rep, cfg)

    x, ff, nn = get_testing_data(root, part, cfg, model)

    rd, zi, nonzero = get_rel_diff(ff, nn, chop=True)
    nzx = numpy.delete(x, zi, axis=1)

    ad = get_abs_diff(ff, nn)

    plot_rel_diff_cum(rep, rd)

    plot_abs_diff_cum(rep, ad)

    plot_rel_diff_ps(rep, rd, nzx, "$s_{23}$", "$s_{4}$", bins=20)

    if args.everything:
        labels = get_labels(cfg, part.stem)

        fn_err(rep, rd.mean(axis=0), labels[nonzero], "relative")
        fn_err(rep, ad.mean(axis=0), labels, "absolute")
