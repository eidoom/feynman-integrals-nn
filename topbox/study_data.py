#!/usr/bin/env python3

import pathlib, json
import torch, numpy, matplotlib.pyplot, matplotlib.cm

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import (
    hist_2d,
    load_input_csv,
    get_f_data,
    hist_f_cum,
    hist_f_eps,
)
from finn.topbox.phase_space import PhaseSpace


if __name__ == "__main__":
    root = pathlib.Path("topbox")

    cfg = get_cfg(root)

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    s12max, mt2, pcut, scut = ps_params(cfg)

    phase_space = PhaseSpace(s12max, mt2, pcut, scut, dtype, device)

    n = 1000000
    data_train = phase_space.test(n, cut_spurious=True).cpu().numpy()

    boundary_points = load_input_csv(root / "boundary_input.csv")
    testing_points = load_input_csv(root / "testing_input.csv")

    hist_2d(
        root,
        data_train,
        boundary_points,
        testing_points,
        "$s_{12}$",
        "$s_{23}$",
        lloc="lower left",
    )

    values = get_f_data(root, cfg)
    if values.size:
        values = abs(values)

        hist_f_eps(root, cfg, values)

        hist_f_cum(root, values)
