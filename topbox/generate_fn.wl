(* ::Package:: *)

(* vim: set ft=mma: *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


cacheDir = "cache"
If[
  DirectoryQ@cacheDir,
  DeleteDirectory[cacheDir, DeleteContents->True];
];


(* nb needs amflow commit b510cb20020fdd226c54f9c2cd0eefa7f5900b32 or later *)
Get[FileNameJoin @ {"~", "git", "amflow", "AMFlow.m"}];
SetReductionOptions["IBPReducer" -> "FiniteFlow+LiteRed"];


args = $CommandLine[[4;;]];
name = args[[1]];
n = ToExpression@args[[2]];


loops = 2;
(* need mis from eps^-4 up to eps^0 *)
minepsmis = -4;
maxepsmis = 0;
(* and the js from eps^-4 up to eps^0 *)
maxepsorder = 0;
(* AMFlow starts js at eps^(-2L) = eps^(-4) *)
(* AMFlow takes the number of steps in eps orders as argument *)
(* ie. (number of eps orders) - 1 = highest weight *)
epsordersteps = 2 * loops + maxepsorder;
precision = 10;
threads = 64;


point = Import[name <> "_input.csv"][[n]];


cfg = Association @@ Import["config.json"];


{ints, sijs, des} = Get[FileNameJoin @ {"system", "topbox_MIs_and_DEs_dimless-polefree-expanded.m"}];


js = Union@Cases[ints, _j, Infinity];


mt2n = "const" /. cfg["phase_space"];
fullPoint = Append[point, mt2n];


Numeric = Thread[sijs -> (Rationalize@fullPoint)];


Internal = {k1, k2};
External = {p1, p2, p3, p4};
Invariants = sijs;
Conservation = {p4 -> -p1 - p2 - p3};
Replacement = {
 p1^2 -> 0,
 p2^2 -> 0,
 p3^2 -> mt2,
 p4^2 -> mt2,
 p1*p2 -> s12/2, 
 p1*p3 -> (mt2-s12-s23)/2, 
 p2*p3 -> (s23-mt2)/2,
 Nothing
};
InverseReplacement = {
  s12 -> 2 p1*p2,
  s23 -> 2 p2*p3 + p3^2,
  mt2 -> p3^2,
  Nothing
};


Propagator = {
  k1^2 - mt2,
  (k1 - p1)^2 - mt2,
  (k1 - p1 - p2)^2 - mt2,
  (k2)^2,
  (k2 + p1 + p2 + p3)^2 - mt2,
  (k2 + p1 + p2)^2,
  (k1 + k2)^2 - mt2,
  (k1 - p1 - p2 - p3)^2,
  (k2 + p1)^2,
  Nothing
};


AMFlowInfo["Family"] = topbox;
AMFlowInfo["Loop"] = Internal;
AMFlowInfo["Leg"] = External;
AMFlowInfo["Conservation"] = Conservation;
AMFlowInfo["Replacement"] = Replacement;
AMFlowInfo["Propagator"] = Propagator;
AMFlowInfo["Numeric"] = Numeric;
AMFlowInfo["NThread"] = threads;


safetyFilename = name <> "_js_" <> ToString[n] <> ".m";
If[
  FileExistsQ[safetyFilename]
  ,
  Print["Using j cache ", safetyFilename];
  jn = Get[safetyFilename];
  ,
  jn = SolveIntegrals[js, precision, epsordersteps];
  Put[jn, safetyFilename];
];


sol = Series[ints /. Numeric /. (jn /. Rule[a_, b_] :> (a -> b + FAIL * eps^(maxepsorder + 1))), {eps, 0, maxepsmis}]//Normal;


Print["check mis highest order:", FreeQ[Variables@sol, FAIL]];
Print["check mis lowest order:", DeleteCases[Coefficient[sol,eps,minepsmis-1],0]==={}];


solCoeffs = Flatten@Transpose@Table[Coefficient[sol,eps,i],{i,minepsmis,maxepsmis}];


filename = name <> "_output_" <> StringPadLeft[ToString@n, 3, "0"] <> ".csv";
Export[FileNameJoin @ {"re", filename}, {Re@solCoeffs}];
Export[FileNameJoin @ {"im", filename}, {Im@solCoeffs}];


If[
  DirectoryQ@cacheDir,
  DeleteDirectory[cacheDir, DeleteContents->True];
];


Quit[];
