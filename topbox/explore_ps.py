#!/usr/bin/env python3

import pathlib, math
import torch, numpy, matplotlib.pyplot, matplotlib.cm

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import (
    get_a_data,
    hist_a_cum,
    hist_a_eps,
    hist_a_2d,
)
from finn.topbox.phase_space import PhaseSpace
from finn.topbox.connection_matrix import ConnectionMatrix


def alphabet(s, mt2):
    s12 = s[0]
    s23 = s[1]
    mt2 = torch.full_like(s12, mt2)

    return torch.stack(
        (
            mt2,
            s12,
            -s23,
            s12 + (mt2 - s23) ** 2 / s23,
            4 * mt2 + s12,
            mt2 - s23,
            (-3 * mt2 - s23),
            s12 - 4 * mt2,
            9 * mt2 - s23,
            (s12 + s23 - mt2),
            9 * mt2 * s12 + 4 * mt2 * s23 - s12 * s23 - 4 * mt2**2,
            -64 * mt2**3 + 9 * mt2**2 * s12 - 10 * mt2 * s12 * s23 + s12 * s23**2,
        )
    ).abs()


def hist_letters(root, data_letters, log=True, s=("pdf", "png")):
    colours = matplotlib.cm.tab20(range(data_letters.shape[1]))

    labels = (
        r"$m_t^2$",
        r"$s_{12}$",
        r"$s_{23}$",
        r"$4 m_t^2 + s_{12}$",
        r"$m_t^2 - s_{23}$",
        r"$3 m_t^2 + s_{23}$",
        r"$4 m_t^2 - s_{12}$",
        r"$9 m_t^2 - s_{23}$",
        r"$m_t^2 - s_{12} - s_{23}$",
        r"$\ell_{10}((m_t^2)^2)$",
        # r"$(m_t^2)^2 - 2 m_t^2 s_{23} + s_{12} s_{23} + s_{23}^2$",
        r"$\ell_{11}((m_t^2)^2)$",
        # r"$4 (m_t^2)^2 - 9 m_t^2 s_{12} - 4 m_t^2 s_{23} + s_{12} s_{23}$",
        r"$\ell_{12}((m_t^3)^2)$",
        # r"$64 (m_t^2)^3 - 9 (m_t^2)^2 s_{12} + 10 m_t^2 s_{12} s_{23} - s_{12} s_{23}^2$",
    )

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$\ell_{i}$")
    ax.set_ylabel("Proportion")

    if log:
        data_letters = abs(data_letters)
        bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data_letters), bins="auto")

        ax.set_xscale("log")
        ax.set_yscale("log")
    else:
        bins = numpy.histogram_bin_edges(data_letters, bins="auto")

    ax.hist(
        data_letters,
        bins=bins,
        weights=numpy.full_like(data_letters, 1 / len(data_letters)),
        histtype="step",
        label=labels,
        color=colours,
    )

    ax.legend(reverse=True, ncols=3)

    for ss in s:
        fig.savefig(root / f"dist_letters.{ss}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_each(root, d, const, s=("pdf", "png")):
    s12m, s23m = d.mean(axis=0)

    colours = matplotlib.cm.tab10(range(3))

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$s_{i}$")
    ax.set_ylabel("Proportion")

    ax.hist(
        d,
        bins=numpy.histogram_bin_edges(d, bins="auto"),
        weights=numpy.full_like(d, 1 / len(d)),
        histtype="step",
        label=(r"$s_{12}$", r"$s_{23}$"),
        color=colours[1:],
    )

    ax.axvline(const, color=colours[0], label=r"$m_t^2$")
    ax.axvline(s12m, color=colours[1])
    ax.axvline(s23m, color=colours[2])

    ax.legend(reverse=True)

    for ss in s:
        fig.savefig(root / f"dist_xs.{ss}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_all(root, d, s=("pdf", "png")):
    d = numpy.ravel(d)
    m = d.mean()
    q = numpy.median(d)

    colours = matplotlib.cm.tab10(range(3))

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$\sum s_{i}$")
    ax.set_ylabel("Proportion")

    ax.set_xscale("log")

    ax.axvline(m, label="mean", color=colours[1])
    ax.axvline(q, label="median", color=colours[2])

    ax.hist(
        d,
        bins=10 ** numpy.histogram_bin_edges(numpy.log10(d), bins="auto"),
        # bins=numpy.histogram_bin_edges(d, bins="auto"),
        weights=numpy.full_like(d, 1 / len(d)),
        histtype="step",
        label="dist",
        color=colours[0],
    )

    ax.legend()

    for ss in s:
        fig.savefig(root / f"dist_x.{ss}", bbox_inches="tight")

    matplotlib.pyplot.close()


def scale_range(x, d=1e-3):
    x = abs(numpy.ravel(x))
    x = x[x != 0]

    ql = math.log10(numpy.quantile(x, d))
    qr = math.log10(numpy.quantile(x, 1 - d))

    return qr - ql


def get_stats(x, name):
    x = numpy.ravel(x)
    x = x[x != 0]

    asr = scale_range(x)
    am = numpy.median(x)

    print(f"{name} scale range: {asr:.1f}")
    print(f"{name} median: {am:.1f}")


if __name__ == "__main__":
    root = pathlib.Path("topbox")
    cfg = get_cfg(root)

    scale, const, cut, _ = ps_params(cfg)

    print(f"Using scale = {scale}, mt2 = {const}, cut = {cut}")

    dtype = torch.float64
    device = torch.device("cpu")

    phase_space = PhaseSpace(scale, const, cut, _, dtype, device)

    n = 1000000
    s = phase_space.test(n, cut_spurious=True)

    sabs = s.abs()
    sabs = sabs.T.cpu().numpy()

    ssr = scale_range(sabs)
    print(f"s scale range: {ssr:.1f}")
    print(f"s median: {numpy.median(sabs):.1f}")

    # print("plotting x")
    # hist_all(root, sabs)

    m = 30000
    s2 = s[:, :m]

    print(f"calculating A over {m} points")
    c = abs(get_a_data(cfg, ConnectionMatrix, s2, const))

    get_stats(c, "A")

    hist_a_cum(root, c)

    s2 = s2.cpu().numpy()
    hist_a_2d(root, s2, c, "$s_{12}$", "$s_{23}$")

    extra = False
    if extra:
        print("plotting extras")

        hist_each(root, sabs, const)

        hist_a_by_eps(root, c, cfg)

        a = alphabet(s, const)
        a = a.T.cpu().numpy()
        hist_letters(root, a)
