import math
import torch


from finn.lib.phase_space import PhaseSpaceBase


class PhaseSpace(PhaseSpaceBase):
    def points(self, n):
        # s12, s23(, mt2=const)

        # 2 * cut <= s12 <= scale
        s12 = self.uniform(2 * self.cut_phys, self.scale, n)

        # -s12 + cut <= s23 <= -cut
        s23 = self.uniform(-s12 + self.cut_phys, -self.cut_phys, n)

        return torch.stack((s12, s23))

    def a_phys(self, s):
        """
        s12 channel: -s23 > 0, (mt2 > 0,) s12 + (mt2 - s23)^2 / s23 > 0
        nb s12 > 0 already constrained by last inequality above
        """
        s12 = s[0]
        s23 = s[1]
        mt2 = self.const

        return torch.stack(
            (
                -s23,
                s12 + (mt2 - s23) ** 2 / s23,
            )
        )

    def a_spur(self, s):
        s12 = s[0]
        s23 = s[1]
        mt2 = self.const

        return torch.stack(
            (
                4 * mt2 + s12,
                mt2 - s23,
                -mt2 - s23,
                s12 - 4 * mt2,
                9 * mt2 - s23,
                s12 + s23 - mt2,
                9 * mt2 * s12 + 4 * mt2 * s23 - s12 * s23 - 4 * mt2**2,
                -64 * mt2**3
                + 9 * mt2**2 * s12
                - 10 * mt2 * s12 * s23
                + s12 * s23**2,
            )
        ).abs()
