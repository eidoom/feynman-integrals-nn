#!/usr/bin/env python3

import torch

from finn.lib.generate_ps import generate_dataset_inputs


def boundaries(ps, n):
    # see system/phase_space.wl
    assert n == 5

    mt2 = ps.const
    cut = ps.cut_phys

    s12min = cut + 4 * mt2
    s12max = ps.scale
    s12mid = 0.5 * (s12min + s12max)

    s12 = torch.stack((s12max, s12max, s12mid, s12mid, s12min))

    sign = torch.tensor((-1, 1, -1, 1, 1), dtype=ps.dtype, device=ps.device)

    s23 = 0.5 * (cut + 2 * mt2 + sign * ((cut - s12) * (s12min - s12)).sqrt() - s12)

    return torch.stack((s12, s23)).T.cpu().numpy()


if __name__ == "__main__":
    generate_dataset_inputs("topbox", boundaries, rationalised=True)
