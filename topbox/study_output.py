#!/usr/bin/env python3

import pathlib
import torch, numpy, matplotlib.pyplot

from finn.lib.configuration import get_cfg, ps_params
from finn.lib.testing import init_model
from finn.lib.study_data import hist_f_cum, hist_f_ps, hist_f_eps, hist_f_f
from finn.topbox.phase_space import PhaseSpace


if __name__ == "__main__":
    root = pathlib.Path("topbox")
    cfg = get_cfg(root)
    part = root / "re"

    scale, const, cut, _ = ps_params(cfg)

    print(f"Using scale = {scale}, mt2 = {const}, cut = {cut}")

    dtype = getattr(torch, cfg["dtype"])

    print("gen ps...")
    phase_space = PhaseSpace(scale, const, cut, _, dtype, cfg["device"])

    n = 900000
    x = phase_space(n, cut_spurious=False).T

    print("run model...")
    model = init_model(part, cfg)

    with torch.no_grad():
        output = model(x)

    x = x.T.cpu().numpy()

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    output = output.view(n, num_fn, num_eps)
    output = output.cpu().numpy()

    hist_f_cum(part, output, name="dist_output_cum")
    # hist_f_eps(part, cfg, output, name="dist_output_eps")
    # hist_f_f(part, cfg, output, name="dist_output_f")
    # hist_f_ps(
    #     part,
    #     x,
    #     output,
    #     r"$s_{12}$",
    #     r"$s_{23}$",
    #     name="dist_output_ps",
    #     q="sum",
    #     bins=50,
    # )
