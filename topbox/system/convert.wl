(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


{js,sijs,des}=Get["topbox_MIs_and_DEs_dimless-polefree-expanded.m"];


sijs


js//Length


(* these indices for list `js` are zero when truncating above eps^0 *)
(* deepest pole is 1/eps^2 => 3 eps orders *)
zeros={2,3,4,5,6,7,8,9,10,19,21};


nonzeros=Complement[Range[44],zeros]


jseps0 = js[[nonzeros]]


jseps0//Length


des//Dimensions


desRed = des[[;;2,nonzeros,nonzeros]];


desRed//Dimensions


(* if computing to functions to eps^2, take full basis (size 44) and des up to eps^4 (5 orders) *)
(* here, computing to eps^0 so take reduced basis and des up to eps^2 (3 order) *)
desCoeff = Table[Coefficient[desRed,eps,i],{i,0,2}];


desCoeff//Dimensions


desFlat=Flatten@desCoeff;


desFlat//Dimensions


desSimp=Simplify@desFlat;


Put[desSimp,"des.m"];


desFS=FullSimplify@desSimp;


CollectFactorsX[EXPR_] := Module[{tmp,depth,coeff},
  depth = 1;
  If[MatchQ[EXPR[[0]],Plus], tmp = List@@EXPR; depth+=1;, tmp = EXPR;];
  coeff = Replace[EXPR,{Power[expr_,i_Integer]:>FFF[expr]^i /; i>0, Power[expr_,i_Integer]:>FFF[expr]^i /; i<0, Plus[expr_]:>FFF[Plus[expr]]},{depth}];
  Return[coeff];
];

FindIndependentFactorsX[Coefficients_List] := Module[{vars,rules,coeffs},
  coeffs = CollectFactorsX /@ Coefficients;
  vars = Join@@(Select[Variables[#],!MatchQ[#,FFF[_?NumericQ]]&]& /@ coeffs);
  vars = DeleteDuplicates[vars];
  rules =  MapIndexed[Rule[#1,y@@#2]&,vars];
  coeffs = coeffs //. Dispatch[rules] /. FFF[a_]:>a;
  Return[{coeffs,rules /. Rule[a_,b_]:>Rule[b,a /. FFF[x_]:>x]}];
];


{collected, factors} = FindIndependentFactorsX@desFS;


Put[factors//Values,"factors.m"]


Put[collected/.{0->z,y[i_]:>y[i-1]}, "collected.m"]
