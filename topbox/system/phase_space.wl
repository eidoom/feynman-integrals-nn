(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* PS parametrisation *)


Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
z ={0,0,1};
e = Sqrt[s]/2;
dot[x_, y_] := x[[1]]*y[[1]] - x[[2;;]] . y[[2;;]];
n = 4;


p[1]=e Prepend[-z,-1];
p[2]=e Prepend[z,-1];
p[3]=e Prepend[Ry[\[Theta]] . z,1];

p[n]=-Plus@@p/@Range[n-1]//Factor;

{MatrixForm[p[#]]&/@Range[n]}//TableForm


Plus@@p/@Range[n]//Simplify
dot[p[#],p[#]]&/@Range[n]//Simplify


sijs=FullSimplify[{2*dot[p[1], p[2]], 2*dot[p[2],p[3]]}]


sijs//InputForm


(* cuts *)


physReg = Get["s12-channel.m"]


boundaries=List@@(physReg/.{a_>b_:>a-b,a_<b_:>b-a})


alphabet = Get["topbox_alphabet.m"]


{js,sijs,des}=Get["topbox_MIs_and_DEs_dimless-polefree-expanded.m"];


desDenoms = Denominator@Together@Flatten@Table[Coefficient[des[[;;2]],eps,i],{i,0,4}];


facs[in_]:=Simplify@Union@DeleteCases[Flatten[FactorList/@DeleteDuplicates@in,1][[;;,1]],_Integer]


a2=facs@facs@desDenoms


(* equation for s23 on boundary 3 *)
s12eq=cut - (mt2 - s23)^2 / s23;
s23eq=Assuming[
{{cut,mt2,s12,s23}\[Element]Reals,s23<0,s12>0,cut>0},
Simplify@Solve[s12eq==s12, s23][[;;,1,2,1]]
]
s23eq[[1]]//InputForm


(* minimise s12 on boundary 3 *)
Solve[D[s12eq,s23]==0,s23][[1,1]]
s12min=cut - (mt2 - s23)^2 / s23 /. %


(* s23 extrema *)
s23eq/.{s12->scale}


Module[
{scale, cut, mt2},
scale = 5;
mt2 = 1;
cut = 0.1 * scale;
Plot[
 {
  -cut,
  (cut + 2*mt2 - Sqrt[(cut - s12)*(cut + 4*mt2 - s12)] - s12)/2,
  (cut + 2*mt2 + Sqrt[(cut - s12)*(cut + 4*mt2 - s12)] - s12)/2
  },
 {s12,0,scale},
 AxesLabel->{"s12","s23"},
 PlotLegends->{"-cut","-sqrt","+sqrt"}
]
]


(* nb for LHC and top mass, scale' = (2E=13.6 TeV)^2 / (mt=172.76 GeV)^2 * const *)
Manipulate[
Module[
{c,d,pscale,prescale,pconst,pcut,mt2n,scale,cut},
c=-3;
d=2;
pscale=ss;
prescale=mm;
pconst=10^pp;
pcut=10^cc;
mt2n=pconst;
scale = pconst*pscale/prescale;
cut = scale*pcut;
RegionPlot[
{
 And@@{
 -s23 > cut,
 s12 + (mt2n - s23)^2 / s23 > cut
 },
 Or@@{
4 mt2n+s12<cut,
mt2n-s23<cut,
-(3 mt2n+s23)<cut,
-(4 mt2n-s12)<cut,
9 mt2n-s23<cut,
-(mt2n-s12-s23)<cut
},
Or@@{
-(4 mt2n^2-9 mt2n s12-4 mt2n s23+s12 s23)<cut,
-(64 mt2n^3-9 mt2n^2 s12+10 mt2n s12 s23-s12 s23^2)<cut
},
},
{s12,0,scale},{s23,-scale,0},
FrameLabel -> {"s12", "s23"}
]
],
{{pp,c,"const"},c-d,c+d},
{{cc,-1,"cut proportion"},-3,0},
{{mm,29846,"mt2"},10^3,10^5},
{{ss,1.8*10^8,"s12"},10^7,10^9}
]


Module[
{scale, pcut, mt2n,scut},
scale = 5;
mt2n = 1;
pcut = 0.1*scale;
scut=0.01*scale;
Print[scale," ",mt2n," ",pcut," ",scut];
phys=And@@{
 -s23 > pcut,
 s12 + (mt2n - s23)^2 / s23 > pcut
};
spur=And@@{
 Abs[4 mt2n+s12]>scut,
 Abs[mt2n-s23]>scut,
 Abs[mt2n+s23]>scut,
 Abs[-(4 mt2n-s12)]>scut,
 Abs[9 mt2n-s23]>scut,
 Abs[-(mt2n-s12-s23)]>scut,
 Abs[-(4 mt2n^2-9 mt2n s12-4 mt2n s23+s12 s23)]>scut,
 Abs[-(64 mt2n^3-9 mt2n^2 s12+10 mt2n s12 s23-s12 s23^2)]>scut
};
RegionPlot[
 {phys, And@@{phys, spur}},
 {s12,0,scale},{s23,-scale,0},
 FrameLabel -> {"s12", "s23"},
 PerformanceGoal->"Quality",
 MaxRecursion->10
]
]


Module[
{scale, pcut, scut, mt2n},
scale = 5;
mt2n = 1;
pcut = 0.1*scale;
scut=0.01*scale;
all=-s12<s23<-pcut&&pcut<s12<scale; (* using current PS gen setup *)
alla=Area@ImplicitRegion[all,{s12,s23}];
phys=And@@{
 -s23 > pcut,
 s12 + (mt2n - s23)^2 / s23 > pcut,
 s12 < scale,
 -s23 < scale
};
physa=Area@ImplicitRegion[phys,{s12,s23}];
spur=And@@{
 phys,
 Abs[4 mt2n+s12]>scut,
 Abs[mt2n-s23]>scut,
 Abs[mt2n+s23]>scut,
 Abs[-(4 mt2n-s12)]>scut,
 Abs[9 mt2n-s23]>scut,
 Abs[-(mt2n-s12-s23)]>scut,
 Abs[-(4 mt2n^2-9 mt2n s12-4 mt2n s23+s12 s23)]>scut,
 Abs[-(64 mt2n^3-9 mt2n^2 s12+10 mt2n s12 s23-s12 s23^2)]>scut
};
spura=Area@ImplicitRegion[spur,{s12,s23}];
Print["scales ", N@Min[pcut,mt2n], "--", N@scale];
Print["efficiency ", PercentForm@N[spura/alla]];
Print["training coverage ", PercentForm@N[spura/physa]];
]



