# Top-box

The system is defined by `system/topbox_MIs_and_DEs_dimless-polefree-expanded.m` as the nested list:

```
{
    {master integrals},
    {kinematic variables},
    {connection matrices}
}
```

## Dataset generation

Mathematica script depends on `AMFlow` for evaluation of integrals.

For example,

```shell
./topbox/generate_ps.py -p 5 20 100
cd topbox
./generate_fn.sh boundary 1 20
./generate_fn.sh testing 1 100
cd ..
```

Pre-generated datasets are available on [Hugging Face](https://huggingface.co/datasets/feynman-integrals-nn/topbox).

## Training

For replica `<replica>` of part `<part>`,

```shell
./train.py topbox <part> <replica>
./test.py -p topbox <part> <replica>
```

## Timing

Epoch (512x256) time:

| Machine   | Device                                | RAM (GB) | Type | Partitions | Time (s) |
| --------- | ------------------------------------- | -------- | ---- | ---------- | -------- |
| Desktop 1 | Nvidia GeForce GTX 1080 Ti GPU        | 11       | cuda | 4          | 71       |
| Laptop 1  | Nvidia GeForce RTX 3050 Ti Laptop GPU | 4        | cuda | 16         | 74       |
| Laptop 2  | Nvidia GeForce GTX 1050 Mobile GPU    | 16       | cuda | 16         | 221      |
| Laptop 1  | Intel Core i7-12700H CPU `-j6`        | 32       | cpu  | 1          | 337      |
| Desktop 1 | Intel Core i5-6600K CPU               | 16       | cpu  | 4          | 545      |
| Server 1  | Intel Xeon Gold 6130 CPU `-j16`       | 768      | cpu  | 1          | 567      |
| Laptop 2  | Intel Core i7-7700HQ CPU              | 16       | cpu  | 2          | 803      |

Note that these tests were performed for an alternative version of this system than the currently committed one with:
* architecture
    ```json
    "hidden_layers": [512, 512, 512],
    ```
* dimensions
    ```json
    "output": {
        "functions": 44,
        "epsilon_orders": 5
    },
    "connection": {
        "epsilon_orders": 5
    }
    ```
    for functions of eps-order -4 to 0 (with the first two orders being all zero valued)
