#!/usr/bin/env bash

trap "exit" SIGINT

d=$1
is=$(seq $2 $3)

for i in ${is[@]}; do
    rm -rf cache/
    math -script generate_fn.wl $d $i
    rm -rf cache/
done
