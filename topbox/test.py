#!/usr/bin/env python3

import pathlib

import numpy

from finn.lib.configuration import get_cfg
from finn.lib.testing import (
    get_abs_diff,
    get_args,
    get_rel_diff,
    get_testing_data,
    init_model,
    plot_abs_diff_cum,
    plot_abs_diff_eps,
    plot_abs_diff_ps,
    plot_rel_diff_cum,
    plot_rel_diff_ps,
    plot_training_statistics,
)


if __name__ == "__main__":
    args = get_args()
    root = pathlib.Path("topbox")
    part = root / args.part
    rep = part / args.replica
    data_root = args.datasets
    data_part = args.datasets / args.part

    plot_training_statistics(rep, p=4)

    cfg = get_cfg(root, part, args.override)

    model = init_model(rep, cfg)

    x, ff, nn = get_testing_data(data_root, data_part, cfg, model)

    ad = get_abs_diff(ff, nn, 2 if args.verbose else 1)

    rd, zi, nonzero = get_rel_diff(ff, nn, v=2 if args.verbose else 1)
    nzx = numpy.delete(x, zi, axis=1)

    plot_abs_diff_cum(rep, ad)

    plot_abs_diff_ps(rep, ad, x, "$s_{12}$", "$s_{23}$")

    plot_abs_diff_eps(rep, cfg, ad, ff)

    plot_rel_diff_cum(rep, rd)

    plot_rel_diff_ps(rep, rd, nzx, "$s_{12}$", "$s_{23}$")
