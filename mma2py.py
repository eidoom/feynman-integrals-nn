#!/usr/bin/env python3

from argparse import ArgumentParser
from sys import stdin
from sympy.parsing.mathematica import parse_mathematica

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--full", action="store_true")
    args = parser.parse_args()

    if args.full:
        print(
            """
import torch

from finn.lib.connection_matrix import ConnectionMatrixBase


class ConnectionMatrix(ConnectionMatrixBase):
    def raw(self, x):
        # edit as required
        s12 = self.const
        s23 = x[0]
        s4 = x[1]

        z = torch.zeros_like(s23)

        return torch.stack(
            """
        )

    print(parse_mathematica(stdin.read()))

    if args.full:
        print(")")
