# module generate_ps

import argparse, pathlib, importlib

import torch, numpy
from sympy import Rational

from finn.lib.configuration import ps_params, get_cfg


def get_args(name):
    parser = argparse.ArgumentParser(
        description=f"Generate boundary and testing input datasets for {name} example."
    )
    parser.add_argument(
        "boundary_size",
        type=int,
        help="Number of boundary value points.",
    )
    parser.add_argument(
        "testing_size",
        type=int,
        help="Number of testing points.",
    )
    parser.add_argument(
        "-p",
        "--poles",
        type=int,
        default=0,
        metavar="P",
        help="Define the number of boundary points P to be taken on the phase space region boundary rather than the bulk (default 0).",
    )
    parser.add_argument(
        "-b",
        "--boundary-rseed",
        type=int,
        default=1,
        metavar="B",
        help="Random number seed B to use for boundary bulk points (default 1).",
    )
    parser.add_argument(
        "-t",
        "--testing-rseed",
        type=int,
        default=2,
        metavar="T",
        help="Random number seed T to use for testing points (default 2).",
    )
    parser.add_argument(
        "-o",
        "--override",
        type=str,
        metavar="O",
        nargs="+",
        help="Paths to configuration files whose values override the default configuration files.",
    )
    return parser.parse_args()


def clear_caches(root):
    for f in ("boundary", "testing"):
        (root / f"{f}_input.pt").unlink(missing_ok=True)


@numpy.vectorize
def rationalise(x, d=100):
    return Rational(x).limit_denominator(d)


def generate(root, name, ps, n, rseed, p=0, boundaries=None, rationalised=False, d=100):
    print(
        f"Generating {name} input dataset with {n} "
        f"{'rationalised ' if rationalised else ''}point{'s' if n != 1 else ''}"
        f"{f' of which {p} are on region boundaries' if p else ''}."
    )

    torch.manual_seed(rseed)

    data = ps(n - p, cut_spurious=False).T.cpu().numpy()

    if boundaries is not None:
        boundary = boundaries(ps, p)
        data = numpy.concatenate((boundary, data))

    if rationalised:
        data = rationalise(data, d).astype(numpy.double)

    filename = pathlib.Path(root) / f"{name}_input.csv"
    print(f"Writing {filename}")
    numpy.savetxt(filename, data, delimiter=",")


def generate_dataset_inputs(name, boundaries, rationalised=False):
    args = get_args(name)
    root = pathlib.Path(name)
    mod_ps = importlib.import_module(f"finn.{name}.phase_space")

    cfg = get_cfg(root, override=args.override)

    scale, const, cut, _ = ps_params(cfg)

    print(f"Using scale = {scale}, const = {const}, cut = {cut}")

    dtype = torch.float64
    # torch.manual_seed(r) only deterministic on same device
    device = torch.device("cpu")

    phase_space = mod_ps.PhaseSpace(scale, const, cut, _, dtype, device)

    clear_caches(root)

    generate(
        root,
        "boundary",
        phase_space,
        args.boundary_size,
        args.boundary_rseed,
        p=args.poles,
        boundaries=boundaries,
        rationalised=rationalised,
    )

    generate(
        root,
        "testing",
        phase_space,
        args.testing_size,
        args.testing_rseed,
        rationalised=rationalised,
    )
