# module connection_matrix

import abc
import torch


class ConnectionMatrixBase(abc.ABC):
    def __init__(self, num_eps, num_in, num_fn, const):
        self.num_eps = num_eps
        self.num_in = num_in
        self.num_fn = num_fn
        self.const = const

    def __call__(self, x):
        de_tensor = self.raw(x)
        # ^ de_tensor.shape = (des_eps * num_in * num_fn * num_fn, batch_size)
        return (
            de_tensor.view(self.num_eps, self.num_in, self.num_fn, self.num_fn, -1)
            # ^ shape = (des_eps, num_in, num_fn, num_fn, batch_size)
            .movedim(4, 1)
            # ^ shape = (des_eps, batch_size, num_in, num_fn, num_fn)
            .contiguous()
        )

    @abc.abstractmethod
    def raw(self, x):
        ...
