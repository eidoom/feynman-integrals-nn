# module testing

import pathlib
import matplotlib.pyplot, matplotlib.cm, matplotlib.patheffects, numpy, torch


def load_input_csv(filename):
    return numpy.loadtxt(filename, delimiter=",").T


def hist_2d(
    root,
    data_input,
    boundary_points,
    testing_points,
    xlabel,
    ylabel,
    scale=None,
    padding=0.03,
    bins=50,
    name=None,
    s=("pdf", "png"),
    lloc="best",
    text=("boundary"),
):
    """
    this is only for double input examples
    """
    print("plotting PS distribution")

    colours = matplotlib.cm.tab10([3, 9])

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    _, _, _, im = ax.hist2d(
        data_input[0],
        data_input[1],
        bins=bins,
        density=True,
        cmin=1e-16,
    )

    if scale is not None:
        ax.set_xlim(0, scale)
        ax.set_ylim(-scale, 0)
    else:
        xl, xh = ax.get_xlim()
        xr = padding * (xh - xl)
        ax.set_xlim(xl - xr, xh + xr)

        yl, yh = ax.get_ylim()
        yr = padding * (yh - yl)
        ax.set_ylim(yl - yr, yh + yr)

    for dataset, label, colour in zip(
        (boundary_points, testing_points),
        ("boundary", "testing"),
        colours,
    ):
        if len(dataset):
            ax.scatter(*dataset, marker=".", label=label, color=colour)

            if label in text:
                for i, (x, y) in enumerate(zip(*dataset), start=1):
                    txt = ax.text(x, y, i, ha="left", va="bottom", c="w")
                    txt.set_path_effects(
                        [
                            matplotlib.patheffects.withStroke(
                                linewidth=1, foreground="black"
                            )
                        ]
                    )

    ax.legend(loc=lloc)

    fig.colorbar(im, label="Density")

    for f in s:
        fig.savefig(
            root / f"dist_2d{f'_{name}' if name is not None else ''}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()


def get_f_data(root, cfg):
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    n = num_fn * num_eps

    bc_re_f = pathlib.Path(root / "re" / "boundary_output.csv")
    if bc_re_f.is_file():
        boundary_values_re = numpy.loadtxt(bc_re_f, delimiter=",")
        boundary_values_im = numpy.loadtxt(
            root / "im" / "boundary_output.csv", delimiter=","
        )
        boundary_values = numpy.concatenate((boundary_values_re, boundary_values_im))
    else:
        boundary_values = numpy.empty((0, n))

    t_re_f = pathlib.Path(root / "re" / "testing_output.csv")
    if t_re_f.is_file():
        testing_values_re = numpy.loadtxt(t_re_f, delimiter=",")
        testing_values_im = numpy.loadtxt(
            root / "im" / "testing_output.csv", delimiter=","
        )
        testing_values = numpy.concatenate((testing_values_re, testing_values_im))
    else:
        testing_values = numpy.empty((0, n))

    return numpy.concatenate((boundary_values, testing_values))


def hist_f_cum(root, data, log=True, s=("pdf", "png"), name="dist_f_cum", v=True):
    if v:
        print("plotting functions cumulative distribution")

    data = abs(numpy.ravel(data))

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data[data != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_xlabel(r"nonzero $|\mathrm{Re}\mathrm{Im}\,f^{(j)}_{i}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
    )

    for f in s:
        fig.savefig(root / f"{name}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_f_eps(root, cfg, data, log=True, s=("pdf", "png"), name="dist_f_eps"):
    print("plotting functions distribution by eps order")

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    data = abs(data.reshape(data.shape[0] * num_fn, num_eps))

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data[data != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_xlabel(r"nonzero $|\mathrm{Re}\mathrm{Im}\,f^{(j)}_{i}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
        label=range(num_eps),
    )

    ax.legend(reverse=True, title=r"$j$")

    for f in s:
        fig.savefig(root / f"{name}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_f_f(root, cfg, data, log=True, s=("pdf", "png"), name="dist_f_f"):
    print("plotting functions distribution by function index")

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    data = data.reshape(data.shape[0], num_fn, num_eps)
    data = numpy.swapaxes(data, 1, 2)
    data = data.reshape(data.shape[0] * num_eps, num_fn)
    data = abs(data)

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data[data != 0]), bins="auto")

    c = (
        matplotlib.cm.tab20(range(num_fn))
        if num_fn > 10
        else matplotlib.cm.tab10(range(num_fn))
    )

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_xlabel(r"nonzero $|\mathrm{Re}\mathrm{Im}\,f^{(j)}_{i}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
        label=numpy.arange(1, 1 + num_fn),
        color=c,
    )

    ax.legend(reverse=True, title=r"$i$")

    for f in s:
        fig.savefig(root / f"{name}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_f_ps(
    root, x, f, xlabel, ylabel, q="mean", bins=40, name="None", suffix=("png", "pdf")
):
    print("plotting functions 2d histogram against ps")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    if q == "mean":
        w = abs(f).mean(axis=1)
    elif q == "sum":
        w = abs(f).sum(axis=1)
    elif q == "min":
        w = abs(f).min(axis=1)
    elif q == "max":
        w = abs(f).max(axis=1)

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        weights=w,
        bins=bins,
    )

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    fig.colorbar(im, label=rf"$\mathrm{{{q}}} \, \left( f_i \right)$")

    for f in suffix:
        fig.savefig(root / f"{name}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def get_a_data(cfg, CM, s, const, verbose=False):
    num_in = cfg["dimensions"]["input"]["variables"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    des_eps = cfg["dimensions"]["connection"]["epsilon_orders"]

    connection = CM(des_eps, num_in, num_fn, const)
    vals = connection.raw(s).view(des_eps, num_in, num_fn, num_fn, -1)

    if verbose:
        de = vals.abs()
        de = de[de != 0.0]
        std, mean = torch.std_mean(de)
        print(f"|DE|: mean={mean:.2f}, rel std={mean * std:.2f}")

    return vals.cpu().numpy()


def hist_a_cum(root, data, name=None, log=True, s=("pdf", "png")):
    print("plotting connection matrix elements cumulative distribution")

    data = abs(numpy.ravel(data))

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data[data != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(6.4, 4.8),
    )

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_xlabel("nonzero $|\mathrm{M}^{(j)}_{i;k,l}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
    )

    for f in s:
        fig.savefig(
            root / f"dist_a_cum{f'_{name}' if name is not None else ''}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()


def hist_a_eps(root, data, cfg, log=True, s=("pdf", "png")):
    print("plotting connection matrix elements distribution by eps order")

    ords = cfg["dimensions"]["connection"]["epsilon_orders"]

    data = abs(data.reshape(ords, -1).T)

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data[data != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(6.4, 4.8),
    )

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_xlabel("nonzero $|\mathrm{M}^{(j)}_{i;k,l}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
        label=[rf"$\mathcal{{O}}(\epsilon^{i})$" for i in range(ords)],
    )

    ax.legend(reverse=True)

    for ss in s:
        fig.savefig(root / f"dist_a_eps.{ss}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_a_2d(root, x, a, xlabel, ylabel, bins=40, name=None, s=("pdf", "png")):
    print("plotting max connection matrix elements PS distribution")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        weights=abs(a).max(axis=(0, 1, 2, 3)),
        bins=bins,
        norm="log",
    )

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    fig.colorbar(
        im,
        label=r"$\mathrm{max}\left( |A^{(k)}_{q,ij}| \right)$",
    )

    for f in s:
        fig.savefig(
            root / f"{'dist_a_ps' if name is None else name}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()


def hist_letters(
    root,
    x,
    letter_labels,
    name=None,
    s=("png", "pdf"),
    verbose=True,
    symbol="$\ell_{i}$",
):
    print("plotting letters distribution")

    x = abs(x)

    b = numpy.histogram_bin_edges(x[x != 0], bins="auto")

    n = x.shape[1]
    c = matplotlib.cm.tab20(range(n)) if n > 10 else matplotlib.cm.tab10(range(n))

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel(symbol)
    ax.set_ylabel("Proportion")

    ax.hist(
        x,
        bins=b,
        histtype="step",
        weights=numpy.full_like(x, 1 / len(x)),
        label=letter_labels,
        color=c,
        linestyle="solid",
    )

    ax.legend(title=symbol, reverse=True)

    for f in s:
        fig.savefig(
            root / f"{'dist_letters' if name is None else name}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()
