# module inference

import abc, pathlib, math
import torch
from finn.lib.model import FeynmanIntegralsModel
from finn.lib.filenames import MODEL


def load_model(location, device, v=True):
    model_file = location / MODEL

    if v:
        print(f"Loading {model_file}")

    data = torch.load(model_file, map_location=device)

    hidden_layers = data["hidden_layers"]
    activation = data["activation"]
    num_in = data["num_in"]
    num_eps = data["num_eps"]
    num_fn = data["num_fn"]
    dtype = data["model_state_dict"]["network.0.weight"].dtype

    model = FeynmanIntegralsModel(
        num_in, hidden_layers, activation, num_fn, num_eps, dtype, device
    )
    model.load_state_dict(data["model_state_dict"])
    model.eval()

    return model


class Inference(abc.ABC):
    def __init__(self, const):
        self.const = const

        self.re_v = None
        self.im_v = None

    def rescale(self, x):
        """
        transform user kinematics to scaled kinematics used for NN
        const must be in final position
        """
        return self.const * x[:, :-1] / x[:, [-1]]

    @abc.abstractmethod
    @torch.no_grad()
    def eval(self, x):
        """
        evaluate model over input batch x
        x.shape: (batch size, number inputs [including const])
        access results using: real(), imag(), pair(), value()
        """
        ...

    def real(self):
        """
        return real parts of batch values
        shape: (batch size, number functions, number epsilons)
        dtype: float
        """
        return self.re_v

    def imag(self):
        """
        return imaginary parts of batch values
        shape: (batch size, number functions, number epsilons)
        dtype: float
        """
        return self.im_v

    def pair(self):
        """
        return batch values as pairs of floats for complex numbers
        shape: (batch size, number functions, number epsilons, 2)
        dtype: float
        """
        return torch.stack((self.re_v, self.im_v), axis=3)

    def value(self):
        """
        return batch values as complex numbers
        shape: (batch size, number functions, number epsilons)
        dtype: complex
        """
        return torch.view_as_complex(self.pair())


class Single(Inference):
    def __init__(self, root, rep, rep_im=None, const=1.0, device="cpu", v=True):
        super().__init__(const)

        root = pathlib.Path(root)

        self.device = torch.device(device)

        self.re_m = load_model(root / "re" / rep, self.device, v)
        self.im_m = load_model(
            root / "im" / (rep if rep_im is None else rep_im), self.device, v
        )

        self.dtype = self.re_m.dtype

    @torch.no_grad()
    def eval(self, x):
        x = self.rescale(x)

        self.re_v = self.re_m.nested(x)
        self.im_v = self.im_m.nested(x)


class Ensemble(Inference):
    def __init__(
        self,
        root,
        reps,
        reps_im=None,
        const=1.0,
        device="cpu",
        re_z=[[]],
        im_z=[[]],
        v=True,
    ):
        """
        root: path to integral family directory
        reps: folder names of replicas
        const: value of kinematic constant
        device: torch computation device
        re_z/im_z: denote zero coefficients of solutions by lists of [function number, eps order]
        """
        super().__init__(const)

        root = pathlib.Path(root)

        self.device = torch.device(device)

        self.n = len(reps)

        self.re_m = [load_model(root / "re" / rep, self.device, v) for rep in reps]
        self.im_m = [
            load_model(root / "im" / rep, self.device, v)
            for rep in (reps if reps_im is None else reps_im)
        ]

        self.dtype = self.re_m[0].dtype

        self.re_z = torch.tensor(re_z, device=self.device).permute((1, 0))
        self.im_z = torch.tensor(im_z, device=self.device).permute((1, 0))

        self.re_e = None
        self.im_e = None

    @torch.no_grad()
    def part(self, x, models, zeros):
        dist = torch.stack([m.nested(x) for m in models])

        std, mean = torch.std_mean(dist, dim=0)

        mean[:, *zeros] = 0

        err = std / math.sqrt(self.n)

        return mean, err

    @torch.no_grad()
    def eval(self, x):
        """
        access error results using: error_abs(), error_rel()
        note that known zero coefficients are explicitly zeroed
        """
        x = self.rescale(x)

        self.re_v, self.re_e = self.part(x, self.re_m, self.re_z)
        self.im_v, self.im_e = self.part(x, self.im_m, self.im_z)

    def real_error_abs(self):
        """
        return absolute error estimates for real parts
        """
        return self.re_e

    def imag_error_abs(self):
        """
        return absolute error estimates for imaginary parts
        """
        return self.im_e

    def error_abs(self):
        """
        return absolute error estimates as pairs for real and imaginary parts
        """
        return torch.stack((self.re_e, self.im_e), axis=3)

    def real_error_rel(self):
        """
        return relative error estimates for real parts
        """
        err = self.re_e / abs(self.re_v)

        # set relative error to zero on zero coefficients
        err[:, *self.re_z] = 0

        return err

    def imag_error_rel(self):
        """
        return relative error estimates for imaginary parts
        """
        err = self.im_e / abs(self.im_v)

        err[:, *self.im_z] = 0

        return err

    def error_rel(self):
        """
        return relative error estimates as pairs for real and imaginary parts
        """
        return torch.stack((self.real_error_rel(), self.imag_error_rel()), axis=3)
