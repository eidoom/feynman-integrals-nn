# module testing

import argparse, pathlib

import matplotlib.pyplot, numpy, torch

from finn.lib.inference import load_model
from finn.lib.filenames import STATS


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "part",
        type=str,
        choices=["re", "im"],
        help="Choose Real (re) or Imaginary (im) part.",
    )
    parser.add_argument(
        "replica",
        type=str,
        help="Name of replica.",
    )
    parser.add_argument(
        "-o",
        "--override",
        type=str,
        metavar="O",
        nargs="+",
        help="Paths to configuration files whose values override the default configuration files.",
    )
    parser.add_argument(
        "-d",
        "--datasets",
        type=pathlib.Path,
        metavar="D",
        help="Path to separate datasets directory for integral family. This option can be used if the datasets are stored in a different location than the main integral family directory.",
    )
    parser.add_argument(
        "-e",
        "--everything",
        action="store_true",
        help="Do extra plots.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity.",
    )
    return parser.parse_args()


def time_unit(time):
    unit = "s"

    if time > 60:
        time /= 60
        unit = "min"

    if time > 60:
        time /= 60
        unit = "hr"

    return time, unit


def load_training_stats(rep):
    file = rep / STATS

    if file.is_file():
        data = numpy.load(file)

        return numpy.array(
            (
                data["epoch_end_times"],
                data["learning_rate_values"],
                data["training_loss_values"].mean(axis=1),
                data["training_loss_values"].min(axis=1),
                data["training_loss_values"].max(axis=1),
            )
        )

    return numpy.load(file.with_suffix(".npy"))


def plot_training_statistics(rep, p=0, v=True, ss=("png", "pdf")):
    print(f"Plotting training statistics")

    colours = matplotlib.cm.tab10(range(2))

    (
        epoch_end_times,
        learning_rate_values,
        mmean,
        mmin,
        mmax,
    ) = load_training_stats(rep)

    learning_rate_values *= 10**p

    if v:
        time, unit = time_unit(epoch_end_times[-1])

        print(
            f"Training time: {time:.1f} {unit}\n"
            f"Final training metric: {mmean[-1]:.2g}"
        )

    e = "" if not p else rf" $\times 10^{p}$"
    labels = ("Training loss", f"Learning rate{e}")

    epochs = numpy.arange(1, len(epoch_end_times) + 1)

    proc = numpy.stack((mmean, learning_rate_values))

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")

    ax.set_yscale("log")

    ax.fill_between(epochs, mmin, mmax, alpha=0.2, color=colours[0], edgecolor=None)

    ax.set_prop_cycle(color=colours)
    ax.plot(epochs, proc.T, label=labels)

    ax.set_xlim(left=0)

    ax.legend()

    for s in ss:
        fig.savefig(rep / f"training.{s}", bbox_inches="tight")

    matplotlib.pyplot.close()


def init_model(rep, cfg, v=True):
    device = torch.device(cfg["device"])
    return load_model(rep, device, v)


def get_testing_dataset(root, part, cfg, name="testing", verbose=True):
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    tif = root / f"{name}_input.csv"
    tof = part / f"{name}_output.csv"

    if verbose:
        print(f"Loading {tif}\nLoading {tof}")

    tid = numpy.loadtxt(tif, delimiter=",", ndmin=2)
    tod = numpy.loadtxt(tof, delimiter=",").reshape(-1, num_fn, num_eps)

    return tid, tod


def infer(model, x):
    with torch.no_grad():
        nn = model.nested(x)

    return nn.cpu().numpy()


def get_testing_data(root, part, cfg, model, verbose=True):
    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    tid, tod = get_testing_dataset(root, part, cfg, verbose=verbose)

    x = torch.tensor(tid, device=device, dtype=dtype)

    nn = infer(model, x)

    return tid.T, tod, nn


def get_abs_diff(ff, nn, v=1):
    ad = abs(ff - nn)

    if v:
        b = 10
        print(f"abs diff means:\n{'all':>{b}}: {ad.mean():.2g}")

    if v > 1:
        mean_by_weight = ad.mean(axis=(0, 1))

        for w, m in enumerate(mean_by_weight):
            print(f"{f'O(eps^{w})':>{b}}: {m:.2g}")

        print(f"{'worst O':>{b}}: {mean_by_weight.max():.2g}")

        mean_by_fn = ad.mean(axis=(0, 2))

        for i, m in enumerate(mean_by_fn, start=1):
            print(f"{f'f{i}':>{b}}: {m:.2g}")

        print(f"{'worst f':>{b}}: {mean_by_fn.max():.2g}")

    return ad


def get_rel_diff_flat(ff, nn, zero_out, chop, v=True):
    # first we remove known zeros
    zero_out = numpy.array(zero_out).T

    mask = numpy.full(ff.shape[1:], True)
    mask[*zero_out] = False

    ff = ff[:, mask]
    nn = nn[:, mask]

    # then we remove infintesimals in the testing dataset
    # these should be zero but aren't due to rounding errors
    # should really take care of this during data generation
    nonzero = abs(ff) > chop

    if v:
        outliers = ff[nonzero == False]
        print(f"{len(outliers)} outliers:\n{outliers}")

    ff = ff[nonzero]
    nn = nn[nonzero]

    rd = abs((ff - nn) / ff)

    if v:
        print(f"Mean rel diff: {rd.mean():.2g}")

    return rd


def get_rel_diff(ff, nn, v=1, chop=False):
    # delete function eps-coeffs that we know are zero
    nonzero = ff[0] != 0.0
    ffnz = ff[:, nonzero]

    # delete ps points that have zero function eps-coeffs
    i = (
        numpy.argwhere(
            (abs(ffnz) <= numpy.finfo(numpy.float32).eps) if chop else (ffnz == 0)
        )
    )[:, 0]
    ffnz = numpy.delete(ffnz, i, axis=0)

    # take same subset from NN dataset
    nnnz = numpy.delete(nn[:, nonzero], i, axis=0)

    rd = abs((ffnz - nnnz) / ffnz)

    if v == 1:
        print(f"Mean rel diff: {rd.mean():.2g}")
    elif v == 2:
        chopn = 1e-4
        chopped = rd[abs(nnnz) > chopn]
        amount = chopped.size / nnnz.size
        b = 10
        print(
            f"rel diff means:\n"
            f"{'all':>{b}}: {rd.mean():.2g}\n"
            f"{'chopped':>{b}}: {chopped.mean():.2g}\n"
            f"(chop @ |NN| > {chopn}, {amount:4.1%} remaining)"
        )

    return rd, i, nonzero


def get_colours(cfg):
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    n = num_eps * num_fn
    assert n <= 20

    return matplotlib.cm.tab20(range(n))


def get_labels(cfg, part, eps_start=0):
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    num_fn = cfg["dimensions"]["output"]["functions"]

    return numpy.array(
        [
            f"$\mathrm{{{part.capitalize()}}}\,f_{{{i}}}^{{({j})}}$"
            for i in range(1, num_fn + 1)
            for j in range(eps_start, eps_start + num_eps)
        ]
    )


def plot_abs_diff_ps(
    rep, ad, x, xlabel, ylabel, bins=None, norm="linear", s=("png", "pdf")
):
    print("Plotting PS absolute difference")

    adf = ad.mean(axis=(1, 2))

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        bins=len(adf) if bins is None else bins,
        norm=norm,
        weights=adf,
    )

    fig.colorbar(im, label=r"$\frac{1}{n} \sum |f(x)-\mathrm{NN}(x)|$")

    for f in s:
        fig.savefig(rep / f"abs_diff_ps.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_rel_diff_ps(
    rep, rd, x, xlabel, ylabel, bins=None, norm="linear", s=("png", "pdf"), v=True
):
    if v:
        print("Plotting PS relative difference")

    rd = rd.reshape(rd.shape[0], -1)
    arf = rd.mean(axis=1)

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        bins=len(arf) if bins is None else bins,
        norm=norm,
        weights=arf,
    )

    fig.colorbar(im, label=r"$\frac{1}{n} \sum |(f-\mathrm{NN})/f|$")

    for f in s:
        fig.savefig(rep / f"rel_diff_ps.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_abs_diff_eps(rep, cfg, ad, ff, s=("png", "pdf")):
    print("Plotting absolute difference by eps order")

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    labels = [rf"$\mathcal{{O}}(\epsilon^{i})$" for i in range(num_eps)]

    ad = ad.reshape(ad.shape[0] * num_fn, num_eps)

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(ad[ad != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|f-NN|$")

    ax.hist(
        ad,
        bins=b,
        histtype="step",
        weights=numpy.full_like(ad, 1.0 / len(ad)),
        label=labels,
    )

    ax.legend(reverse=True)

    for f in s:
        fig.savefig(rep / f"abs_diff_eps.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_abs_diff_cum(rep, ad, s=("png", "pdf"), v=True):
    if v:
        print("Plotting cumulative absolute difference")

    adf = numpy.ravel(ad)

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(adf[adf != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|f-NN|$")

    ax.hist(
        adf,
        bins=b,
        histtype="step",
        weights=numpy.full_like(adf, 1.0 / len(adf)),
    )

    for f in s:
        fig.savefig(rep / f"abs_diff_cum.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_rel_diff_cum(rep, rd, s=("png", "pdf"), log=False, v=True):
    if v:
        print("Plotting cumulative relative difference")

    frd = numpy.ravel(rd)

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(frd[frd != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")
    if log:
        ax.set_yscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|(f-NN)/f|$")

    ax.hist(
        frd,
        bins=b,
        histtype="step",
        weights=numpy.full_like(frd, 1.0 / len(frd)),
    )

    for f in s:
        fig.savefig(rep / f"rel_diff_cum.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_abs_diff_each(rep, ad, labels, colours, lcols, s=("png", "pdf")):
    print("Plotting absolute difference by output")

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(ad[ad != 0]), bins="auto")

    ad = ad.reshape(ad.shape[0], -1)

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|f-NN|$")

    ax.hist(
        ad,
        bins=b,
        histtype="step",
        weights=numpy.full_like(ad, 1.0 / len(ad)),
        label=labels,
        color=colours,
    )

    ax.legend(
        title="Functions",
        reverse=True,
        ncols=lcols,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
    )

    for f in s:
        fig.savefig(rep / f"abs_diff_each.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_rel_diff_each(rep, rd, labels, colours, nonzero, lcols, s=("png", "pdf")):
    print("Plotting relative difference by output")

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(rd[rd != 0]), bins="auto")

    rd = rd.reshape(rd.shape[0], -1)

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|(f-NN)/f|$")

    ax.hist(
        rd,
        bins=b,
        histtype="step",
        weights=numpy.full_like(rd, 1.0 / len(rd)),
        label=labels,
        color=colours,
    )

    ax.legend(
        title="Nonzero functions",
        reverse=True,
        ncols=lcols,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
    )

    for f in s:
        fig.savefig(rep / f"rel_diff_each.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_rel_diff_i(rep, cfg, rd, labels, colours, i, s=("png", "pdf")):
    print(f"Plotting nonzero function {i} relative difference")

    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    n = num_eps * num_fn

    rd = rd[:, i]

    b = 10 ** numpy.histogram_bin_edges(numpy.log10(rd[rd != 0]), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(rf"$|(f-NN)/f|$ with $f=${labels[i]}")

    ax.hist(
        rd,
        bins=b,
        histtype="step",
        weights=numpy.full_like(rd, 1.0 / len(rd)),
        color=colours[i],
    )

    for f in s:
        fig.savefig(rep / f"rel_diff_{i}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()
