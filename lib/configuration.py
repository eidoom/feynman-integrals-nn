# module configuration

import pathlib, json


def merge_nested_dicts(a, b):
    c = a | b
    for ak, av in a.items():
        for bk, bv in b.items():
            if ak == bk and isinstance(av, dict):
                c[ak] = merge_nested_dicts(av, bv)
    return c


def get_cfg(root, part=None, override=None, verbose=True, conf_file="config.json"):
    root = pathlib.Path(root)
    root_file = root / conf_file

    if verbose:
        print(f"Reading {root_file}")
    with open(root_file, "r") as f:
        cfg = json.load(f)

    if part is not None:
        part = pathlib.Path(part)
        part_file = part / conf_file
        if part_file.is_file():
            if verbose:
                print(f"Reading {part_file}")
            with open(part_file, "r") as f:
                pcfg = json.load(f)
            cfg = merge_nested_dicts(cfg, pcfg)

    if override is not None:
        for extra_file in override:
            if verbose:
                print(f"Reading {extra_file}")
            with open(extra_file, "r") as f:
                ocfg = json.load(f)
            cfg = merge_nested_dicts(cfg, ocfg)

    return cfg


def ps_params(cfg):
    ps_cfg = cfg["phase_space"]
    scale = ps_cfg["scale"]
    const = ps_cfg["const"]
    pcut = scale * ps_cfg["cut"]["physical"]
    scut = scale * ps_cfg["cut"]["spurious"]
    return scale, const, pcut, scut
