# module model

import torch


class FeynmanIntegralsModel(torch.nn.Module):
    def __init__(
        self,
        num_in,
        hidden_layers,
        activation,
        num_fn,
        num_eps,
        dtype,
        device,
        bvy=None,
    ):
        super().__init__()

        self.num_in = num_in
        self.num_fn = num_fn
        self.num_eps = num_eps
        self.output_size = num_fn * num_eps
        layer_sizes = [num_in] + hidden_layers

        self.dtype = dtype
        self.device = device

        self.network = torch.nn.Sequential(
            *[
                f(x)
                for x in zip(layer_sizes[:-1], layer_sizes[1:])
                for f in (
                    lambda x: torch.nn.Linear(*x, dtype=dtype, device=device),
                    lambda _: getattr(torch.nn, activation)(),
                )
            ],
            torch.nn.Linear(
                layer_sizes[-1], self.output_size, dtype=dtype, device=device
            ),
        )

        if activation == "GELU":
            gain = 1
        elif activation == "Tanh":
            gain = torch.nn.init.calculate_gain("tanh")

        for layer in self.network[::2]:
            torch.nn.init.xavier_uniform_(layer.weight, gain)

        if bvy is not None:
            self.network[-1].bias.data = torch.mean(bvy, dim=0)

    def forward(self, x):
        return self.network(x)

    def nested(self, x):
        return self.forward(x).view(len(x), self.num_fn, self.num_eps)
