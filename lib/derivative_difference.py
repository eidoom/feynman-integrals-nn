#!/usr/bin/env python3

import importlib

import torch

from finn.lib.configuration import get_cfg, ps_params
from finn.lib.inference import load_model
from finn.lib.loss import DifferentialEquationsLoss
from finn.lib.model import FeynmanIntegralsModel


class DerivativeDifference:
    def __init__(self, folder, name, device, v=True):
        self.device = device
        self.v = v

        mod_ps = importlib.import_module(f"finn.{name}.phase_space")

        mod_de = importlib.import_module(f"finn.{name}.connection_matrix")

        cfg = get_cfg(folder, verbose=self.v)

        scale, const, cut_phys, cut_spur = ps_params(cfg)

        dtype = torch.float32

        self.ps = mod_ps.PhaseSpace(
            scale, const, cut_phys, cut_spur, dtype, self.device
        )

        self.des_eps = cfg["dimensions"]["connection"]["epsilon_orders"]
        self.num_in = cfg["dimensions"]["input"]["variables"]
        self.num_fn = cfg["dimensions"]["output"]["functions"]

        self.de = mod_de.ConnectionMatrix(self.des_eps, self.num_in, self.num_fn, const)

        num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
        hidden_layers = cfg["hyperparameters"]["architecture"]["hidden_layers"]
        activation = cfg["hyperparameters"]["architecture"]["activation"]

        raw_model = FeynmanIntegralsModel(
            self.num_in,
            hidden_layers,
            activation,
            self.num_fn,
            num_eps,
            dtype,
            self.device,
        )

        self.loss = DifferentialEquationsLoss([], [], self.des_eps, raw_model)

    def get_des(self, x):
        return self.de(x).view(
            self.des_eps, x.shape[1] * self.num_in, self.num_fn, self.num_fn
        )

    def get_diff(self, x, des, f):
        dnn = self.loss.d_auto(f, x, training=False).detach()
        with torch.no_grad():
            conn = self.loss.d_sys(des, f)
            return abs(dnn - conn)

    def __call__(self, model_names, partition_size, number_partitions=1, mean=False):
        models = [load_model(name, self.device, v=self.v) for name in model_names]

        diffs = torch.empty(
            (len(models), number_partitions)
            if mean
            else (
                len(models),
                number_partitions,
                models[0].num_eps,
                partition_size,
                models[0].num_in,
                models[0].num_fn,
            )
        )

        for i in range(number_partitions):
            if self.v:
                print(f"partition {i:>2}/{number_partitions}")

            x = self.ps(partition_size, cut_spurious=True)
            des = self.get_des(x)
            x = x.T
            x.requires_grad_()
            for j, model in enumerate(models):
                f = model(x)
                diff = self.get_diff(x, des, f)
                del f
                diffs[j, i] = diff.mean() if mean else diff
                del diff

            del x
            del des

        return diffs.mean(dim=1) if mean else diffs
