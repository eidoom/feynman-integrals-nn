# model phase_space

import abc, time
import torch


class PhaseSpaceBase(abc.ABC):
    def __init__(self, scale, const, cut_phys, cut_spur, dtype, device):
        self.dtype = getattr(torch, dtype) if isinstance(dtype, str) else dtype
        self.device = torch.device(device)

        self.scale = torch.tensor(scale, dtype=self.dtype, device=self.device)
        self.const = torch.tensor(const, dtype=self.dtype, device=self.device)
        self.cut_phys = torch.tensor(cut_phys, dtype=self.dtype, device=self.device)
        self.cut_spur = torch.tensor(cut_spur, dtype=self.dtype, device=self.device)

    def __call__(self, n, cut_spurious=False):
        s = self.points(n)

        singular = self.test_singular(s, cut_spurious)

        while singular.any():
            num = singular.sum()

            ss = self.points(num)

            s[:, singular] = ss

            singular[singular.clone()] = self.test_singular(ss, cut_spurious)

        return s

    def test(self, n, cut_spurious=False):
        print(
            f"Generating PS with {n} points{' and spurious cuts' if cut_spurious else ''}"
        )
        t = time.time()

        s = self.points(n)
        c = n

        singular = self.test_singular(s, cut_spurious)

        p = -n
        while singular.any():
            num = singular.sum()

            d = n - num
            if d > p + n / 10:
                p = d
                a = d / n
                print(f"Progress: {a:4.1%}")

            ss = self.points(num)
            c += num

            s[:, singular] = ss

            singular[singular.clone()] = self.test_singular(ss, cut_spurious)

        e = n / c
        u = time.time() - t
        print(f"Complete\nTime: {u:.1f}s\nEfficiency: {e:4.1%}")

        return s

    def uniform(self, l, r, n):
        return l + (r - l) * torch.rand(n, dtype=self.dtype, device=self.device)

    def test_singular(self, s, cut_spurious):
        singular = (self.a_phys(s) < self.cut_phys).any(axis=0)

        if cut_spurious:
            singular |= (self.a_spur(s) < self.cut_spur).any(axis=0)

        return singular

    @abc.abstractmethod
    def a_phys(self, s):
        ...

    @abc.abstractmethod
    def a_spur(self, s):
        ...

    @abc.abstractmethod
    def points(self, n):
        ...
