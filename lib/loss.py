# module loss

import torch


class DifferentialEquationsLoss(torch.nn.Module):
    def __init__(
        self, bvx, bvy, des_eps, model, bvde=None, boundary_bias=1.0, output_bias=None
    ):
        super().__init__()

        self.bvx = bvx
        self.bvy = bvy

        self.boundary_bias = boundary_bias

        self.model = model

        self.des_eps = des_eps

        self.num_in = model.num_in
        self.num_fn = model.num_fn
        self.num_eps = model.num_eps
        self.output_size = model.output_size

        self.loss = torch.nn.MSELoss()

        self.device = model.device
        self.dtype = model.dtype

        self.bias_outputs = output_bias is not None

        if self.bias_outputs:
            self.output_bias = (
                torch.tensor(output_bias, dtype=self.dtype, device=self.device)
                # ^ shape = (num_fn,)
                .repeat(self.num_eps)
                # ^ shape = (num_eps * num_fn,)
                .view(self.num_eps, 1, 1, self.num_fn)
            )
            # ^ dims = (num_eps, batch_size, num_in, num_fn)

        self.boundary_derivatives = bvde is not None

        if self.boundary_derivatives:
            self.bvdy = self.d_sys(
                bvde.view(des_eps, len(bvx) * self.num_in, self.num_fn, self.num_fn),
                bvy,
            )

    def d_auto(self, outputs, inputs, training=True):
        """
        returns the model derivative using autograd

        inputs.shape = (batch_size, num_in)
        outputs.shape = (batch_size, num_fn * num_eps)
        """
        batch_size = len(inputs)

        return (
            torch.stack(
                [
                    torch.autograd.grad(
                        outputs=outputs[:, i],
                        inputs=inputs,
                        grad_outputs=torch.ones(
                            batch_size, device=self.device, dtype=self.dtype
                        ),
                        retain_graph=training or i != self.output_size - 1,
                        create_graph=training,
                    )[0]
                    for i in range(self.output_size)
                ]
            ).view(
                self.num_fn,
                self.num_eps,
                batch_size,
                self.num_in,
            )
            # ^ shape = (num_fn, num_eps, batch_size, num_in)
            .permute((1, 2, 3, 0))
            # ^ shape = (num_eps, batch_size, num_in, num_fn)
        )

    def d_sys(self, coeff_mats, outputs):
        """
        returns the model derivative using the system of differential equations

        coeff_mats.shape = (des_eps, batch_size * num_in, num_fn, num_fn)
        outputs.shape = (batch_size, num_fn * num_eps)
        """
        batch_size = len(outputs)

        outputs = (
            outputs
            # ^ shape = (batch_size, num_fn * num_eps)
            .view(batch_size, self.num_fn, self.num_eps)
            # ^ shape = (batch_size, num_fn, num_eps)
            .permute((2, 0, 1))
            # ^ shape = (num_eps, batch_size, num_fn)
            .repeat((1, 1, self.num_in))
            # ^ shape = (num_eps, batch_size, num_in * num_fn)
            .view(
                self.num_eps,
                batch_size * self.num_in,
                self.num_fn,
                1,
            )
            # ^ shape = (num_eps, batch_size * num_in, num_fn, 1)
        )

        # to broadcast over dimension 1
        target = torch.empty(
            (self.num_eps, batch_size * self.num_in, self.num_fn, 1),
            device=self.device,
            dtype=self.dtype,
        )
        # ^ shape = (num_eps, batch_size * num_in, num_fn, 1)

        for w in range(self.num_eps):
            target[w] = sum(
                torch.matmul(coeff_mats[k], outputs[w - k])
                for k in range(min(w + 1, self.des_eps))
            )

        return target.view(self.num_eps, batch_size, self.num_in, self.num_fn)
        # ^ shape = (num_eps, batch_size, num_in, num_fn)

    def forward(self, x, coeff_mats, training=True):
        pred = self.model(x)

        dnn = self.d_auto(pred, x, training)
        target = self.d_sys(coeff_mats, pred)

        if self.bias_outputs:
            dnn = self.output_bias * dnn
            target = self.output_bias * target

        bulk = self.loss(dnn, target)

        bv = self.model(self.bvx)
        boundary = self.loss(bv, self.bvy)

        if self.boundary_derivatives:
            dbv = self.d_auto(bv, self.bvx, training)
            boundary += self.loss(dbv, self.bvdy)

        return bulk + self.boundary_bias * boundary
