#!/usr/bin/env python3

import time, argparse, pathlib, math, importlib, random
import torch, numpy
from finn.lib.configuration import get_cfg, ps_params
from finn.lib.model import FeynmanIntegralsModel
from finn.lib.loss import DifferentialEquationsLoss
from finn.lib.filenames import STATS, MODEL, CHECK


def get_args():
    parser = argparse.ArgumentParser(
        description="Learn the real or imaginary parts of an integral family."
    )
    parser.add_argument(
        "directory",
        type=pathlib.Path,
        help="Path to integral family directory.",
    )
    parser.add_argument(
        "part",
        type=str,
        choices=["re", "im"],
        help="Choose Real (re) or Imaginary (im) part.",
    )
    parser.add_argument(
        "replica",
        type=str,
        help="Name of replica.",
    )
    parser.add_argument(
        "-d",
        "--datasets",
        type=pathlib.Path,
        metavar="D",
        help="""
Path to separate datasets directory for integral family. This option can be used if the datasets are stored in a different location than the main integral family directory. Only the boundary dataset is loaded; any configuration files in the datasets directory will be ignored unless specified by --override.
""",
    )
    parser.add_argument(
        "-o",
        "--override",
        type=str,
        metavar="O",
        nargs="+",
        help="Paths to configuration files whose values override the default configuration files.",
    )
    parser.add_argument(
        "-i",
        "--interval",
        type=float,
        metavar="I",
        default=10.0,
        help="Show the current training metric every I percent each epoch (default: 10).",
    )
    parser.add_argument(
        "-j",
        "--threads",
        type=int,
        metavar="J",
        help="Sets the number of threads used for intraop parallelism on CPU.",
    )
    parser.add_argument(
        "-c",
        "--use-cache",
        action="store_true",
        help="Load boundary data caches.",
    )
    parser.add_argument(
        "-r",
        "--resume",
        action="store_true",
        help="Resume training from checkpoint.",
    )
    args = parser.parse_args()
    if args.datasets is None:
        args.datasets = args.directory
    return args


def load_cached(filename, device, dtype, use_cache, verbose=True):
    """
    This is used to cache boundary datasets.
    It is not really necessary and exists for historical purposes.
    I'll leave it in for now.
    """

    filename = pathlib.Path(filename)

    cache = filename.with_suffix(".pt")

    if use_cache and cache.is_file():
        if verbose:
            print(f"Loading {cache}")
        return torch.load(cache, map_location=device)

    if verbose:
        print(f"Loading {filename}")
    data = numpy.loadtxt(filename, delimiter=",", ndmin=2)

    data = torch.tensor(data, device=device, dtype=dtype)

    torch.save(data, cache)

    return data


def iteration(model, x, loss_fn, de_vals, optimiser):
    optimiser.zero_grad()
    x.requires_grad_()

    loss = loss_fn(x, de_vals)

    loss.backward()
    optimiser.step()

    return loss


if __name__ == "__main__":
    print("Initialising")
    start = time.time()

    args = get_args()

    print(f"Learning {args.directory} {args.part} (replica {args.replica})")

    if args.threads is not None:
        torch.set_num_threads(args.threads)

    root = args.directory
    dataroot = args.datasets

    part = root / args.part
    part.mkdir(exist_ok=True)
    datapart = dataroot / args.part

    mod_ps = importlib.import_module(f"finn.{root}.phase_space")

    mod_de = importlib.import_module(f"finn.{root}.connection_matrix")

    cfg = get_cfg(root, part, args.override)

    num_in = cfg["dimensions"]["input"]["variables"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    des_eps = cfg["dimensions"]["connection"]["epsilon_orders"]

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    scale, const, cut_phys, cut_spur = ps_params(cfg)

    ps = mod_ps.PhaseSpace(scale, const, cut_phys, cut_spur, dtype, device)

    de = mod_de.ConnectionMatrix(des_eps, num_in, num_fn, const)

    hidden_layers = cfg["hyperparameters"]["architecture"]["hidden_layers"]
    activation = cfg["hyperparameters"]["architecture"]["activation"]
    assert activation in ("GELU", "Tanh")

    algorithm = cfg["hyperparameters"]["optimiser"]["algorithm"]
    assert algorithm in ("Adam", "RAdam")
    learning_rate = cfg["hyperparameters"]["optimiser"]["initial_learning_rate"]

    lr_params = cfg["hyperparameters"]["learning_rate_scheduler"]

    batch_size = cfg["hyperparameters"]["iterations"]["batch_size"]
    partition_size = cfg["hyperparameters"]["iterations"]["partition_size"]
    partitions = cfg["hyperparameters"]["iterations"]["number_partitions"]

    boundary_bias = cfg["hyperparameters"]["loss_function"]["boundary_bias"]
    boundary_derivatives = cfg["hyperparameters"]["loss_function"][
        "boundary_derivatives"
    ]

    max_epochs = cfg["termination"]["epochs"]
    max_seconds = cfg["termination"]["seconds"]
    target = cfg["termination"]["learning_rate"]["target"]
    target_patience = cfg["termination"]["learning_rate"]["patience"]

    bvx = load_cached(dataroot / "boundary_input.csv", device, dtype, args.use_cache)
    if bvx.size(1) != num_in:
        raise Exception(
            f"Number of kinematic variables in each boundary dataset x point {bvx.size(1)} doesn't match config number of function inputs {num_in}"
        )

    boundary_size = len(bvx)

    bvy = load_cached(datapart / "boundary_output.csv", device, dtype, args.use_cache)
    if bvy.shape[0] != boundary_size:
        raise Exception(
            f"Number of boundary dataset y values {bvy.shape[0]} doesn't match number of boundary dataset x points {boundary_size}"
        )
    if bvy.shape[1] % num_fn:
        raise Exception(
            f"Number of coefficients in each boundary dataset y value {bvy.shape[1]} doesn't divide by config number of basis functions {num_fn}"
        )
    bvy_eps = bvy.shape[1] // num_fn
    if bvy_eps < num_eps:
        raise Exception(
            f"Number of epilson orders for each boundary dataset y value {bvy_eps} doesn't match config number of epsilon orders for basis functions {num_eps}"
        )
    if bvy_eps > num_eps:
        bvy = bvy.view(boundary_size, num_fn, bvy_eps)[:, :, :num_eps].reshape(
            boundary_size, num_fn * num_eps
        )

    bvde = de(bvx.T) if boundary_derivatives else None

    bvx.requires_grad_()

    time_limit = max_seconds > 0
    epoch_limit = max_epochs > 0

    samples_per_partition = partition_size * batch_size
    iterations = partitions * partition_size
    samples_per_epoch = iterations * batch_size

    pl = lambda x: "s" if x > 1 else ""

    print(
        f"Using data type {dtype}\n"
        f"Using device {device}{f' with {torch.get_num_threads()} threads' if cfg['device'] == 'cpu' else ''}\n"
        f"Solving for {num_fn} function{pl(num_fn)} of {num_in} variable"
        f"{pl(num_in)} with {num_eps} order{pl(num_eps)} in epsilon\n"
        f"Sampling phase space with\n"
        f" scale = {scale}, const = {const}, physical cut = {cut_phys}, spurious cut = {cut_spur}"
    )
    if boundary_derivatives:
        print("Including boundary derivative term in loss function")
    if time_limit:
        print(f"Training time limit = {max_seconds}s")
    if epoch_limit:
        print(f"Training epochs limit = {max_epochs}")
    if target > 0:
        print(
            f"Training target = {target} in learning rate with patience {target_patience} epochs"
        )
    print(
        f"Using {boundary_size} boundary value{pl(boundary_size)}\n"
        f"Training with {iterations} batches of {batch_size} samples"
        f" over {partitions} partition{pl(partitions)}\n"
        f" = {samples_per_epoch} samples per epoch\n"
        f"Model architecture: {num_in}-{'-'.join(map(str, hidden_layers))}-{num_eps * num_fn}\n"
        f"Loss function boundary bias = {boundary_bias}\n"
        f"Using {algorithm} optimiser\n"
        f"Initial learning rate = {learning_rate}\n"
        f"Using ReduceLROnPlateau scheduler with\n"
        f" {', '.join(f'{key} = {val}' for key, val in lr_params.items())}\n"
        f"Setting up training"
    )

    model = FeynmanIntegralsModel(
        num_in, hidden_layers, activation, num_fn, num_eps, dtype, device, bvy
    )
    model.train()

    loss_fn = DifferentialEquationsLoss(bvx, bvy, des_eps, model, bvde, boundary_bias)

    optimiser = getattr(torch.optim, algorithm)(
        params=model.parameters(),
        lr=learning_rate,
        **({"fused": device == torch.device("cuda")} if algorithm == "Adam" else {}),
    )

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimiser, **lr_params)

    rep = part / args.replica
    rep.mkdir(exist_ok=True)

    checkpoint_file = rep / CHECK
    model_file = rep / MODEL

    if args.resume:
        print(f"Loading checkpoint {checkpoint_file}")

        checkpoint = torch.load(checkpoint_file, map_location=device)

        model.load_state_dict(checkpoint["model_state_dict"])
        optimiser.load_state_dict(checkpoint["optimiser_state_dict"])
        scheduler.load_state_dict(checkpoint["scheduler_state_dict"])
        epoch = checkpoint["epoch"]

    else:
        if (checkpoint_file.is_file() or model_file.is_file()) and input(
            "Overwrite existing checkpoint/model files? (y/n) "
        ) != "y":
            exit()

        epoch = 0

    training_stats_file = rep / STATS

    learning_rate_values = []
    training_loss_values = []
    epoch_end_times = []

    dur = time.time() - start
    print(f"Initialisation took {dur:.1f}s")

    div = "-" * 30
    print(f"Training\n{div}")
    start = time.time()

    counter = 0

    try:
        while not epoch_limit or epoch < max_epochs:
            epoch_start = time.time()

            lr = optimiser.param_groups[0]["lr"]

            print(
                f"Epoch {epoch + 1}{f'/{max_epochs}' if epoch_limit else ''} (lr: {lr:.2g})"
            )

            loss_epoch = []

            for i in range(partitions):
                x_partition = ps(samples_per_partition, cut_spurious=True)
                de_partition = de(x_partition)

                x_batches = torch.split(x_partition.T, batch_size)
                de_batches = torch.split(de_partition, batch_size, dim=1)

                for j, (x_batch, de_batch) in enumerate(zip(x_batches, de_batches)):
                    de_batch = de_batch.view(
                        des_eps, batch_size * num_in, num_fn, num_fn
                    )

                    loss_train = iteration(model, x_batch, loss_fn, de_batch, optimiser)

                    loss_epoch.append(loss_train.detach().cpu())

                    # print progress through iterations of epoch in I% intervals
                    it = i * partition_size + j + 1
                    if math.fmod(it, iterations / 100 * args.interval) < 1:
                        print(
                            f"{it/iterations:3.0%}: {sum(loss_epoch)/max(1,it):.3g} ({loss_train:.3g})"
                        )

            loss_epoch = numpy.array(loss_epoch)
            loss_epoch_mean = loss_epoch.mean()

            print(
                f"mean training loss: {loss_epoch_mean:.3g}\n"
                f"        epoch took: {time.time() - epoch_start:.2f}s\n"
                f"{div}"
            )

            learning_rate_values.append(lr)
            training_loss_values.append(loss_epoch)
            epoch_end_times.append(time.time() - start)

            if lr <= target:
                # nb might be better to reduce scheduler eps/min_lr a step and use its
                # thresholded patience rather than this independent static patience check
                counter += 1
                if counter >= target_patience:
                    print("Learning rate has met target.\nStopping training.")
                    break
            else:
                counter = 0

            if time_limit and (time.time() - start) > max_seconds:
                print("Time limit reached.\nStopping training.")
                break

            scheduler.step(loss_epoch_mean)

            epoch += 1

    except KeyboardInterrupt:
        print("Manual stop")

        if not epoch or (args.resume and checkpoint["epoch"] == epoch):
            exit("Quitting")

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    print(f"Saving checkpoint {checkpoint_file}")
    torch.save(
        {
            "epoch": epoch,
            "model_state_dict": model.state_dict(),
            "optimiser_state_dict": optimiser.state_dict(),
            "scheduler_state_dict": scheduler.state_dict(),
        },
        checkpoint_file,
    )

    print(f"Saving model {model_file}")
    torch.save(
        {
            "num_in": num_in,
            "hidden_layers": hidden_layers,
            "activation": activation,
            "num_fn": num_fn,
            "num_eps": num_eps,
            "model_state_dict": model.state_dict(),
        },
        model_file,
    )

    print(f"Saving training statistics {training_stats_file}")
    training_statistics = {
        "epoch_end_times": numpy.array(epoch_end_times),
        "learning_rate_values": numpy.array(learning_rate_values),
        "training_loss_values": numpy.array(training_loss_values),
    }
    if args.resume:
        previous_stats = numpy.load(training_stats_file)
        start_time = previous_stats["epoch_end_times"][-1]
        training_statistics["epoch_end_times"] += start_time
        for key in ("epoch_end_times", "learning_rate_values"):
            training_statistics[key] = numpy.concatenate(
                (previous_stats[key], training_statistics[key])
            )
        training_statistics["training_loss_values"] = numpy.vstack(
            (
                previous_stats["training_loss_values"],
                training_statistics["training_loss_values"],
            )
        )
    numpy.savez_compressed(training_stats_file, **training_statistics)
