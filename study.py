#!/usr/bin/env python3

import argparse, pathlib

import numpy

# from finn.lib.configuration import get_cfg
from finn.lib.study_data import hist_f_cum


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "models",
        type=pathlib.Path,
        help="Path to integral family models directory.",
    )
    parser.add_argument(
        "datasets",
        type=pathlib.Path,
        nargs="?",
        help="Path to integral family datasets directory. Only necessary if different from models directory",
    )
    parser.add_argument(
        "part",
        type=str,
        choices=["re", "im"],
        help="Choose Real (re) or Imaginary (im) part.",
    )
    args = parser.parse_args()
    args.datasets = args.models if args.datasets is None else args.datasets
    return args


if __name__ == "__main__":
    args = get_args()

    # root = args.models

    # cfg = get_cfg(root, verbose=False)

    datadir = args.datasets
    datapart = datadir / args.part

    data_fn = numpy.loadtxt(datapart / "testing_output.csv", delimiter=",")

    hist_f_cum(datapart, data_fn, s=("pdf",), v=False)
