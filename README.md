# [Feynman integral neural networks](https://gitlab.com/feynman-integrals-nn/feynman-integrals-nn)

Physics-informed deep learning for Feynman integrals.

We present this work on [arXiv](https://arxiv.org/abs/2312.02067) (with [v1.0.0](https://gitlab.com/feynman-integrals-nn/feynman-integrals-nn/-/tags/v1.0.0) archived on [Zenodo](https://zenodo.org/records/10246280)).

## Initialisation

This project requires Python 3.11.
Python dependencies are managed with [Pipenv](https://pipenv.pypa.io/en/latest/).
It can be installed on a system with Python and Pip by

```shell
pip install --user pipenv
```

Install Python dependencies for the project in a virtual environment by running in the root directory of the project

```shell
pipenv install
```

Activate the Pipenv shell with

```shell
pipenv shell
```

It is deactivated by `exit`.

## Datasets and models

Example pre-generated datasets and pre-trained models are available on [Hugging Face](https://huggingface.co/feynman-integrals-nn).
All path command line arguments for scripts may point to directories outside this repository to facilitate the use of these datasets and models when they are cloned to a separate location; see the script `--help` for details.

## Usage

All filenames in the following are relative to the root of the project.
Options for all Python scripts are described by a help message, accessed with `-h` or `--help`.

### Training

Training is performed using the script `train.py`.
To support use for any integral family, real or imaginary part, or replica rerun, it takes three positional arguments which specify these in that order.
For example,

```shell
./train.py box re 0
```

will train the real parts of the box integral family, putting the results in the directory `box/re/0/`.
Further information including details of optional arguments available from `./train.py --help`.

While training, `train.py` will report some statistics to the terminal.
First, the training parameters are printed.
Each epoch, the epoch number and learning rate are shown, followed by the training metrics (the cumulative mean value of the loss function since epoch start and, in brackets, the instantaneous value of the loss function for the current batch) for a even-intervaled subset of the iterations, then finally the epoch training metric (mean value of the loss function over the epoch) and time.

The `train.py` script relies on some files for each integral family in the integral family directory `<dir>`:

* `<dir>/connection_matrix.py` defines the connection matrix for the system of differential equations.
    It must contain a class `ConnectionMatrix` derived from `ConnectionMatrixBase` in `lib/connection_matrix.py`, with the abstract method defined.
    This a method of name and signature:
    ```python
    raw(self, s: torch.Tensor) -> torch.Tensor
    ```
    where:
    * `s` is a tensor of batched kinematic variables of shape `(number_inputs, batch_size)`,
    * the method returns a batched tensor of the flattened connection matrix of shape `(matrix_size, batch_size)` with the matrix in the order before flattening `(epsilon orders, inputs, outputs, outputs)`.
* `<dir>/phase_space.py` defines the phase space sampling algorithm used in training.
    It must contain a class derived from the base class in `lib/phase_space.py` with the abstract methods defined.
    These are:
    * a method returning `n` sample points as a `Tensor` of shape `(number_inputs, n)`,
        ```python
        points(self, n: int) -> torch.Tensor
        ```
    * and a method which tests whether those points `s` are singular, returning a `Tensor` of shape `(n,)` with boolean entries (`True` for singular),
        ```python
        points(self, s: torch.Tensor, cut_spurious: bool) -> torch.Tensor
        ```
        Physical cuts are always checked, while spurious cuts are optionally checked if `cut_spurious` is set to `True`.

Training is configured for a part `<part>` (real `re` or imaginary `im`) of an integral family by the JSON files:

* `<dir>/config.json`,
* `<dir>/<part>/config.json` (optional),
* `<custom>` (optional, with the path `<custom>` defined by `./train.py ... --override <custom>`, and can be given multiple times),

where later files take precedent, ie. their entries override duplicate entries from earlier files.
The `<custom>` JSON file is useful for setting device specific options;
for example, to run the `box` example on an Nvidia GPU,

```shell
./train.py box re 0 --override box/cuda.json
```

The options available in the JSON files are discussed in the "Configuration" section below.

The following pre-computed datasets are required for training:

* `<dir>/boundary_input.csv`: each row is a list of non-constant kinematic variables in the order of the inputs of the model, which defines a phase space point with the kinematic constant defined in `<dir>/config.json`.
    These are the phase space points used for the boundary values.
* `<dir>/<part>/boundary_output.csv`: each row is a list of the `<part>` parts of the epsilon coefficients of the integrals evaluated at the points defined in the previous file.
    The row entries are ordered as the flattening of the shape `(function, epsilon)`.

If the boundary dataset is stored in a separate location, `<dir>` can be changed with the `--datasets` option.
Note that the training dataset, including both inputs and connection matrices, is generated dynamically at runtime.

Training statistics (value of loss function and learning rate against epoch) can be plotted after training using:

```shell
./stats.py <dir>/<part>/<replica>
```

### Testing

Generic testing may be performed using the script `test.py`.
This evaluates testing errors of, for estimate `g` and target `G`,

* absolute difference: `|G-g|` (not susceptible to small-value divergences like the following relative metrics)
* relative difference: `|(G-g)/G|` (measure of random error)
* natural logarithm of the ratio: `log(|g/G|)` (measure of systematic error)

against a testing dataset, which is defined similarly to the boundary dataset with files `<dir>/testing_input.csv` and `<dir>/<part>/boundary_output.csv`.
The `datasets` option can optionally be provided if the datasets are stored at a different location than the models.

For example,

```shell
./test.py --plot --verbose box re 0
```

The differential error can also be calculated with the script `derivative_difference.py`, for instance,

```shell
./derivative_difference.py box --part re --replica 0 --verbose
```

See `--help` for further usage details.

### Inference

Convenience classes are provided for inference in `lib/inference.py`.
These include:

* `Single` for inference with a single real/imaginary pair of neural networks.
* `Ensemble` for inference with an ensemble model, which additionally provides an error estimate.

Their usage is exemplified by `box/inference.py`.

## Configuration

The configuration files include the options as follows, with supported choices listed where appropriate.

```python
{
    "device": str,  # "cpu", "cuda", "mps", "cuda:0", etc
    "dtype": str,  # "float32", "float64"
    "dimensions": {
        "input": {
            "variables": int
        },
        "output": {
            "epsilon_orders": int,
            "functions": int
        },
        "connection": {
            "epsilon_orders": int
        }
    },
    "phase_space": {
        "scale": float,
        "const": float,
        "cut": {
            "physical": float,
            "spurious": float
        }
    },
    "hyperparameters": {
        "optimiser": {
            "algorithm": str,  # "Adam", "RAdam"
            "initial_learning_rate": float
        },
        "learning_rate_scheduler": {
            "factor": float,
            "patience": int,
            "threshold": float,
            "cooldown": int
        },
        "iterations": {
            "batch_size": int,
            "partition_size": int,
            "number_partitions": int
        },
        "architecture": {
            "hidden_layers": List[int],
            "activation": str  # "GELU", "Tanh"
        },
        "loss_function": {
            "boundary_bias": float,
            "boundary_derivatives": bool
        }
    },
    "termination": {
        "epochs": int,
        "seconds": float,
        "learning_rate": {
            "target": float,
            "patience": int
        }
    },
    "zeros": {
        "re": List[List[int]],
        "im": List[List[int]]
    }
}
```

Note that the cuts are defined as a proportion of the scale, so

```json
    "phase_space": {
        "scale": 5.0,
        "const": 1.0,
        "cut": {
            "physical": 0.1,
            "spurious": 0.01
        }
    },
```

would give:

* scale = 5
* const = 1
* physical cut = 0.5
* spurious cut = 0.05

## Examples

The following integral families are provided as examples.
All lines are massless unless stated otherwise.

The definitions of the master integrals, the differential equations they satisfy, and the independent kinematic variables are provided in the example directories under the subdirectory `system/`, as listed in the example `README.md`.
The differential equations are provided as connections matrices, ordered as the independent kinematic variables.

Each example uses one of the following programs to generate the integral values for the datasets:

* [`AMFlow`](https://gitlab.com/multiloop-pku/amflow)
* [`handyG`](https://gitlab.com/mule-tools/handyg)

Further details can be found in the `README.md` inside each example directory, including:

* Usage instructions, covering dataset generation and training.
* Links to example datasets and trained models.
* Typical epoch times for a variety of machines.
    The configuration committed to the repository subdirectory is used for the timing test unless states otherwise.
    The number of samples per epoch is shown as (batch size)x(iterations).

### Box

One-loop box from $1/\epsilon^2$ to $\epsilon^2$.

Directory: `box/`.

### One-external-mass box

One-loop box with a single off-shell leg from $1/\epsilon^2$ to $\epsilon^2$.

Directory: `box1m/`.

### One-external-mass double-box

Two-loop planar double-box with a single off-shell leg from $1/\epsilon^4$ to $\epsilon^0$.

Directory: `t331ZZZM/`.

### Heavy-cross-box

Two-loop non-planar double-box with an internal massive closed loop from $1/\epsilon^4$ to $\epsilon^0$.

Directory: `heavycrossbox/`.

### Top-box

Two-loop planar double-box with an internal massive closed loop and an external line (two legs) of the same mass from $1/\epsilon^2$ to $\epsilon^0$.

Directory: `topbox/`.

## Development

### Converting Mathematica to Python

This is required to code up the connection matrix for `torch`.
Syntax conversion is automated in Python by `sympy`.
A convenience script can be used to convert a Mathematica export `des.m`, for example, as

```shell
./mma2py.py --full < des.m > des.py
black des.py  # auto-formatting
```

This `vim` `regex` writes numbers as explicit floats, although it doesn't appear to provide a performance advantage.

```vim
:%s/\(\*\{2\} \?\)\@<!\([(\- ]\)\@<=\(\d\+\)\([) ]\)\@=/\3.0/g
```

## Licence

This work is licenced under the [GNU GPLv3](https://gitlab.com/feynman-integrals-nn/feynman-integrals-nn/-/raw/main/LICENSE).
