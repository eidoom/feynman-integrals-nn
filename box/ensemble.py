#!/usr/bin/env python3

import itertools
import torch, numpy
from finn.lib.inference import Ensemble
from finn.lib.configuration import get_cfg, ps_params
from finn.box.phase_space import PhaseSpace


if __name__ == "__main__":
    root = "box"
    cfg = get_cfg(root)
    device = cfg["device"]
    ps = PhaseSpace(*ps_params(cfg), cfg["dtype"], device)

    # n = 10000
    n = 1
    torch.manual_seed(1337)
    x = ps(n)
    # restore the fixed variable (must be last variable in list)
    s12s = torch.full((1, n), ps.const, device=device)
    x = torch.cat((x, s12s))
    x = x.T

    # denote zero coefficients of solutions by [function number, eps order]
    re_z = cfg["zeros"]["re"]
    im_z = cfg["zeros"]["im"]

    # # test number of replicas
    # for j in range(8, 10):
    #     print(j)
    #     errs = []
    #     for reps in itertools.combinations(range(10), j):
    #         reps = [str(i) for i in reps]
    #         ensemble = Ensemble(root, reps, ps.const, cfg["device"], re_z, im_z)
    #         ensemble.eval(x)
    #         err_abs = ensemble.error_abs().mean().item()
    #         errs.append(err_abs)
    #         # err_rel = ensemble.error_rel().mean().item()
    #         # print(f"{j:2d} {err_abs:7.2g} {err_rel:7.2g}")
    #     errs = numpy.array(errs)
    #     print(errs.std())

    ensemble = Ensemble(
        root,
        [str(i) for i in range(10)],
        const=ps.const,
        device=cfg["device"],
        re_z=re_z,
        im_z=im_z,
    )
    ensemble.eval(x)

    # model = init_model(rep, cfg)

    # tid, ff, nn = get_testing_data(root, part, cfg, model)
