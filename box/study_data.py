#!/usr/bin/env python3

import pathlib

import numpy, matplotlib.pyplot, matplotlib.cm, torch

from finn.lib.configuration import get_cfg, ps_params
from finn.lib.study_data import hist_f_cum, hist_f_f, hist_f_eps, get_a_data, hist_a_cum

from finn.box.phase_space import PhaseSpace
from finn.box.connection_matrix import ConnectionMatrix


def hist_input(root, data, s12, name=None, suffix=("png", "pdf")):
    print("Input histogram...")

    spurious = s12 + data

    t = -data

    x = numpy.stack((t, spurious), axis=1)

    bins_input = numpy.histogram_bin_edges(x, bins="auto")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$\ell_{i}$")
    ax.set_ylabel("Count")

    ax.hist(
        x,
        bins=bins_input,
        histtype="step",
        label=("$-s_{23}$", "$s_{12}+s_{23}$"),
    )

    ax.legend(reverse=True)

    for f in suffix:
        fig.savefig(
            root / f"{'dist_input' if name is None else name}.{f}",
            bbox_inches="tight",
        )


def hist_output(part, data, bins=None, name=None, suffix=("png", "pdf")):
    print("Output histogram...")

    nfns = 3
    neps = 5

    data = data.reshape(len(data), nfns * neps)

    (choose,) = data.std(axis=0).nonzero()

    data = data[:, choose]

    p = "Re"
    labels = numpy.array(
        [f"{p} $f^{{({j})}}_{{{i}}}$" for i in range(1, nfns + 1) for j in range(neps)]
    )[choose]

    bins = numpy.histogram_bin_edges(data, bins="auto") if bins is None else bins

    colours = matplotlib.cm.tab20(range(nfns * neps))[choose]

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$f^{(j)}_{i}$")
    ax.set_ylabel("Count")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        label=labels,
        color=colours,
    )

    ax.legend(
        title="Non-const fns",
        reverse=True,
    )

    for f in suffix:
        fig.savefig(
            part / f"{'dist_output' if name is None else name}.{f}",
            bbox_inches="tight",
        )


if __name__ == "__main__":
    root = pathlib.Path("box")

    part = root / "re"

    cfg = get_cfg(root, part)

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    scale, s12, pcut, scut = ps_params(cfg)

    phase_space = PhaseSpace(scale, s12, pcut, scut, dtype, device)

    # data_in = numpy.loadtxt(root / "testing_input.csv", delimiter=",")
    n = 1000000
    x = phase_space.test(n, cut_spurious=False)
    data_in = x.squeeze().cpu().numpy()

    hist_input(root, data_in, s12)

    data_de = get_a_data(cfg, ConnectionMatrix, x, s12)

    hist_a_cum(root, data_de)

    print("mean A^{(k)}_{q,ij}:", f"{abs(data_de).mean():.2g}")

    data_fn = numpy.loadtxt(part / "testing_output.csv", delimiter=",")

    hist_output(part, data_fn)
    hist_f_cum(part, data_fn)
    hist_f_eps(part, cfg, data_fn)
    hist_f_f(part, cfg, data_fn)
