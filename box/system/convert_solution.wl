(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


family = box;


{is, sijs, des} = Get["box_MIs_and_DEs_dimless.m"];
(* is = Laporta master integrals in the DEs with kinematic prefactors to make dimensionless *)
(* sijs = independent kinematic variables *)
(* des = connection matrices for the DEs, ordered as sijs *)

(* for instance, 

d MIs / d s23 = As23 . MIs

with 

As23 = des[[2]];

*)


js=Union@Cases[is,_j,Infinity];


j2mi = Get[ToString[family]<>"_reduction_to_utbasis.m"]; (* expression of the Laporta MIs j[box,...]'s in terms of pure MIs mi[box,i] *)


mi2spfuncs = Get[ToString[family]<>"_mi2spfuncs.m"]; (* expressions of the pure basis integrals mi[box,i] in terms of MPLs, logs and constants *)


(* the analytic solution to the DEs "des" up to order eps^2 is given by *)
solution = Normal@Series[is /. j2mi /. mi[a__]:>(mi[a]+eps^3*ALARM) /. mi2spfuncs,{eps,0,2}];

(* ALARM is just a dummy variable to make sure we don't miss orders in eps because of the truncation 
to eps^2 in the expression of the pure MIs in terms of special functions *)


solTrans=solution/.G[{x__},1]:>G[x,1]/.ipi->I*Pi/.zeta2->Zeta[2]/.zeta3->Zeta[3];


(* analytic continuation to s12 channel: s12>0, s23<0 *)
logcont = {
  Log[-s12]->Log[s12]-I*Pi,
  Log[s12+s23]->If[s12+s23>0,Log[s12+s23],Log[-s12-s23]+I*Pi]
  };
  
mplcont = G[a___,-s12/s23,b___]:>G[a,SubPlus[-s12/s23],b];


solCont = solTrans/.logcont//.mplcont;


solEps = Transpose@Table[Coefficient[solCont,eps,i],{i,-2,2}];


solEps//Dimensions


sol=FullSimplify@solEps;


Put[sol, "../solution.m"];
