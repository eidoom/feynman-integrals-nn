{mi[box, 1] -> 6/eps^2 + (5*ipi^2)/2 - Log[-s12]^2/2 + 
   (-3*Log[-s12] - 3*Log[-s23])/eps + 4*Log[-s12]*Log[-s23] - Log[-s23]^2/2 + 
   eps*((-11*ipi^3)/2 - 21*ipi*zeta2 - 18*zeta3 - 
     4*G[{0, 0, -(s12/s23)}, 1] - (9*ipi^2*Log[-s12])/4 - 
     4*G[{0, -(s12/s23)}, 1]*Log[-s12] + 2*ipi*Log[-s12]^2 + 
     (13*Log[-s12]^3)/6 - (9*ipi^2*Log[-s23])/4 + 4*G[{0, -(s12/s23)}, 1]*
      Log[-s23] - 4*ipi*Log[-s12]*Log[-s23] - 4*Log[-s12]^2*Log[-s23] + 
     2*ipi*Log[-s23]^2 + (5*Log[-s23]^3)/6 + 2*ipi^2*Log[s12 + s23] - 
     2*Log[-s12]^2*Log[s12 + s23] + 4*Log[-s12]*Log[-s23]*Log[s12 + s23] - 
     2*Log[-s23]^2*Log[s12 + s23]) + eps^2*((13*ipi^4)/2 + 42*ipi^2*zeta2 + 
     (963*zeta2^2)/20 + 4*ipi*zeta3 + 4*G[{0, 0, 0, -(s12/s23)}, 1] - 
     4*G[{0, 0, -(s12/s23), -(s12/s23)}, 1] - 
     4*G[{0, -(s12/s23), 0, -(s12/s23)}, 1] - 
     4*G[{-(s12/s23), 0, 0, -(s12/s23)}, 1] + (11*ipi^3*Log[-s12])/2 + 
     21*ipi*zeta2*Log[-s12] + 11*zeta3*Log[-s12] + 4*G[{0, 0, -(s12/s23)}, 1]*
      Log[-s12] - 4*G[{0, -(s12/s23), -(s12/s23)}, 1]*Log[-s12] - 
     4*G[{-(s12/s23), 0, -(s12/s23)}, 1]*Log[-s12] - (ipi^2*Log[-s12]^2)/24 + 
     2*G[{0, -(s12/s23)}, 1]*Log[-s12]^2 - (8*ipi*Log[-s12]^3)/3 - 
     (41*Log[-s12]^4)/24 + (11*ipi^3*Log[-s23])/2 + 21*ipi*zeta2*Log[-s23] + 
     11*zeta3*Log[-s23] + 4*G[{0, -(s12/s23), -(s12/s23)}, 1]*Log[-s23] + 
     4*G[{-(s12/s23), 0, -(s12/s23)}, 1]*Log[-s23] + 
     (13*ipi^2*Log[-s12]*Log[-s23])/3 + 4*ipi*Log[-s12]^2*Log[-s23] + 
     2*Log[-s12]^3*Log[-s23] - (ipi^2*Log[-s23]^2)/24 - 
     2*G[{0, -(s12/s23)}, 1]*Log[-s23]^2 + Log[-s12]^2*Log[-s23]^2 - 
     (4*ipi*Log[-s23]^3)/3 - (2*Log[-s12]*Log[-s23]^3)/3 - 
     (3*Log[-s23]^4)/8 - (11*ipi^3*Log[s12 + s23])/2 - 
     21*ipi*zeta2*Log[s12 + s23] - 4*zeta3*Log[s12 + s23] - 
     2*ipi^2*Log[-s12]*Log[s12 + s23] + 2*ipi*Log[-s12]^2*Log[s12 + s23] + 
     (8*Log[-s12]^3*Log[s12 + s23])/3 - 2*ipi^2*Log[-s23]*Log[s12 + s23] - 
     4*ipi*Log[-s12]*Log[-s23]*Log[s12 + s23] - 4*Log[-s12]^2*Log[-s23]*
      Log[s12 + s23] + 2*ipi*Log[-s23]^2*Log[s12 + s23] + 
     (4*Log[-s23]^3*Log[s12 + s23])/3 + ipi^2*Log[s12 + s23]^2 - 
     Log[-s12]^2*Log[s12 + s23]^2 + 2*Log[-s12]*Log[-s23]*Log[s12 + s23]^2 - 
     Log[-s23]^2*Log[s12 + s23]^2), 
 mi[box, 2] -> eps^(-2) + ipi^2/12 - Log[-s23]/eps + Log[-s23]^2/2 + 
   eps*((-7*zeta3)/3 - (ipi^2*Log[-s23])/12 - Log[-s23]^3/6) + 
   eps^2*((-47*ipi^4)/1440 + (7*zeta3*Log[-s23])/3 + (ipi^2*Log[-s23]^2)/24 + 
     Log[-s23]^4/24), mi[box, 3] -> eps^(-2) + ipi^2/12 - Log[-s12]/eps + 
   Log[-s12]^2/2 + eps*((-7*zeta3)/3 - (ipi^2*Log[-s12])/12 - 
     Log[-s12]^3/6) + eps^2*((-47*ipi^4)/1440 + (7*zeta3*Log[-s12])/3 + 
     (ipi^2*Log[-s12]^2)/24 + Log[-s12]^4/24)}
