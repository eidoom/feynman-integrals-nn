(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
z ={0,0,1};
e = Sqrt[s]/2;
dot[x_, y_] := x[[1]]*y[[1]] - x[[2;;]] . y[[2;;]];
n = 4;


p[1]=e Prepend[-z,-1];
p[2]=e Prepend[z,-1];
p[3]=e Prepend[Ry[\[Theta]] . z,1];

p[n]=-Plus@@p/@Range[n-1]//Factor;

{MatrixForm[p[#]]&/@Range[n]}//TableForm


Plus@@p/@Range[n]//Simplify
dot[p[#],p[#]]&/@Range[n]//Simplify


sijs=FullSimplify[{2*dot[p[1], p[2]], 2*dot[p[1],p[3]]}]


sijs//InputForm
