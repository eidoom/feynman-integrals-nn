(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* factor to be pulled out - must have dim of mass^2 ~ s_{ij} *)
factor = s23;


(* load DEs for dimensionsfull basis *)
sijs = {s12,s23};
des = Get["box_des.m"];
mis=Keys[Get["box_reduction_to_utbasis.m"]];


scaling=sijs . des  // Simplify;
Print["check scaling: ", scaling//DiagonalMatrixQ];
Print["check integrability: ", 
  DeleteDuplicates[des[[1]] . des[[2]]-des[[2]] . des[[1]]+(D[des[[1]],sijs[[2]]]-D[des[[2]],sijs[[1]]])//Simplify//Flatten]==={0}];


(* function which returns the dimension in units of mass^2 for an expression in terms of sij's and j's *)
looporder = 1;
MassDim[expr_]:= Simplify[(expr/.Thread[sijs->\[CapitalLambda]*sijs]/.j[fam_,b__]:>j[fam,b]*\[CapitalLambda]^(-Total[{b}]+looporder*(2-eps)))/expr,\[CapitalLambda]>0];


dimensions=Exponent[MassDim/@mis,\[CapitalLambda]]
Print["check that scaling matches dimension: ", DeleteDuplicates[DiagonalMatrix[dimensions]-scaling//Simplify//Flatten]==={0}];


T = DiagonalMatrix[(factor^dimensions)]
newmis=Inverse[T] . mis;
Print["check that new basis is dimensionless: ", Union[MassDim/@newmis]==={1}];


newdes = Table[Factor[Inverse[T] . (des[[jj]] . T-D[T,sijs[[jj]]])],{jj,Length[sijs]}]


Print["check scaling: ", Union[Flatten[sijs . newdes//Simplify]]==={0}];
Print["check integrability: ",
 DeleteDuplicates[newdes[[1]] . newdes[[2]]-newdes[[2]] . newdes[[1]]+(D[newdes[[1]],sijs[[2]]]-D[newdes[[2]],sijs[[1]]])//Simplify//Flatten]==={0}];


newfinal = {newmis, sijs, newdes};


Put[newfinal,"box_MIs_and_DEs_dimless.m"];
