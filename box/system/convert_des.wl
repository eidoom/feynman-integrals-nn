(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


{mis, sijs, des0} = Get["box_MIs_and_DEs_dimless.m"]


des1 = des0[[2;;]]


des2 = FullSimplify@Table[Coefficient[des1,eps,i],{i,0,1}]


des2//Dimensions


Put[Flatten@des2/.{0->z},"des.m"];
