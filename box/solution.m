{{0, 1, 2 - Log[-s23] + Log[s23], 4 - Pi^2/12 + 2*Log[s23] + 
   (Log[-s23]^2 + Log[s23]^2 - 2*Log[-s23]*(2 + Log[s23]))/2, 
  (96 - 2*Pi^2 - 2*Log[-s23]^3 + 6*Log[-s23]^2*(2 + Log[s23]) + 
    Log[-s23]*(-48 + Pi^2 - 6*Log[s23]*(4 + Log[s23])) + 
    Log[s23]*(48 - Pi^2 + 2*Log[s23]*(6 + Log[s23])) - 28*Zeta[3])/12}, 
 {0, 1, 2 + I*Pi - Log[s12] + Log[s23], 
  4 + (-Pi^2 - 6*(Pi + I*Log[s12])^2)/12 + 2*Log[s23] + Log[s23]^2/2 + 
   I*(Pi + I*Log[s12])*(2 + Log[s23]), 
  (96 + Pi^2*((-I)*Pi + Log[s12]) - 2*((-I)*Pi + Log[s12])^3 + 48*Log[s23] + 
    12*Log[s23]^2 + 2*Log[s23]^3 - (7*Pi^2 + (12*I)*Pi*Log[s12] - 
      6*Log[s12]^2)*(2 + Log[s23]) + (6*I)*(Pi + I*Log[s12])*
     (8 + Log[s23]*(4 + Log[s23])) - 28*Zeta[3])/12}, 
 {(4*s23)/s12, (2*s23*(I*Pi - Log[s12] - Log[-s23] + 2*Log[s23]))/s12, 
  (-2*s23*(2*Pi^2 + 3*(Log[-s23] - Log[s23])*(I*Pi - Log[s12] + Log[s23])))/
   (3*s12), (s23*(-12*G[0, 0, SubPlus[-(s12/s23)], 1] - 
     (12*I)*Pi*Log[s12]^2 + 6*Log[s12]^3 + Pi^2*((-I)*Pi + Log[s12]) + 
     7*Pi^2*Log[-s23] + (12*I)*Pi*Log[s12]*Log[-s23] - 
     12*Log[s12]^2*Log[-s23] + (6*I)*Pi*Log[-s23]^2 + 2*Log[-s23]^3 + 
     12*G[0, SubPlus[-(s12/s23)], 1]*(I*Pi - Log[s12] + Log[-s23]) + 
     6*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
      (Log[s12] - Log[-s23])*((2*I)*Pi - Log[s12] + Log[-s23]) - 
     4*(2*Pi^2 + (3*I)*(Pi + I*Log[s12])*Log[-s23])*Log[s23] + 
     (6*I)*(Pi + I*Log[s12] + I*Log[-s23])*Log[s23]^2 + 4*Log[s23]^3 - 
     68*Zeta[3]))/(6*s12), 
  -1/360*(s23*(41*Pi^4 - 720*G[0, 0, 0, SubPlus[-(s12/s23)], 1] + 
      720*G[0, 0, SubPlus[-(s12/s23)], SubPlus[-(s12/s23)], 1] + 
      720*G[0, SubPlus[-(s12/s23)], 0, SubPlus[-(s12/s23)], 1] + 
      720*G[SubPlus[-(s12/s23)], 0, 0, SubPlus[-(s12/s23)], 1] + 
      360*G[0, SubPlus[-(s12/s23)], 1]*(Pi + I*Log[s12] - I*Log[-s23])*
       (Pi + I*Log[s12] + I*Log[-s23] - (2*I)*Log[s23]) + 
      720*G[0, 0, SubPlus[-(s12/s23)], 1]*(I*Pi - Log[s12] + Log[s23]) + 
      60*((-2*I)*Pi^3*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + 
           I*Pi] + (2*I)*Pi^3*Log[s12] + 6*Pi^2*If[s12 + s23 > 0, 
          Log[s12 + s23], Log[-s12 - s23] + I*Pi]*Log[s12] - 
        (6*I)*Pi*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]^2*
         Log[s12] - 6*Pi^2*Log[s12]^2 + (18*I)*Pi*If[s12 + s23 > 0, 
          Log[s12 + s23], Log[-s12 - s23] + I*Pi]*Log[s12]^2 + 
        3*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]^2*
         Log[s12]^2 - (12*I)*Pi*Log[s12]^3 - 
        8*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[s12]^3 + 5*Log[s12]^4 + 12*G[0, SubPlus[-(s12/s23)], 
          SubPlus[-(s12/s23)], 1]*((-I)*Pi + Log[s12] - Log[-s23]) + 
        12*G[SubPlus[-(s12/s23)], 0, SubPlus[-(s12/s23)], 1]*
         ((-I)*Pi + Log[s12] - Log[-s23]) - I*Pi^3*Log[-s23] - 
        6*Pi^2*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[-s23] + (6*I)*Pi*If[s12 + s23 > 0, Log[s12 + s23], 
           Log[-s12 - s23] + I*Pi]^2*Log[-s23] + 7*Pi^2*Log[s12]*Log[-s23] - 
        (12*I)*Pi*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[s12]*Log[-s23] - 6*If[s12 + s23 > 0, Log[s12 + s23], 
           Log[-s12 - s23] + I*Pi]^2*Log[s12]*Log[-s23] + 
        (6*I)*Pi*Log[s12]^2*Log[-s23] + 12*If[s12 + s23 > 0, Log[s12 + s23], 
          Log[-s12 - s23] + I*Pi]*Log[s12]^2*Log[-s23] - 
        6*Log[s12]^3*Log[-s23] + 3*Pi^2*Log[-s23]^2 - 
        (6*I)*Pi*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[-s23]^2 + 3*If[s12 + s23 > 0, Log[s12 + s23], 
           Log[-s12 - s23] + I*Pi]^2*Log[-s23]^2 + (6*I)*Pi*Log[s12]*
         Log[-s23]^2 - 3*Log[s12]^2*Log[-s23]^2 + (2*I)*Pi*Log[-s23]^3 - 
        4*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[-s23]^3 + 2*Log[s12]*Log[-s23]^3 + Log[-s23]^4 + 
        I*Pi^3*Log[s23] - Pi^2*Log[s12]*Log[s23] - 
        (12*I)*Pi*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[s12]*Log[s23] + (12*I)*Pi*Log[s12]^2*Log[s23] + 
        6*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*
         Log[s12]^2*Log[s23] - 6*Log[s12]^3*Log[s23] - 
        7*Pi^2*Log[-s23]*Log[s23] + (12*I)*Pi*If[s12 + s23 > 0, 
          Log[s12 + s23], Log[-s12 - s23] + I*Pi]*Log[-s23]*Log[s23] - 
        (12*I)*Pi*Log[s12]*Log[-s23]*Log[s23] - 
        12*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + I*Pi]*Log[s12]*
         Log[-s23]*Log[s23] + 12*Log[s12]^2*Log[-s23]*Log[s23] - 
        (6*I)*Pi*Log[-s23]^2*Log[s23] + 6*If[s12 + s23 > 0, Log[s12 + s23], 
          Log[-s12 - s23] + I*Pi]*Log[-s23]^2*Log[s23] - 
        2*Log[-s23]^3*Log[s23] + 4*Pi^2*Log[s23]^2 + (6*I)*Pi*Log[-s23]*
         Log[s23]^2 - 6*Log[s12]*Log[-s23]*Log[s23]^2 - (2*I)*Pi*Log[s23]^3 + 
        2*Log[s12]*Log[s23]^3 + 2*Log[-s23]*Log[s23]^3 - Log[s23]^4 + 
        4*((7*I)*Pi + 3*If[s12 + s23 > 0, Log[s12 + s23], Log[-s12 - s23] + 
             I*Pi] - 10*Log[s12] - 10*Log[-s23] + 17*Log[s23])*Zeta[3])))/
    s12}}
