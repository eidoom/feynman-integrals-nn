#!/usr/bin/env python3

import numpy

from finn.lib.generate_ps import generate_dataset_inputs


def boundaries(ps, n):
    assert n == 2

    s12 = float(ps.const)
    cut = float(ps.cut_phys)

    # s12 + s23 > 0
    s23l = -s12 + cut

    # -s23 > 0
    s23h = -cut

    return numpy.array([[s23l], [s23h]])


if __name__ == "__main__":
    generate_dataset_inputs("box", boundaries)
