import math
import torch

from finn.lib.phase_space import PhaseSpaceBase


class PhaseSpace(PhaseSpaceBase):
    def points(self, n):
        """
        (s12=const,) s23
        """
        s12 = self.const
        cut = self.cut_phys

        # -s12+cut <= s23 <= -cut
        s23 = self.uniform(-s12 + cut, -cut, n)

        return s23.unsqueeze(0)

    def a_phys(self, s):
        """
        s12 channel: (s12 > 0,) -s23 > 0, s12 + s23 > 0
        """
        s12 = self.const
        s23 = s[0]

        return torch.stack((-s23, s12 + s23))

    def a_spur(self, s):
        """
        no spurious cuts
        """
        ...

    def test_singular(self, s, cut_spurious):
        """
        override to not do spurious checks
        """
        return (self.a_phys(s) < self.cut_phys).any(axis=0)
