(* ::Package:: *)

Install["handyG"];


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


cfg = Association @@ Import["config.json"];


s12n = "const" /. cfg["phase_space"];
trans = {t_} -> {s12 -> s12n, s23 -> t};
toRepl[filename_] := # /. trans & /@ Import[filename]


sol = Get["solution.m"];
mpls = Cases[sol, _G, Infinity] // DeleteDuplicates;
geval[ps_] := Module[{gg = Thread[mpls->(mpls/.#)]}, N[sol /. gg /. #]] & /@ ps;


flat[part_, x_] := Flatten /@ part @ x;


bvPS = toRepl["boundary_input.csv"];


bv = geval[bvPS];


Export["re/boundary_output.csv", flat[Re, bv]];
Export["im/boundary_output.csv", flat[Im, bv]];


testPS = toRepl["testing_input.csv"];


test = geval[testPS];


Export["re/testing_output.csv", flat[Re, test]];
Export["im/testing_output.csv", flat[Im, test]];
