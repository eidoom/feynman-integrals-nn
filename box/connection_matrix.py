import torch

from finn.lib.connection_matrix import ConnectionMatrixBase


class ConnectionMatrix(ConnectionMatrixBase):
    def raw(self, x):
        # flattened dimensions: epsilon=2, input_variable=1, function=3, function=3

        s12 = self.const
        s23 = x[0]

        z = torch.zeros_like(s23)

        return torch.stack(
            (
                z,
                z,
                z,
                z,
                z,
                z,
                2 / (s12 + s23),
                -2 * s23 / (s12 * (s12 + s23)),
                1 / s23,
                z,
                z,
                z,
                z,
                1 / s23,
                z,
                -4 / (s12 + s23),
                4 * s23 / (s12 * (s12 + s23)),
                1 / (s12 + s23),
            )
        )
