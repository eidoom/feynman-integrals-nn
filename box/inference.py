#!/usr/bin/env python3

import torch
from finn.lib.inference import Single, Ensemble
from finn.lib.configuration import get_cfg, ps_params
from finn.box.phase_space import PhaseSpace


if __name__ == "__main__":
    root = "box"
    cfg = get_cfg(root)
    device = cfg["device"]
    ps = PhaseSpace(*ps_params(cfg), cfg["dtype"], device)

    n = 4
    x = ps(n)
    # restore the fixed variable (must be last variable in list)
    s12s = torch.full((1, n), 20, device=device)
    x = torch.cat((x, s12s))
    x = x.T

    single = Single(root, "0", ps.const, cfg["device"])
    single.eval(x)
    val = single.value()
    print(val)

    ensemble = Ensemble(root, ["0", "1"], ps.const, cfg["device"])
    ensemble.eval(x)
    print(ensemble.value())
    print(ensemble.error_abs())
