#!/usr/bin/env python3

import pathlib

import torch, numpy, matplotlib.pyplot, matplotlib.cm

from finn.lib.configuration import get_cfg
from finn.lib.model import FeynmanIntegralsModel
from finn.lib.testing import (
    get_abs_diff,
    get_args,
    get_colours,
    get_labels,
    get_rel_diff,
    get_testing_data,
    init_model,
    plot_abs_diff_cum,
    plot_abs_diff_each,
    plot_rel_diff_cum,
    plot_rel_diff_each,
    plot_rel_diff_i,
    plot_training_statistics,
)


def plot_testing_inference(part, xx, ff, nn, labels):
    print(f"Plotting output")

    xx = xx.reshape(xx.shape[0], -1)
    ff = ff.reshape(ff.shape[0], -1)
    nn = nn.reshape(nn.shape[0], -1)

    i = numpy.argsort(xx, axis=0).flat

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

    ax.set_xlabel("$s_{23}$")
    ax.set_ylabel("$f_i^{(j)}$")

    colours = numpy.concatenate(
        (
            matplotlib.cm.tab20(range(20)),
            matplotlib.cm.Set3(range(4)),
        )
    )

    ax.set_prop_cycle(color=colours)
    ax.plot(
        xx[i],
        ff[i],
        linestyle="dashed",
    )

    ax.set_prop_cycle(color=colours)
    ax.plot(
        xx[i],
        nn[i],
        label=labels,
        linestyle="solid",
    )

    ax.legend(
        ncols=5,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
    )

    for f in ("png", "pdf"):
        fig.savefig(part / f"inference.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


if __name__ == "__main__":
    args = get_args()
    root = pathlib.Path("box")
    part = root / args.part
    rep = part / args.replica

    plot_training_statistics(rep)

    cfg = get_cfg(root, part, args.override)

    model = init_model(rep, cfg)

    tid, ff, nn = get_testing_data(root, part, cfg, model)

    labels = get_labels(cfg, part.stem, eps_start=-2)
    colours = get_colours(cfg)

    ad = get_abs_diff(ff, nn)
    rd, _, nonzero = get_rel_diff(ff, nn)

    labels_nz = labels[nonzero.flat]
    colours_nz = colours[nonzero.flat]

    plot_rel_diff_each(rep, rd, labels_nz, colours_nz, nonzero, 5)
    plot_rel_diff_cum(rep, rd)

    plot_abs_diff_each(rep, ad, labels, colours, 5)
    plot_abs_diff_cum(rep, ad)

    plot_testing_inference(rep, tid.T, ff, nn, labels)

    if args.everything:
        for i in range(nonzero.sum()):
            relative_difference_i(rep, cfg, rd, labels_nz, colours_nz, i)
