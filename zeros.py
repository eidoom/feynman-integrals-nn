#!/usr/bin/env python3

import argparse, pathlib

import numpy

from finn.lib.configuration import get_cfg


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "models",
        type=pathlib.Path,
        help="Path to integral family models directory. This is required for the configuration file `config.json`.",
    )
    parser.add_argument(
        "datasets",
        type=pathlib.Path,
        nargs="?",
        help="Path to integral family datasets directory. Only necessary if different from models directory. This is required for the boundary dataset.",
    )
    parser.add_argument(
        "part",
        type=str,
        choices=["re", "im"],
        help="Choose Real (re) or Imaginary (im) part.",
    )
    parser.add_argument(
        "-s",
        "--set",
        type=str,
        choices=["boundary", "testing"],
        default="boundary",
        help="Choose dataset [default: boundary]",
    )
    parser.add_argument(
        "-c",
        "--chop",
        type=float,
        default=numpy.finfo(numpy.float32).eps,
        help="Replace numbers smaller in absolute magnitude than CHOP by zero [default: float32 eps ~ 1e-7].",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity of output.",
    )
    args = parser.parse_args()
    args.datasets = args.models if args.datasets is None else args.datasets
    return args


if __name__ == "__main__":
    args = get_args()

    cfg = get_cfg(args.models, verbose=False)
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    part = numpy.loadtxt(
        args.datasets / args.part / f"{args.set}_output.csv", delimiter=",", ndmin=2
    ).reshape(-1, num_fn, num_eps)

    negl = (part != 0) & (abs(part) < args.chop)
    if negl.any():
        n = negl.sum()
        print(f"Warning! {n} coefficients with magnitude <= {args.chop:.2g} .")
        prob_points = numpy.argwhere(negl)
        print("Problem points:")
        print(prob_points)
        print("Chopping")
        zeros = numpy.argwhere(abs(part[0]) <= args.chop)
    else:
        zeros = numpy.argwhere(part[0] == 0)

    if args.verbose:
        nz = len(zeros)
        no = part[0].size
        print(f"Found {nz} zeros out of {no} outputs ({nz / no:.0%}).")

        print("Indices of zeros:")
        print(zeros)

        print("Outputs for a couple of points:")
        print(part[:2])
    else:
        print(zeros.tolist())
