#!/usr/bin/env python3

import argparse, pathlib, operator, math

import numpy, torch

from finn.lib.configuration import get_cfg
from finn.lib.testing import (
    get_abs_diff,
    get_rel_diff_flat,
    get_testing_dataset,
    infer,
    init_model,
    plot_abs_diff_cum,
    plot_rel_diff_cum,
)


def get_args():
    parser = argparse.ArgumentParser(description="Perform generic testing.")
    parser.add_argument(
        "models",
        type=pathlib.Path,
        help="Path to integral family models directory.",
    )
    parser.add_argument(
        "datasets",
        type=pathlib.Path,
        nargs="?",
        help="Path to integral family datasets directory. Only necessary if different from models directory.",
    )
    parser.add_argument(
        "part",
        type=str,
        choices=["re", "im"],
        help="Choose Real (re) or Imaginary (im) part.",
    )
    parser.add_argument(
        "replica",
        type=str,
        nargs="?",
        help="Provide name of replica to test it alone. Leave blank to test all replicas.",
    )
    parser.add_argument(
        "-o",
        "--override",
        type=str,
        metavar="O",
        nargs="+",
        help="Paths to configuration files whose values override the default configuration files.",
    )
    parser.add_argument(
        "-n",
        "--name",
        type=str,
        metavar="N",
        default="testing",
        help="Name of dataset to use [default: testing]",
    )
    parser.add_argument(
        "-c",
        "--chop",
        type=float,
        metavar="C",
        default=numpy.finfo(numpy.float32).eps,
        help="Replace numbers smaller in absolute magnitude than C by zero [default: float32 eps ~ 1e-7].",
    )
    parser.add_argument(
        "-p",
        "--plot",
        action="store_true",
        help="Generate plots.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity.",
    )
    args = parser.parse_args()
    args.datasets = args.models if args.datasets is None else args.datasets
    return args


def get_ratio(ff, nn, zero_out, v):
    zero_out = numpy.array(zero_out).T

    mask = numpy.full(ff.shape[1:], True)
    mask[*zero_out] = False

    ff = ff[:, mask]
    nn = nn[:, mask]

    r = abs(nn / ff)

    r = numpy.ma.masked_equal(r, 0)

    with numpy.errstate(divide="print" if v else "ignore"):
        lr = numpy.log(r)

    return lr


def single(rep, cfg, x, ff, zero_coeffs, chop, v):
    model = init_model(rep, cfg, v=v)

    nn = infer(model, x)

    ad = get_abs_diff(ff, nn, v=2 if v else 0)

    if args.plot:
        plot_abs_diff_cum(rep, ad, s=("pdf",), v=v)

    rd = get_rel_diff_flat(ff, nn, zero_coeffs, chop, v=v)

    if args.plot:
        plot_rel_diff_cum(rep, rd, s=("pdf",), v=v)

    lr = get_ratio(ff, nn, zero_coeffs, v)

    print(
        f"{rep.stem:>6}: ad={ad.mean():7.2g}  rd={rd.mean():7.2g}   lr={lr.mean():8.2g}   ({ad.shape[0]:.2g})"
    )

    return ad, rd, lr


def std_err(x):
    return x.std() / math.sqrt(len(x))


if __name__ == "__main__":
    args = get_args()

    root = args.models
    part = root / args.part

    cfg = get_cfg(root, part, args.override, verbose=args.verbose)
    zero_coeffs = cfg["zeros"][args.part]
    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    datadir = args.datasets
    datapart = datadir / args.part

    xx, ff = get_testing_dataset(
        datadir, datapart, cfg, name=args.name, verbose=args.verbose
    )
    x = torch.tensor(xx, device=device, dtype=dtype)

    print(
        "Errors: ad=|g-h|    rd=|(g-h)/g| lr=log(|g/h|) (num. points)\n"
        "                    (relative nonzero only)"
    )

    if args.replica is None:
        reps = [x for x in part.iterdir() if x.is_dir()]
        n = len(reps)

        rds = []
        ads = []
        lrs = []

        for rep in reps:
            ad, rd, lr = single(rep, cfg, x, ff, zero_coeffs, args.chop, args.verbose)
            ads.append(ad)
            rds.append(rd)
            lrs.append(lr)

        ads = numpy.array(ads)
        xn = operator.mul(*ads.shape[:2])
        ads = ads.mean(axis=(1, 2, 3))

        rds = numpy.array(rds).mean(axis=(1,))

        lrs = numpy.array(lrs).mean(axis=(1, 2))

        print(
            "\n"
            f"  mean: ad={ads.mean():7.2g}  rd={rds.mean():7.2g}   lr={lrs.mean():8.2g}   ({xn:.2g})\n"
            f"   err: ad={std_err(ads):7.2g}  rd={std_err(rds):7.2g}   lr={std_err(lrs):8.2g}\n"
            f"   min: ad={ads.min():7.2g}  rd={rds.min():7.2g}   lr={abs(lrs).min():8.2g}\n"
            f"   max: ad={ads.max():7.2g}  rd={rds.max():7.2g}   lr={abs(lrs).max():8.2g}\n"
            f"sorted:"
        )

        names = numpy.array([x.stem for x in reps])
        iad = numpy.argsort(ads)
        ird = numpy.argsort(rds)
        ilr = numpy.argsort(lrs)
        print(f"ad: {names[iad]}")
        print(f"rd: {names[ird]}")
        print(f"lr: {names[ilr]}")
    else:
        single(part / args.replica, cfg, x, ff, zero_coeffs, args.chop, args.verbose)
