#!/usr/bin/env python3

import pathlib

import numpy, matplotlib.pyplot, matplotlib.cm, torch

from finn.lib.configuration import ps_params, get_cfg
from finn.lib.study_data import (
    get_a_data,
    hist_2d,
    hist_a_cum,
    hist_f_cum,
    hist_f_f,
    hist_f_ps,
    hist_letters,
)

from finn.box1m.connection_matrix import ConnectionMatrix
from finn.box1m.phase_space import PhaseSpace


def alphabet(s, s12):
    # s = [s23, s4]
    # s12 channel: -s23 > 0, s4 > 0, s12 + s23 - s4 > 0
    return numpy.array(
        [
            # numpy.full_like(s[0], s12),  # s12
            -s[0],  # s23
            s[1],  # s4
            s12 + s[0] - s[1],  # -s13 = s12 + s23 - s4
            # following are spurious
            s12 - s[1],  # s12 - s4
            s[1] - s[0],  # s4 - s23
        ]
    )


def hist_de(root, data, labels, colours, bins=None, name=None, suffix=("png", "pdf")):
    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(6.4, 5.6),
    )

    ax.set_xlabel("$\mathrm{M}^{(j)}_{i;k,l}$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=numpy.histogram_bin_edges(data, bins="auto") if bins is None else bins,
        weights=numpy.full_like(data, 1 / len(data)),
        histtype="step",
        label=labels,
        color=colours,
    )

    ax.legend(
        title="Nonzero entries",
        ncols=5,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
        reverse=True,
    )

    for f in suffix:
        fig.savefig(
            root / f"{'dist_de' if name is None else name}.{f}", bbox_inches="tight"
        )

    matplotlib.pyplot.close()


def hist_de_flat(root, data, name=None, suffix=("png", "pdf")):
    data = abs(data).flatten()

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(6.4, 4.8),
    )

    ax.set_xscale("log")

    ax.set_xlabel("nonzero $|\mathrm{M}^{(j)}_{i;k,l}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        weights=numpy.full_like(data, 1 / len(data)),
    )

    for f in suffix:
        fig.savefig(
            root / f"{'dist_de_flat' if name is None else name}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()


def hist_de_abs(root, data, labels, colours, name=None, suffix=("png", "pdf")):
    data = abs(data)

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(6.4, 5.6),
    )

    ax.set_xscale("log")

    ax.set_xlabel("$|\mathrm{M}^{(j)}_{i;k,l}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        weights=numpy.full_like(data, 1 / len(data)),
        histtype="step",
        label=labels,
        color=colours,
    )

    ax.legend(
        title="Nonzero entries",
        ncols=5,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
        reverse=True,
    )

    for f in suffix:
        fig.savefig(
            root / f"{'dist_de_abs' if name is None else name}.{f}", bbox_inches="tight"
        )

    matplotlib.pyplot.close()


def hist_output(root, data):
    nfn = 4
    neps = 5

    choose = data.std(axis=0) != 0.0

    data = data[:, choose]

    labels = numpy.array(
        [f"Re $f^{{({j})}}_{{{i}}}$" for i in range(1, nfn + 1) for j in range(neps)]
    )[choose]

    bins = numpy.histogram_bin_edges(data, bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 5.6), tight_layout=True)

    ax.set_xlabel("$f^{(j)}_{i}$")
    ax.set_ylabel("Proportion")

    colours = matplotlib.cm.tab20(range(nfn * neps))[choose]

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        label=labels,
        color=colours,
        weights=numpy.full_like(data, 1 / len(data)),
    )

    ax.legend(
        title="Non-constant functions",
        reverse=True,
        ncols=5,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
    )

    for f in ("png", "pdf"):
        fig.savefig(root / f"dist_output.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_output_abs(root, data):
    nfn = 4
    neps = 5

    choose = data.std(axis=0) != 0.0

    data = abs(data[:, choose])

    labels = numpy.array(
        [f"Re $f^{{({j})}}_{{{i}}}$" for i in range(1, nfn + 1) for j in range(neps)]
    )[choose]

    colours = matplotlib.cm.tab20(range(nfn * neps))[choose]

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(data), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 5.6), tight_layout=True)

    ax.set_xscale("log")
    ax.set_yscale("log")

    ax.set_xlabel("$|f^{(j)}_{i}|$")
    ax.set_ylabel("Proportion")

    ax.hist(
        data,
        bins=bins,
        histtype="step",
        label=labels,
        color=colours,
        weights=numpy.full_like(data, 1 / len(data)),
    )

    ax.legend(
        title="Non-constant functions",
        reverse=True,
        ncols=5,
        bbox_to_anchor=(0.5, -0.15),
        loc="upper center",
    )

    for f in ("png", "pdf"):
        fig.savefig(root / f"dist_output_abs.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def hist_df(part, cfg, x, data_fn, mats, name=None, suffix=("png", "pdf")):
    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    batch_size = x.shape[1]
    num_in = cfg["dimensions"]["input"]["variables"]
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]
    n = num_fn * num_eps

    f = (
        torch.tensor(data_fn, device=device, dtype=dtype)
        .view(batch_size, num_fn, num_eps)
        .permute((2, 0, 1))
        .repeat_interleave(repeats=num_in, dim=1)
        .view(num_eps, batch_size * num_in, num_fn, 1)
    )

    connection = pp_de_batch(mats, num_in, num_fn).view(
        2, batch_size * num_in, num_fn, num_fn
    )

    df = torch.empty(
        (num_eps, batch_size * num_in, num_fn, 1), device=device, dtype=dtype
    )

    for k in range(num_eps):
        df[k] = sum(torch.matmul(connection[i], f[k - i]) for i in range(min(k + 1, 2)))

    df = (
        df.view(num_eps, batch_size, num_in, num_fn)
        .permute((0, 2, 1, 3))
        .reshape(num_in * batch_size, num_fn * num_eps)
        .abs()
        .cpu()
        .numpy()
    )

    dfa = numpy.ravel(df)

    nonzero = dfa != 0.0
    dfa = dfa[nonzero]

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(dfa), bins="auto")

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(6.4, 4.8),
        tight_layout=True,
    )

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$|\partial_k f_i^{(j)}|$")

    ax.hist(
        dfa,
        bins=bins,
        weights=numpy.full_like(dfa, 1 / len(dfa)),
        histtype="step",
    )

    for f in suffix:
        fig.savefig(
            part / f"{'dist_df' if name is None else name}_cum.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()

    df = df[:, :10]

    zeros = df == 0.0
    df[zeros] = numpy.full(zeros.sum(), numpy.finfo(numpy.float32).eps)

    bins = 10 ** numpy.histogram_bin_edges(numpy.log10(df), bins="auto")

    labels = numpy.array(
        [
            f"{p} $f^{{({j})}}_{{{i}}}$"
            for i in range(1, num_fn + 1)
            for j in range(num_eps)
            for p in ("Re", "Im")
        ]
    )

    colours = numpy.repeat(matplotlib.cm.tab20(range(n)), 2, axis=0)

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

    ax.set_xscale("log")

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\sum_k |\partial_k f_i^{(j)}|$")

    ax.set_prop_cycle(linestyle=["dashed", "solid"])

    ax.hist(
        df,
        bins=bins,
        weights=numpy.full_like(df, 1 / len(df)),
        histtype="step",
        label=labels,
        color=colours[:10],
    )

    ax.legend(
        title="$f_1^{(j)}$ functions derivatives",
        reverse=True,
        ncols=5,
        bbox_to_anchor=(0.5, -0.2),
        loc="upper center",
    )

    for f in suffix:
        fig.savefig(
            part / f"{'dist_df' if name is None else name}.{f}",
            bbox_inches="tight",
        )

    matplotlib.pyplot.close()


if __name__ == "__main__":
    root = pathlib.Path("box1m")
    part = root / "re"

    cfg = get_cfg(root, part)

    device = torch.device(cfg["device"])
    dtype = getattr(torch, cfg["dtype"])

    _, s12, cut, sc = ps_params(cfg)
    phase_space = PhaseSpace(_, s12, cut, sc, dtype, device)

    n = 1000000
    x = phase_space.test(n, cut_spurious=True)
    data_input = x.cpu().numpy()

    data_boundary_points = numpy.loadtxt(root / "boundary_input.csv", delimiter=",").T
    hist_2d(
        root,
        data_input,
        data_boundary_points,
        [],
        "$s_{23}$",
        "$s_{4}$",
        lloc="upper left",
    )

    print("s12:", s12)
    print(" " * 4, "s23", " " * 7, "s4")
    print("min", numpy.min(data_input, axis=1))
    print("max", numpy.max(data_input, axis=1))

    # letters = (
    #     # r"$s_{12}$",
    #     r"$-s_{23}$",
    #     r"$s_{4}$",
    #     r"$-s_{13}$",
    #     r"$s_{12}-s_{4}$",
    #     r"$s_{4}-s_{23}$",
    # )
    # data_letters = alphabet(data_input, s12).T
    # hist_letters(root, data_letters, letters)

    xx = x[:, :10000]
    data_de = get_a_data(cfg, ConnectionMatrix, xx, s12, verbose=True)

    hist_a_cum(root, data_de, log=False)

    print("Output histogram...")
    data_fn = numpy.loadtxt(part / "testing_output.csv", delimiter=",")
    # hist_output(part, data_fn)
    # hist_output_abs(part, data_fn)
    hist_f_cum(part, data_fn)
    hist_f_f(part, cfg, data_fn, log=False)
    # x = numpy.loadtxt(root / "testing_input.csv", delimiter=",").T
    # hist_output_2d(part, x, data_fn, "$s_{23}$", "$s_{4}$")

    # mats = de_matrix(torch.tensor(x, device=device, dtype=dtype), s12)
    # hist_df(part, cfg, x, data_fn, mats)
