{{G[s4/s23, 1], G[(-s12 + s4)/s23, 1], G[SubPlus[s4/s12], 1], 
  G[0, s4/s23, 1], G[0, (-s12 + s4)/s23, 1], G[0, SubPlus[s4/s12], 1], 
  G[s4/s23, (-s12 + s4)/s23, 1], G[0, 0, s4/s23, 1], 
  G[0, 0, (-s12 + s4)/s23, 1], G[0, 0, SubPlus[s4/s12], 1], 
  G[0, s4/s23, (-s12 + s4)/s23, 1], G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 
   1], G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1], 
  G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1], G[0, 0, 0, s4/s23, 1], 
  G[0, 0, 0, SubPlus[s4/s12], 1], G[0, 0, s4/s23, (-s12 + s4)/s23, 1], 
  G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1], 
  G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1], 
  G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1]}, 
 {{(2*s4)/s12, (-2*s4*(Log[s12/s4] + Log[-(s23/s4)]))/s12, 
   (s4*(Pi^2 + 12*G[0, s4/s23, 1] + 12*G[0, SubPlus[s4/s12], 1] - 
      (12*I)*G[s4/s23, 1]*(Pi - I*Log[-(s23/s4)]) + 
      6*(-2*G[SubPlus[s4/s12], 1]*Log[s12/s4] + 
        (Log[s12/s4] + Log[-(s23/s4)])^2)))/(6*s12), 
   (s4*(2*Pi^2*G[SubPlus[s4/s12], 1] + (12*I)*Pi*G[0, s4/s23, 1] + 
      12*G[SubPlus[s4/s12], 1]*G[0, SubPlus[s4/s12], 1] + 
      (12*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 
      12*(G[0, 0, s4/s23, 1] + G[0, 0, SubPlus[s4/s12], 1] + 
        G[0, s4/s23, (-s12 + s4)/s23, 1] + G[0, SubPlus[s4/s12], 
         SubPlus[s4/s12], 1]) - Pi^2*Log[s12/s4] - 6*G[SubPlus[s4/s12], 1]^2*
       Log[s12/s4] - 12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
      6*G[SubPlus[s4/s12], 1]*Log[s12/s4]^2 - 2*Log[s12/s4]^3 + 
      6*G[s4/s23, 1]*(Pi - I*Log[-(s23/s4)])*
       (Pi - (2*I)*G[(-s12 + s4)/s23, 1] + I*Log[-(s23/s4)]) - 
      (Pi^2 + 12*G[0, SubPlus[s4/s12], 1] - 12*G[s4/s23, (-s12 + s4)/s23, 
          1] + 6*Log[s12/s4]*(-2*G[SubPlus[s4/s12], 1] + Log[s12/s4]))*
       Log[-(s23/s4)] - 6*Log[s12/s4]*Log[-(s23/s4)]^2 - 2*Log[-(s23/s4)]^3 + 
      2*G[(-s12 + s4)/s23, 1]*(6*G[0, s4/s23, 1] + 
        6*G[0, SubPlus[s4/s12], 1] + Pi*(Pi + (6*I)*Log[s12/s4]) + 
        6*Log[s12/s4]*(-G[SubPlus[s4/s12], 1] + Log[-(s23/s4)])) - 
      28*Zeta[3]))/(6*s12), (s4*(-13*Pi^4 + 480*G[0, 0, 0, s4/s23, 1] + 
      480*G[0, 0, 0, SubPlus[s4/s12], 1] + 
      480*G[0, 0, s4/s23, (-s12 + s4)/s23, 1] + 
      480*G[0, 0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
      480*G[0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1] + 
      480*G[0, SubPlus[s4/s12], SubPlus[s4/s12], SubPlus[s4/s12], 1] + 
      40*G[s4/s23, 1]*(I*Pi + Log[-(s23/s4)])*
       (3*Pi^2 - 6*G[(-s12 + s4)/s23, 1]^2 + (2*I)*Pi*Log[-(s23/s4)] - 
        2*Log[-(s23/s4)]^2 + 6*G[(-s12 + s4)/s23, 1]*
         ((-I)*Pi + Log[-(s23/s4)])) + 40*G[(-s12 + s4)/s23, 1]^2*
       (6*G[0, s4/s23, 1] + 6*G[0, SubPlus[s4/s12], 1] + 
        Pi*(Pi + (6*I)*Log[s12/s4]) + 6*Log[s12/s4]*(-G[SubPlus[s4/s12], 1] + 
          Log[-(s23/s4)])) + 80*G[(-s12 + s4)/s23, 1]*
       ((6*I)*Pi*G[0, s4/s23, 1] + (6*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 1] - 
        6*(G[0, 0, s4/s23, 1] + G[0, 0, SubPlus[s4/s12], 1] + 
          G[0, s4/s23, (-s12 + s4)/s23, 1] + G[0, SubPlus[s4/s12], 
           SubPlus[s4/s12], 1]) - 4*Pi^2*Log[s12/s4] - 
        3*G[SubPlus[s4/s12], 1]^2*Log[s12/s4] - 6*G[0, (-s12 + s4)/s23, 1]*
         Log[s12/s4] - (3*I)*Pi*Log[s12/s4]^2 - 
        (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] - 6*G[s4/s23, (-s12 + s4)/s23, 
            1] + 3*Log[s12/s4]^2)*Log[-(s23/s4)] - 3*Log[s12/s4]*
         Log[-(s23/s4)]^2 + G[SubPlus[s4/s12], 1]*
         (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 3*Log[s12/s4]*
           (Log[s12/s4] + 2*Log[-(s23/s4)]))) + 
      20*(-14*Pi^2*G[0, s4/s23, 1] - 2*Pi^2*G[0, SubPlus[s4/s12], 1] - 
        12*Pi^2*G[s4/s23, (-s12 + s4)/s23, 1] - 
        (24*I)*Pi*G[0, 0, s4/s23, 1] - (24*I)*Pi*G[0, s4/s23, 
          (-s12 + s4)/s23, 1] - (24*I)*Pi*G[s4/s23, (-s12 + s4)/s23, 
          (-s12 + s4)/s23, 1] - 4*G[SubPlus[s4/s12], 1]^3*Log[s12/s4] - 
        (24*I)*Pi*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
        24*G[0, 0, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
        24*G[0, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[s12/s4] + 
        Pi^2*Log[s12/s4]^2 + 12*G[0, (-s12 + s4)/s23, 1]*Log[s12/s4]^2 + 
        Log[s12/s4]^4 + 24*(G[0, 0, SubPlus[s4/s12], 1] + 
          G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1])*Log[-(s23/s4)] - 
        24*G[s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23, 1]*Log[-(s23/s4)] + 
        2*Pi^2*Log[s12/s4]*Log[-(s23/s4)] + 4*Log[s12/s4]^3*Log[-(s23/s4)] + 
        Pi^2*Log[-(s23/s4)]^2 + 12*G[0, SubPlus[s4/s12], 1]*
         Log[-(s23/s4)]^2 - 12*G[s4/s23, (-s12 + s4)/s23, 1]*
         Log[-(s23/s4)]^2 + 6*Log[s12/s4]^2*Log[-(s23/s4)]^2 + 
        4*Log[s12/s4]*Log[-(s23/s4)]^3 + Log[-(s23/s4)]^4 - 
        2*G[SubPlus[s4/s12], 1]*(12*G[0, 0, SubPlus[s4/s12], 1] + 
          12*G[0, SubPlus[s4/s12], SubPlus[s4/s12], 1] + Pi^2*Log[s12/s4] + 
          2*Log[s12/s4]^3 + 2*(Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 
            3*Log[s12/s4]^2)*Log[-(s23/s4)] + 6*Log[s12/s4]*
           Log[-(s23/s4)]^2) + 2*G[SubPlus[s4/s12], 1]^2*
         (Pi^2 + 6*G[0, SubPlus[s4/s12], 1] + 3*Log[s12/s4]*
           (Log[s12/s4] + 2*Log[-(s23/s4)])) + 
        56*(Log[s12/s4] + Log[-(s23/s4)])*Zeta[3])))/(240*s12)}, 
  {0, s4/s12, -((s4*(-2 + Log[-(s23/s4)]))/s12), 
   -1/12*(s4*(-48 + Pi^2 - 6*(-4 + Log[-(s23/s4)])*Log[-(s23/s4)]))/s12, 
   (s4*(96 - 2*Pi^2 + Log[-(s23/s4)]*(-48 + Pi^2 - 2*(-6 + Log[-(s23/s4)])*
         Log[-(s23/s4)]) - 28*Zeta[3]))/(12*s12)}, 
  {0, 1, 2 + I*Pi, 4 + (2*I)*Pi - (7*Pi^2)/12, 8 + (4*I)*Pi - (7*Pi^2)/6 - 
    (I/4)*Pi^3 - (7*Zeta[3])/3}, {0, s23/s4, (s23*(2 + I*Pi - Log[s12/s4]))/
    s4, (s23*(48 + (24*I - 7*Pi)*Pi + 6*Log[s12/s4]*(-4 - (2*I)*Pi + 
        Log[s12/s4])))/(12*s4), 
   (s23*(96 + Pi*(48*I + (-14 - (3*I)*Pi)*Pi) + 
      Log[s12/s4]*(-48 + Pi*(-24*I + 7*Pi) + 2*(6 + (3*I)*Pi - Log[s12/s4])*
         Log[s12/s4]) - 28*Zeta[3]))/(12*s4)}}}
