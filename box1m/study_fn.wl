(* ::Package:: *)

Install["handyG"];


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


sol = Get["solution.m"];


s12n = 6.5;
points = Import["explore.csv"]/.{a_,b_}:>{s12->s12n,s23->a,s4->b};
fns = sol /. points;
fns = Abs@Flatten@ReIm@fns;
Mean[fns]
Histogram[Log10@fns]



