#!/usr/bin/env python3

import pathlib, json, math

import matplotlib.pyplot, numpy, torch

from finn.lib.configuration import get_cfg
from finn.lib.model import FeynmanIntegralsModel
from finn.lib.testing import (
    get_abs_diff,
    get_args,
    get_colours,
    get_labels,
    get_rel_diff,
    get_rel_diff_flat,
    get_testing_data,
    init_model,
    plot_abs_diff_cum,
    plot_abs_diff_each,
    plot_abs_diff_ps,
    plot_rel_diff_cum,
    plot_rel_diff_each,
    plot_rel_diff_i,
    plot_rel_diff_ps,
    plot_training_statistics,
)


def relative_difference_ps_fi(part, rd, x, labels, i):
    print(f"Plotting PS relative difference for fn {i}")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xlabel("$s_{23}$")
    ax.set_ylabel("$s_{4}$")

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        weights=rd[:, i],
        bins=50,
        density=True,
        norm="log",
    )

    fig.colorbar(im, label=rf"$\rho(|(f-\mathrm{{NN}})/f|)$ with $f=${labels[i]}")

    for f in ("png", "pdf"):
        fig.savefig(part / f"ps_rel_diff_{i}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


def absolute_difference_ps_fi(part, ad, x, i):
    print(f"Plotting PS absolute difference for fn {i}")

    fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

    ax.set_xlabel("$s_{23}$")
    ax.set_ylabel("$s_{4}$")

    _, _, _, im = ax.hist2d(
        x[0],
        x[1],
        weights=ad[:, i],
        bins=50,
        density=True,
        norm="log",
    )

    fig.colorbar(im, label=r"$\rho(|f-\mathrm{NN}|)$")

    for f in ("png", "pdf"):
        fig.savefig(part / f"ps_abs_diff_{i}.{f}", bbox_inches="tight")

    matplotlib.pyplot.close()


# def random_walk(cfg, model, n, stride, labels, colours):
#     print("random walk")

#     s12 = cfg["phase_space"]["s12"]
#     cut = cfg["phase_space"]["cut"]
#     dtype = getattr(torch, cfg["dtype"])
#     device = torch.device(cfg["device"])

#     singular = True

#     while singular:
#         position = points(1, s12, dtype, device).squeeze()
#         singular = test_singular(position, s12, cut, False)

#     walk = [position]

#     for _ in range(n - 1):
#         singular = True

#         while singular:
#             movement = (
#                 stride * s12 * 2 * (torch.rand(2, dtype=dtype, device=device) - 0.5)
#             )
#             next_position = position + movement
#             singular = test_singular(next_position, s12, cut, False)

#         position = next_position
#         walk.append(position)

#     walk = torch.stack(walk)

#     fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

#     ax.set_xlim(-s12 + 2 * cut, -cut)
#     ax.set_ylim(cut, s12 - 2 * cut)

#     ax.set_xlabel("$s_{23}$")
#     ax.set_ylabel("$s_{4}$")

#     ax.plot(*walk.cpu().T)

#     for f in ("png", "pdf"):
#         fig.savefig(part / f"walk.{f}", bbox_inches="tight")

#     matplotlib.pyplot.close()

#     with torch.no_grad():
#         f = model(walk)

#     f = f.cpu()

#     linestyles = numpy.tile(["solid", "dashed"], 20)

#     fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 7.2), tight_layout=True)

#     ax.set_xlabel("step")
#     ax.set_ylabel("$f_i^{(j)}$")

#     ax.set_prop_cycle(linestyle=linestyles, color=colours)

#     ax.plot(f, label=labels)

#     ax.legend(
#         ncols=5,
#         bbox_to_anchor=(0.5, -0.15),
#         loc="upper center",
#     )

#     for f in ("png", "pdf"):
#         fig.savefig(part / f"walk_f.{f}", bbox_inches="tight")

#     matplotlib.pyplot.close()


# def slice(root, part, cfg, model, labels, n):
#     print("slice")

#     s12 = cfg["phase_space"]["s12"]
#     cut = cfg["phase_space"]["cut"]

#     dtype = getattr(torch, cfg["dtype"])
#     device = torch.device(cfg["device"])

#     delta = 0.1 * s12
#     x = numpy.linspace(
#         (-s12 + 2 * cut + delta, cut + 0.5 * delta),
#         (-cut - 0.5 * delta, s12 - 2 * cut - delta),
#         n,
#     )

#     fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 4.8), tight_layout=True)

#     ax.set_xlim(-s12 + 2 * cut, -cut)
#     ax.set_ylim(cut, s12 - 2 * cut)

#     ax.set_xlabel("$s_{23}$")
#     ax.set_ylabel("$s_{4}$")

#     ax.plot(*x.T)

#     for f in ("png", "pdf"):
#         fig.savefig(part / f"slice_x.{f}", bbox_inches="tight")

#     matplotlib.pyplot.close()

#     xx = torch.tensor(x, dtype=dtype, device=device)

#     assert not test_singular(xx.T, s12, cut, False).any()

#     numpy.savetxt(root / "slice_input.csv", x, delimiter=",")

#     y = numpy.loadtxt(part / "slice_output.csv", delimiter=",")

#     assert len(y) == len(x)

#     with torch.no_grad():
#         yy = model(xx)

#     yy = yy.cpu()

#     colours = numpy.concatenate(
#         (
#             matplotlib.cm.tab20b(range(20)),
#             matplotlib.cm.tab20c(range(20)),
#         )
#     )

#     fig, ax = matplotlib.pyplot.subplots(figsize=(6.4, 6.4), tight_layout=True)

#     ax.set_xlabel("slice point")
#     ax.set_ylabel("$f_i^{(j)}$")

#     ax.set_prop_cycle(color=colours)

#     ax.plot(
#         y,
#         linestyle="dashed",
#     )

#     ax.plot(
#         yy,
#         label=labels,
#         linestyle="solid",
#     )

#     ax.legend(
#         ncols=5,
#         bbox_to_anchor=(0.5, -0.15),
#         loc="upper center",
#     )

#     for f in ("png", "pdf"):
#         fig.savefig(part / f"slice_y.{f}", bbox_inches="tight")

#     matplotlib.pyplot.close()


if __name__ == "__main__":
    args = get_args()
    root = pathlib.Path("box1m")
    part = root / args.part
    rep = part / args.replica

    plot_training_statistics(rep)

    cfg = get_cfg(root, part)

    labels = get_labels(cfg, args.part)
    colours = get_colours(cfg)

    model = init_model(rep, cfg)

    x, ff, nn = get_testing_data(root, part, cfg, model)

    zero_coeffs = cfg["zeros"][args.part]

    rdf = get_rel_diff_flat(ff, nn, zero_coeffs, 1e-4)

    rd, zi, nonzero = get_rel_diff(ff, nn, v=0)
    nzx = numpy.delete(x, zi, axis=1)

    labels_nonzero = labels[nonzero.flat]
    colours_nonzero = colours[nonzero.flat]

    plot_rel_diff_cum(rep, rdf, log=True)

    ad = get_abs_diff(ff, nn)

    plot_abs_diff_cum(rep, ad)

    if args.everything:
        zeros = [label for label, f in zip(labels, ff[0].flat) if f == 0.0]
        print(f"zero outputs: {zeros}")

        plot_rel_diff_each(rep, rd, labels_nonzero, colours_nonzero, nonzero, 6)
        plot_rel_diff_ps(rep, rd, nzx, "$s_{23}$", "$s_{4}$")
        for i in range(nonzero.sum()):
            relative_difference_i(rep, cfg, rd, labels_nonzero, colours_nonzero, i)
            relative_difference_ps_fi(rep, rd, nzx, labels_nonzero, i)

        plot_abs_diff_each(rep, ad, labels, colours, 6)
        plot_abs_diff_ps(rep, ad, x, "$s_{23}$", "$s_{4}$")
        for i in range(ff.shape[1]):
            absolute_difference_ps_fi(rep, ad, x, i)

        # slice(root, rep, cfg, model, labels, 1000)

        # random_walk(cfg, model, 10000, 0.005, labels, colours)
