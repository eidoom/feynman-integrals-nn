#!/usr/bin/env python3

import numpy

from finn.lib.generate_ps import generate_dataset_inputs


def boundaries(ps, n):
    assert not n % 3
    m = n // 3

    s12 = float(ps.const)
    cut = float(ps.cut_phys)

    s4l = cut
    s4h = s12 - 2.0 * cut

    s23l = -s4h
    s23h = -s4l

    rng = numpy.random.default_rng(1)

    # -s23 == cut
    s4a = rng.uniform(s4l, s4h, m)
    x0 = numpy.column_stack((numpy.full(m, -cut), s4a))

    # s4 == cut
    s23 = rng.uniform(s23l, s23h, m)
    x1 = numpy.column_stack((s23, numpy.full(m, cut)))

    # s12 + s23 - s4 == cut
    s4b = rng.uniform(s4l, s4h, m)
    x2 = numpy.column_stack((cut - s12 + s4b, s4b))

    # spurious surfaces are excluded by physical region

    return numpy.concatenate((x0, x1, x2))


if __name__ == "__main__":
    generate_dataset_inputs("box1m", boundaries)
