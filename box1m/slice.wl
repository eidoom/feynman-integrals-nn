(* ::Package:: *)

Install["handyG"];


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


cfg = Association @@ Import["config.json"];


s12n = "s12" /. cfg["phase_space"]


sol = Get["solution.m"];


trans = {t_, v_} -> {s12 -> s12n, s23 -> t, s4 -> v};


slicePS = Import["slice_input.csv"] /. trans


slice = sol /. slicePS;


flat[part_, x_] := Flatten /@ part @ x;


Export["re/slice_output.csv", flat[Re, slice]];


Export["im/slice_output.csv", flat[Im, slice]];
