(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
z ={0,0,1};
X=Sqrt[s]/2;
dot[x_, y_] := x[[1]]*y[[1]] - x[[2;;]] . y[[2;;]];


q[1]=X Prepend[-z,-1];
q[2]=X Prepend[z,-1];
q[3]=X x[1] Prepend[Ry[\[Theta]] . z,1];
q[4]=X x[2]Prepend[Ry[\[Theta]] . Rz[\[Alpha]] . Ry[\[Beta]] . z,1];
q[5]=-Plus@@q/@Range[4]//Factor;
{MatrixForm[q[#]]&/@Range[5]}//TableForm


Plus@@q/@Range[5]//Simplify
dot[q[#],q[#]]&/@Range[4]//Simplify


m5=dot[q[5],q[5]]/.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2];
{{sol}}=Solve[m5==0, {Cos[\[Beta]]}];
sol


Do[p[i]=q[i];,{i,3}];
p[4]=Simplify[q[4]+q[5] /.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2] /. sol];


{MatrixForm[p[#]]&/@Range[4]}//TableForm


Plus@@p/@Range[4]//Simplify
dot[p[#],p[#]]&/@Range[4]//Simplify


s23=FullSimplify[2*dot[p[2],p[3]]]


s23/.\[Theta]->0


s23/.\[Theta]->\[Pi]


s4=FullSimplify[dot[p[4],p[4]]]
