{{s12, s23, s4}, {j[box1m, 1, 1, 1, 1], j[box1m, 0, 1, 0, 1], 
  j[box1m, 1, 0, 0, 1], j[box1m, 1, 0, 1, 0]}, 
 {{{(s12 + s23 + eps*s23 - s4 - eps*s4)/(-s12^2 - s12*s23 + s12*s4), 
    (2 - 4*eps)/(-(s12^2*s23) - s12*s23^2 + s12*s23*s4), 
    (-2 + 4*eps)/(s12^3 + s12^2*s23 - 2*s12^2*s4 - s12*s23*s4 + s12*s4^2), 
    (2 - 4*eps)/(s12^3 + s12^2*s23 - 2*s12^2*s4 - s12*s23*s4 + s12*s4^2)}, 
   {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, -(eps/s12)}}, 
  {{(s12 + eps*s12 + s23 - s4 - eps*s4)/(-(s12*s23) - s23^2 + s23*s4), 
    (2 - 4*eps)/(s12*s23^2 + s23^3 - s12*s23*s4 - 2*s23^2*s4 + s23*s4^2), 
    (-2 + 4*eps)/(s12*s23^2 + s23^3 - s12*s23*s4 - 2*s23^2*s4 + s23*s4^2), 
    (2 - 4*eps)/(-(s12^2*s23) - s12*s23^2 + s12*s23*s4)}, 
   {0, -(eps/s23), 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}, 
  {{eps/(-s12 - s23 + s4), (-2 + 4*eps)/(s12*s23^2 + s23^3 - s12*s23*s4 - 
      2*s23^2*s4 + s23*s4^2), (-2*s12 + 4*eps*s12 - 2*s23 + 4*eps*s23 + 
      4*s4 - 8*eps*s4)/(-(s12^2*s23*s4) - s12*s23^2*s4 + s12^2*s4^2 + 
      3*s12*s23*s4^2 + s23^2*s4^2 - 2*s12*s4^3 - 2*s23*s4^3 + s4^4), 
    (-2 + 4*eps)/(s12^3 + s12^2*s23 - 2*s12^2*s4 - s12*s23*s4 + s12*s4^2)}, 
   {0, 0, 0, 0}, {0, 0, -(eps/s4), 0}, {0, 0, 0, 0}}}}
