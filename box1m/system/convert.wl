(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


family = box1m;
{is,sijs,des}=Get[ToString[family]<>"_MIs_and_DEs_dimless.m"];


j2mi = Get[ToString[family]<>"_reduction_to_utbasis.m"];


mi2spfuncs = Get[ToString[family]<>"_mi2spfuncs.m"];


solution = Normal@Series[is /. j2mi /. mi[a__]:>(mi[a]+eps^5*ALARM) /. mi2spfuncs,{eps,0,4}];


logs2mpls={
	Log[1 - s12/s4] -> G$[{s4/s12}, 1],
	Log[1 - s23/s4] -> G$[{s4/s23}, 1],
	Log[(s12 + s23 - s4)/(s12 - s4)] -> G$[{(-s12 + s4)/s23}, 1]
};


sol = Simplify[Transpose@Table[Coefficient[solution,eps,i],{i,0,4}]/.logs2mpls/.G$[{x__},1]:>G[x,1]];


sol // Dimensions


logCont = {
	Log[s23/s4] -> I*Pi + Log[-(s23/s4)],
	Log[-s4] -> (-I)*Pi + Log[s4]
};


mplCont = G[i___,s4/s12,j___]:>G[i,SubPlus[s4/s12],j];


solCont = FullSimplify[sol/.logCont//.mplCont];


solCont//Dimensions


mpls = Union@Cases[solCont, _G, Infinity];


Put[{mpls,solCont}, "../solution.m"];


des1 = Simplify@des[[2;;]];
des2 = FullSimplify@Table[Coefficient[des1,eps,i],{i,0,1}];


des2//Dimensions
(* eps, in, out, out *)


Put[Flatten@des2/.{0->z},"des.m"];
