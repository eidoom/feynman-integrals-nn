{mi[box1m, 1] -> -(eps*Log[s12/s4]) + 
   (eps^2*Log[s12/s4]*(Log[s12/s4] + 2*Log[-s4]))/2 + 
   (eps^3*Log[s12/s4]*(Pi^2 - 2*Log[s12/s4]^2 - 6*Log[s12/s4]*Log[-s4] - 
      6*Log[-s4]^2))/12 + (eps^4*Log[s12/s4]*(Log[s12/s4]^3 - 
      2*Pi^2*Log[-s4] + 4*Log[s12/s4]^2*Log[-s4] + 4*Log[-s4]^3 - 
      Log[s12/s4]*(Pi^2 - 6*Log[-s4]^2) + 56*Zeta[3]))/24, 
 mi[box1m, 2] -> -(eps*Log[s23/s4]) + 
   (eps^2*Log[s23/s4]*(Log[s23/s4] + 2*Log[-s4]))/2 + 
   (eps^3*Log[s23/s4]*(Pi^2 - 2*Log[s23/s4]^2 - 6*Log[s23/s4]*Log[-s4] - 
      6*Log[-s4]^2))/12 + (eps^4*Log[s23/s4]*(Log[s23/s4]^3 - 
      2*Pi^2*Log[-s4] + 4*Log[s23/s4]^2*Log[-s4] + 4*Log[-s4]^3 - 
      Log[s23/s4]*(Pi^2 - 6*Log[-s4]^2) + 56*Zeta[3]))/24, 
 mi[box1m, 3] -> 1 + eps*(-Log[s12/s4] - Log[-s4]) + 
   eps^2*(-1/12*Pi^2 + (Log[s12/s4] + Log[-s4])^2/2) + 
   (eps^3*(Pi^2*(Log[s12/s4] + Log[-s4]) - 2*(Log[s12/s4] + Log[-s4])^3 - 
      28*Zeta[3]))/12 + 
   (eps^4*(-47*Pi^4 - 60*Pi^2*(Log[s12/s4] + Log[-s4])^2 + 
      60*(Log[s12/s4] + Log[-s4])^4 + 3360*(Log[s12/s4] + Log[-s4])*Zeta[3]))/
    1440, mi[box1m, 4] -> 2 - 2*eps*(Log[s12/s4] + Log[s23/s4] + Log[-s4]) + 
   eps^2*(Pi^2/6 + 2*G$[{0, s4/s12}, 1] + 2*G$[{0, s4/s23}, 1] - 
     2*Log[1 - s12/s4]*Log[s12/s4] + Log[s12/s4]^2 - 
     2*Log[1 - s23/s4]*Log[s23/s4] + 2*Log[s12/s4]*Log[s23/s4] + 
     Log[s23/s4]^2 + 2*Log[s12/s4]*Log[-s4] + 2*Log[s23/s4]*Log[-s4] + 
     Log[-s4]^2) + eps^3*(-2*G$[{0, 0, s4/s12}, 1] - 
     2*G$[{0, 0, s4/s23}, 1] - 2*G$[{0, s4/s12, s4/s12}, 1] - 
     2*G$[{0, s4/s23, (-s12 + s4)/s23}, 1] + (Pi^2*Log[1 - s12/s4])/3 + 
     2*G$[{0, s4/s12}, 1]*Log[1 - s12/s4] + 
     (Pi^2*Log[(s12 + s23 - s4)/(s12 - s4)])/3 + 2*G$[{0, s4/s12}, 1]*
      Log[(s12 + s23 - s4)/(s12 - s4)] + 2*G$[{0, s4/s23}, 1]*
      Log[(s12 + s23 - s4)/(s12 - s4)] - (Pi^2*Log[s12/s4])/6 - 
     2*G$[{0, (-s12 + s4)/s23}, 1]*Log[s12/s4] - Log[1 - s12/s4]^2*
      Log[s12/s4] - 2*Log[1 - s12/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*
      Log[s12/s4] + Log[1 - s12/s4]*Log[s12/s4]^2 - Log[s12/s4]^3/3 - 
     (Pi^2*Log[s23/s4])/6 - 2*G$[{0, s4/s12}, 1]*Log[s23/s4] + 
     2*G$[{s4/s23, (-s12 + s4)/s23}, 1]*Log[s23/s4] - 
     2*Log[1 - s23/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s23/s4] + 
     2*Log[1 - s12/s4]*Log[s12/s4]*Log[s23/s4] + 
     2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]*Log[s23/s4] - 
     Log[s12/s4]^2*Log[s23/s4] + Log[1 - s23/s4]*Log[s23/s4]^2 - 
     Log[s12/s4]*Log[s23/s4]^2 - Log[s23/s4]^3/3 - (Pi^2*Log[-s4])/6 - 
     2*G$[{0, s4/s12}, 1]*Log[-s4] - 2*G$[{0, s4/s23}, 1]*Log[-s4] + 
     2*Log[1 - s12/s4]*Log[s12/s4]*Log[-s4] - Log[s12/s4]^2*Log[-s4] + 
     2*Log[1 - s23/s4]*Log[s23/s4]*Log[-s4] - 2*Log[s12/s4]*Log[s23/s4]*
      Log[-s4] - Log[s23/s4]^2*Log[-s4] - Log[s12/s4]*Log[-s4]^2 - 
     Log[s23/s4]*Log[-s4]^2 - Log[-s4]^3/3 - (14*Zeta[3])/3) + 
   eps^4*((-13*Pi^4)/240 + 2*G$[{0, 0, 0, s4/s12}, 1] + 
     2*G$[{0, 0, 0, s4/s23}, 1] + 2*G$[{0, 0, s4/s12, s4/s12}, 1] + 
     2*G$[{0, 0, s4/s23, (-s12 + s4)/s23}, 1] + 
     2*G$[{0, s4/s12, s4/s12, s4/s12}, 1] + 
     2*G$[{0, s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23}, 1] - 
     2*G$[{0, 0, s4/s12}, 1]*Log[1 - s12/s4] - 2*G$[{0, s4/s12, s4/s12}, 1]*
      Log[1 - s12/s4] + (Pi^2*Log[1 - s12/s4]^2)/6 - 
     2*G$[{0, 0, s4/s12}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)] - 
     2*G$[{0, 0, s4/s23}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)] - 
     2*G$[{0, s4/s12, s4/s12}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)] - 
     2*G$[{0, s4/s23, (-s12 + s4)/s23}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)] + 
     (Pi^2*Log[1 - s12/s4]*Log[(s12 + s23 - s4)/(s12 - s4)])/3 + 
     (Pi^2*Log[(s12 + s23 - s4)/(s12 - s4)]^2)/6 + 
     2*G$[{0, 0, (-s12 + s4)/s23}, 1]*Log[s12/s4] + 
     2*G$[{0, (-s12 + s4)/s23, (-s12 + s4)/s23}, 1]*Log[s12/s4] - 
     (Pi^2*Log[1 - s12/s4]*Log[s12/s4])/6 - (Log[1 - s12/s4]^3*Log[s12/s4])/
      3 - (Pi^2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4])/3 - 
     2*G$[{0, (-s12 + s4)/s23}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)]*
      Log[s12/s4] - Log[1 - s12/s4]^2*Log[(s12 + s23 - s4)/(s12 - s4)]*
      Log[s12/s4] - Log[1 - s12/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]^2*
      Log[s12/s4] + (Pi^2*Log[s12/s4]^2)/12 + G$[{0, (-s12 + s4)/s23}, 1]*
      Log[s12/s4]^2 + (Log[1 - s12/s4]^2*Log[s12/s4]^2)/2 + 
     Log[1 - s12/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]^2 - 
     (Log[1 - s12/s4]*Log[s12/s4]^3)/3 + Log[s12/s4]^4/12 + 
     2*G$[{0, 0, s4/s12}, 1]*Log[s23/s4] + 2*G$[{0, s4/s12, s4/s12}, 1]*
      Log[s23/s4] - 2*G$[{s4/s23, (-s12 + s4)/s23, (-s12 + s4)/s23}, 1]*
      Log[s23/s4] - (Pi^2*Log[1 - s12/s4]*Log[s23/s4])/3 + 
     (Pi^2*Log[1 - s23/s4]*Log[s23/s4])/6 - 
     (Pi^2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s23/s4])/3 + 
     2*G$[{s4/s23, (-s12 + s4)/s23}, 1]*Log[(s12 + s23 - s4)/(s12 - s4)]*
      Log[s23/s4] - Log[1 - s23/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]^2*
      Log[s23/s4] + (Pi^2*Log[s12/s4]*Log[s23/s4])/6 + 
     Log[1 - s12/s4]^2*Log[s12/s4]*Log[s23/s4] + 2*Log[1 - s12/s4]*
      Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]*Log[s23/s4] + 
     Log[(s12 + s23 - s4)/(s12 - s4)]^2*Log[s12/s4]*Log[s23/s4] - 
     Log[1 - s12/s4]*Log[s12/s4]^2*Log[s23/s4] - 
     Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]^2*Log[s23/s4] + 
     (Log[s12/s4]^3*Log[s23/s4])/3 + (Pi^2*Log[s23/s4]^2)/12 - 
     G$[{s4/s23, (-s12 + s4)/s23}, 1]*Log[s23/s4]^2 + 
     Log[1 - s23/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s23/s4]^2 - 
     Log[1 - s12/s4]*Log[s12/s4]*Log[s23/s4]^2 - 
     Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]*Log[s23/s4]^2 + 
     (Log[s12/s4]^2*Log[s23/s4]^2)/2 - (Log[1 - s23/s4]*Log[s23/s4]^3)/3 + 
     (Log[s12/s4]*Log[s23/s4]^3)/3 + Log[s23/s4]^4/12 + 
     2*G$[{0, 0, s4/s12}, 1]*Log[-s4] + 2*G$[{0, 0, s4/s23}, 1]*Log[-s4] + 
     2*G$[{0, s4/s12, s4/s12}, 1]*Log[-s4] + 
     2*G$[{0, s4/s23, (-s12 + s4)/s23}, 1]*Log[-s4] - 
     (Pi^2*Log[1 - s12/s4]*Log[-s4])/3 - 
     (Pi^2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[-s4])/3 + 
     (Pi^2*Log[s12/s4]*Log[-s4])/6 + 2*G$[{0, (-s12 + s4)/s23}, 1]*
      Log[s12/s4]*Log[-s4] + Log[1 - s12/s4]^2*Log[s12/s4]*Log[-s4] + 
     2*Log[1 - s12/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]*
      Log[-s4] - Log[1 - s12/s4]*Log[s12/s4]^2*Log[-s4] + 
     (Log[s12/s4]^3*Log[-s4])/3 + (Pi^2*Log[s23/s4]*Log[-s4])/6 - 
     2*G$[{s4/s23, (-s12 + s4)/s23}, 1]*Log[s23/s4]*Log[-s4] + 
     2*Log[1 - s23/s4]*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s23/s4]*
      Log[-s4] - 2*Log[1 - s12/s4]*Log[s12/s4]*Log[s23/s4]*Log[-s4] - 
     2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[s12/s4]*Log[s23/s4]*Log[-s4] + 
     Log[s12/s4]^2*Log[s23/s4]*Log[-s4] - Log[1 - s23/s4]*Log[s23/s4]^2*
      Log[-s4] + Log[s12/s4]*Log[s23/s4]^2*Log[-s4] + 
     (Log[s23/s4]^3*Log[-s4])/3 + (Pi^2*Log[-s4]^2)/12 - 
     Log[1 - s12/s4]*Log[s12/s4]*Log[-s4]^2 + (Log[s12/s4]^2*Log[-s4]^2)/2 - 
     Log[1 - s23/s4]*Log[s23/s4]*Log[-s4]^2 + Log[s12/s4]*Log[s23/s4]*
      Log[-s4]^2 + (Log[s23/s4]^2*Log[-s4]^2)/2 + (Log[s12/s4]*Log[-s4]^3)/
      3 + (Log[s23/s4]*Log[-s4]^3)/3 + Log[-s4]^4/12 + 
     G$[{0, s4/s23}, 1]*(-1/6*Pi^2 + Log[(s12 + s23 - s4)/(s12 - s4)]^2 - 
       2*Log[(s12 + s23 - s4)/(s12 - s4)]*Log[-s4] + Log[-s4]^2) - 
     (G$[{0, s4/s12}, 1]*(Pi^2 - 6*Log[1 - s12/s4]^2 - 
        6*Log[(s12 + s23 - s4)/(s12 - s4)]^2 - 6*Log[s23/s4]^2 - 
        12*Log[1 - s12/s4]*(Log[(s12 + s23 - s4)/(s12 - s4)] - Log[s23/s4] - 
          Log[-s4]) - 12*Log[s23/s4]*Log[-s4] - 6*Log[-s4]^2 + 
        12*Log[(s12 + s23 - s4)/(s12 - s4)]*(Log[s23/s4] + Log[-s4])))/6 + 
     (14*Log[s12/s4]*Zeta[3])/3 + (14*Log[s23/s4]*Zeta[3])/3 + 
     (14*Log[-s4]*Zeta[3])/3)}
