#!/usr/bin/env python3

import pathlib
import torch, numpy

from finn.lib.study_data import hist_letters
from finn.box1m.phase_space import phase_space
from finn.box1m.de_matrix import de_matrix
from finn.box1m.study_data import hist_2d, hist_de, hist_de_flat, alphabet


if __name__ == "__main__":
    root = pathlib.Path("box1m")
    cfg = get_cfg(root)

    scale, const, cut, _ = ps_params(cfg)

    print(f"Using scale = {scale}, mt2 = {const}, cut = {cut}")

    dtype = torch.float64
    device = torch.device("cpu")

    phase_space = PhaseSpace(scale, const, cut, _, dtype, device)

    n = 1000000
    s = phase_space.test(n, cut_spurious=True)

    bins = 50

    torch.manual_seed(1337)

    training_ps = phase_space(size, const, cut, dtype, device, cut_spurious=True)
    # numpy.savetxt(root / "explore.csv", training_ps.T.cpu(), delimiter=",")
    training_de = de_matrix(training_ps, const)
    training_ps = training_ps.cpu().numpy()

    dea = training_de.abs()
    dea = dea[dea != 0.0]
    std, mean = torch.std_mean(dea)
    print(f"|DE|: mean={mean:.2f} std={std:.2f} max={dea.max():.2f}")

    data_letters = alphabet(training_ps, const).T
    hist_letters(root, data_letters, name="dist_letters_train", s=("pdf",))
    hist_2d(root, training_ps, bins, "dist_2d_train", ("png",))

    hist_de_flat(root, training_de, "dist_de_train", ("pdf",))

    # testing_ps = phase_space(size, const, cut, dtype, device, cut_spurious=False)
    # testing_de = de_matrix(testing_ps, const)
    # testing_ps = testing_ps.cpu().numpy()

    # hist_letters(root, testing_ps, const, "dist_letters_test", ("pdf",))
    # hist_2d(root, testing_ps, bins, "dist_2d_test", ("png",))
    # hist_de(root, testing_de, "dist_de_test", ("pdf",))
