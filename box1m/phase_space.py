import math
import torch

from finn.lib.phase_space import PhaseSpaceBase


class PhaseSpace(PhaseSpaceBase):
    def points(self, n):
        """
        (s12=const,) s23, s4
        """
        s12 = self.const

        # 2 * cut < x < s12 - cut
        x = self.uniform(2 * self.cut_phys, s12 - self.cut_phys, n)

        # -s12 + 2 * cut < s23 < -cut
        # s23 = self.uniform(-s12 + 2 * self.cut_phys, -self.cut_phys, n)
        s23 = self.uniform(-x + self.cut_phys, -self.cut_phys, n)

        # cut < s4 < s12 - 2 * cut
        # s4 = self.uniform(self.cut_phys, s12 - 2 * self.cut_phys, n)
        s4 = s12 - x

        return torch.stack((s23, s4))

    def a_phys(self, s):
        """
        s12 channel: -s23 > 0, s4 > 0, s12 + s23 - s4 > 0
        """
        s12 = self.const
        s23 = s[0]
        s4 = s[1]

        return torch.stack(
            (
                -s23,
                s4,
                s12 + s23 - s4,
            )
        )

    def a_spur(self, s):
        """
        spurious regulated: s12 - s4 > cut, s4 - s23 > cut
        these are actually always satisfied by phys cut for current PS choice
        """
        s12 = self.const
        s23 = s[0]
        s4 = s[1]

        return torch.stack(
            (
                s12 - s4,
                s4 - s23,
            )
        )
