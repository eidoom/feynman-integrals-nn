# One-external-mass box

The system is defined by `system/box1m_MIs_and_DEs_dimless.m` as the nested list:

```
{
    {master integrals},
    {kinematic variables},
    {connection matrices}
}
```

## Dataset generation

Mathematica script depends on `handyG` for evaluation of MPLs.

For example,

```shell
./box1m/generate_ps.py -p 3 3 10000
math -script box1m/generate_fn.wl
```

Pre-generated datasets are available on [Hugging Face](https://huggingface.co/datasets/feynman-integrals-nn/box1m).

## Training

For replica `<replica>` of part `<part>`,

```shell
./train.py box1m <part> <replica>
./test.py -p box1m <part> <replica>
```

## Timing

Epoch (2048x128) time:

| Machine   | Device                                | RAM (GB) | Type | Partitions | Time (s) |
| --------- | ------------------------------------- | -------- | ---- | ---------- | -------- |
| Laptop 1  | Nvidia GeForce RTX 3050 Ti Laptop GPU | 4        | cuda | 1          | 13       |
| Laptop 1  | Intel Core i7-12700H CPU `-j6`        | 32       | cpu  | 1          | 14       |
| Desktop 2 | AMD Ryzen 7 5700G CPU                 | 32       | cpu  | 1          | 30       |
| Laptop 2  | Intel Core i7-7700HQ CPU              | 16       | cpu  | 1          | 37       |
| Laptop 2  | Nvidia GeForce GTX 1050 Mobile GPU    | 4        | cuda | 1          | 38       |
| Server 1  | Intel Xeon Gold 6130 CPU `-j8`        | 768      | cpu  | 1          | 40       |
| Laptop 3  | Apple M1 Pro (6+2) CPU `-j6`          | 16       | cpu  | 1          | 63       |
