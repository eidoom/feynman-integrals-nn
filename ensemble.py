#!/usr/bin/env python3

import argparse, pathlib, copy, importlib, random, itertools

import numpy, torch, matplotlib.pyplot

from finn.lib.configuration import get_cfg, ps_params
from finn.lib.inference import Ensemble


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "model",
        type=pathlib.Path,
        help="Path to integral family model directory.",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()

    cfg = get_cfg(args.model, verbose=False)
    scale, const, cut_phys, cut_spur = ps_params(cfg)

    model = Ensemble(
        args.model,
        [r.stem for r in (args.model / "re").iterdir() if r.is_dir()],
        const=const,
        device=cfg["device"],
        re_z=cfg["zeros"]["re"],
        im_z=cfg["zeros"]["im"],
        v=False,
    )
    model.re_m = numpy.array(model.re_m)
    model.im_m = numpy.array(model.im_m)

    name = args.model.stem
    mod_ps = importlib.import_module(f"finn.{name}.phase_space")
    ps = mod_ps.PhaseSpace(scale, const, cut_phys, cut_spur, model.dtype, model.device)
    n = 10000
    x = ps(n)
    s12s = torch.full((1, n), const, device=model.device)
    x = torch.cat((x, s12s))
    x = x.T

    model.eval(x)

    ord = [*range(10)]

    # TODO need more replicas (say n=20) to evaluate efficacy of n=10
    j = 10
    c = len([*itertools.combinations(ord, j)])
    print(f"End: {j} of {c}")

    ii = [*range(2, j + 1)]
    ess = []
    for _ in range(10):
        random.shuffle(ord)

        es = []
        for i in ii:
            e = model.error_abs()[ord[:i]].mean().item()

            # print(f"{i:>2} {e:.2g}")
            es.append(e)

        ess.append(es)

    fig, ax = matplotlib.pyplot.subplots()

    for es in ess:
        ax.plot(ii, es)

    matplotlib.pyplot.show()
