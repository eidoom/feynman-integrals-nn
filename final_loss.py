#!/usr/bin/env python3

import argparse, pathlib

from finn.lib.testing import (
    load_training_stats,
)


def get_args():
    parser = argparse.ArgumentParser("Plots the learning curve")
    parser.add_argument(
        "data",
        type=pathlib.Path,
        help="Path to data, eg box/re",
    )
    parser.add_argument(
        "-n",
        type=int,
        default=10,
        help="Number of best replicas to select [default: 10]",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()

    print(f"loading from {args.data}")

    fls = {
        rep.stem: load_training_stats(rep)[2, -1]
        for rep in args.data.iterdir()
        if rep.is_dir()
    }

    print(f"found {len(fls)} replicas")

    fls = sorted(fls.items(), key=lambda x: x[1])

    print("replicas sorted by final loss:")

    for k, v in fls:
        print(f"{k:>2} {v:7.2g}")

    print(f"{args.n} best replicas:")

    print(sorted([x[0] for x in fls[: args.n]]))

    mean = sum(x[1] for x in fls[: args.n]) / args.n
    print(f"mean final loss of best replicas: {mean:.2g}")
