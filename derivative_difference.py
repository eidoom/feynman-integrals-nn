#!/usr/bin/env python3

import argparse, pathlib, importlib

import torch

from finn.lib.configuration import get_cfg, ps_params
from finn.lib.inference import load_model
from finn.lib.loss import DifferentialEquationsLoss
from finn.lib.model import FeynmanIntegralsModel
from finn.lib.derivative_difference import DerivativeDifference


def get_args():
    parser = argparse.ArgumentParser("Calculate the derivative difference")
    parser.add_argument(
        "family",
        type=pathlib.Path,
        help="Path to integral family directory, eg box",
    )
    parser.add_argument(
        "-n",
        "--number",
        type=int,
        default=10**5,
        metavar="N",
        help="Number of points to test on [default: 10**5]",
    )
    parser.add_argument(
        "-s",
        "--partition",
        type=int,
        metavar="S",
        help="Size of partitions to split operation into, useful to calculate large N without overflowing memory [default: N]",
    )
    parser.add_argument(
        "-d",
        "--device",
        type=torch.device,
        default="cpu",
        metavar="D",
        help="PyTorch backend device (cpu [default], cuda, mps, ...)",
    )
    parser.add_argument(
        "-c",
        "--name",
        type=str,
        metavar="C",
        help="Name of integral family in source code if different to model directory name",
    )
    parser.add_argument(
        "-p",
        "--part",
        choices=("re", "im"),
        help="Choose real or imaginary part; do both if unset",
    )
    parser.add_argument(
        "-r",
        "--replica",
        type=str,
        nargs="+",
        metavar="R",
        help="Name of replica(s); do all if unset",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity",
    )
    args = parser.parse_args()
    if args.name is None:
        args.name = args.family.stem
    if args.partition is None:
        args.partition = args.number
    if args.number % args.partition:
        raise Exception(
            f"Total number {args.number} is not exactly divided by parititon size {args.partition}"
        )
    args.partitions = args.number // args.partition
    return args


if __name__ == "__main__":
    args = get_args()

    print(
        f"Computing {args.name} derivative differences over {args.number} points with partitions size {args.partition} ({args.partitions} partitions) on {args.device}"
    )
    if str(args.device) == "cpu" and input("Is this safe for RAM? ") != "y":
        exit()

    n = 10

    dd = DerivativeDifference(args.family, args.name, args.device, args.verbose)

    models = []
    for part in (args.part,) if args.part is not None else ("re", "im"):
        folder = args.family / part
        replicas = (
            (x for x in folder.iterdir() if x.is_dir())
            if args.replica is None
            else (folder / x for x in args.replica)
        )
        models += replicas
    names = [
        f"{model.parts[-2]}-{model.stem}" if args.part is None else model.stem
        for model in models
    ]

    means = dd(models, args.partition, args.partitions, mean=True)

    res = [*zip(names, means)]
    mdd = sorted(res, key=lambda x: x[1])

    for k, v in mdd:
        print(f"{k:>5} {v:7.2g}")

    n = 10
    print(f"{n} best replicas:")
    print(sorted([x[0] for x in mdd[:n]]))
